// $ANTLR 2.7.6 (2005-12-22): "JSHOP2.g" -> "JSHOP2Lexer.java"$

  package com.gamalocus.jshop2rt;

  import java.io.IOException;
  import java.util.LinkedList;
  import java.util.Vector; 
  import java.util.Map;
  import java.util.HashMap;

public interface JSHOP2TokenTypes {
	int EOF = 1;
	int NULL_TREE_LOOKAHEAD = 3;
	int LP = 4;
	int DEFPROBLEM = 5;
	int ID = 6;
	int RP = 7;
	int NIL = 8;
	int DEFDOMAIN = 9;
	int METHOD = 10;
	int RELATIVE = 11;
	int NUM = 12;
	int OPERATOR = 13;
	int OPID = 14;
	int VARID = 15;
	int PROTECTION = 16;
	int FORALL = 17;
	int AXIOM = 18;
	int UNORDERED = 19;
	int IMMEDIATE = 20;
	int FIRST = 21;
	int SORT = 22;
	int AND = 23;
	int OR = 24;
	int NOT = 25;
	int IMPLY = 26;
	int ASSIGN = 27;
	int CALL = 28;
	int DOT = 29;
	int DIV = 30;
	int EQUAL = 31;
	int LESS = 32;
	int LESSEQ = 33;
	int MEMBER = 34;
	int MINUS = 35;
	int MORE = 36;
	int MOREEQ = 37;
	int MULT = 38;
	int NOTEQ = 39;
	int PLUS = 40;
	int POWER = 41;
	int DEFPROBLEMSET = 42;
	int STDLIB = 43;
	int WS = 44;
	int COMMENT = 45;
}
