package examples;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;


import basic.basic;
import basic.problem;
import blocks.blocks;

import com.gamalocus.jshop2rt.util.DomainCompiler;
import com.gamalocus.jshop2rt.util.ProblemCompiler;

public class Examples {

	private File srcdir;
	private File destdir;

	@Before
	public void setUp() throws Exception {
		String basePath = "/Users/gcc/workspace/jshop2cp/";
		srcdir = new File(basePath + "examples/");
		destdir = new File(basePath + "examples");
	}
	
	@Test
	public void compileTestDomainAndExecute() throws Exception {
		DomainCompiler.generateJavaSource(srcdir, Class.forName("test.test"), destdir);
		System.out.println("success compilation");
		ProblemCompiler.generateJavaSource(srcdir, Class.forName("test.problem"), destdir);
		System.out.println("success compilation");
		System.out.println(test.problem.getPlans().getFirst().toString(test.problem.owner));
		assertTrue(true);
	}
	
	
	@Test
	public void compileRelativeConditionalDomainAndExecuteRelative() throws Exception {
		DomainCompiler.generateJavaSource(srcdir, Class.forName("relativeconditional.relativeconditional"), destdir);
		System.out.println("success compilation");
		ProblemCompiler.generateJavaSource(srcdir, Class.forName("relativeconditional.problem"), destdir);
		System.out.println("success compilation");
		System.out.println(relativeconditional.problem.getPlans().getFirst().toString(relativeconditional.problem.owner));
		assertTrue(true);
	}
	
	@Test
	public void compileBasicDomain() throws Exception {
		DomainCompiler.generateJavaSource(srcdir, basic.class, destdir);
		System.out.println("success compilation");
		assertTrue(true);
	}

	@Test
	public void compileBasicProblem() throws Exception {
		ProblemCompiler.generateJavaSource(srcdir, problem.class, destdir);
		assertTrue(true);
	}

	@Test
	public void compileBlockDomain() throws Exception {
		DomainCompiler.generateJavaSource(srcdir, blocks.class, destdir);
		System.out.println("success compilation");
		assertTrue(true);
	}

	@Test
	public void compileBlockProblems() throws Exception {
		ProblemCompiler.generateJavaSource(srcdir, Class.forName("blocks.problem"), destdir);
		System.out.println("success compilation");
		ProblemCompiler.generateJavaSource(srcdir, Class.forName("blocks.smallproblem"), destdir);
		System.out.println("success compilation");
		assertTrue(true);
	}

	@Test
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void compileForallDomainAndProblem() throws Exception {
		DomainCompiler.generateJavaSource(srcdir, forall.forallexample.class, destdir);
		DomainCompiler.generateJavaSource(srcdir, foralltest.foralltest.class, destdir);
		List<String> problems = Arrays.asList(new String[]{"forall.problem", "foralltest.problem"});
		for (String problemName : problems) {			
			ProblemCompiler.generateJavaSource(srcdir, Class.forName(problemName), destdir);
			System.out.println("success compilation");
		}
		assertTrue(true);
	}

	@Test
	public void compileDomainsCreateBeforeJavaFile() throws Exception {
		List<String> domains = Arrays.asList(new String[]{"freecell.freecell",
				"logistics.logistics", "madrts.madrts", "oldblocks.oldblocks", "propagation.propagation", "rover.rover"});
		for (String className : domains) {
			File javaSource = new File(destdir, className.replace(".", "/") + ".java");
			javaSource.getParentFile().mkdirs();
			FileWriter out = new FileWriter(javaSource);
			out.write("package " + javaSource.getName().replaceAll("\\.java", "") + ";\n");
			out.write("public class " + javaSource.getName().replaceAll("\\.java", "") + " { }");
			out.close();

			DomainCompiler.generateJavaSource(srcdir, Class.forName(className), destdir);
			System.out.println("success compilation :: " + destdir + className);
		}

		assertTrue(true);
	}

	
	@Test
	public void compileProblemsCreateBeforeJavaFile() throws Exception {
		List<String> domains = Arrays.asList(new String[]{"freecell",
				"logistics", "madrts", "oldblocks", "propagation", "rover"});
		for (String name : domains) {
			File javaSource = new File(destdir, name.replace(".", "/") + "problem.java");
			FileWriter out = new FileWriter(javaSource);
			out.write("package " + javaSource.getName().replaceAll("\\.java", "") + ";\n");
			out.write("public class " + javaSource.getName().replaceAll("\\.java", "") + " { }");
			out.close();

			ProblemCompiler.generateJavaSource(srcdir, Class.forName(name + ".problem"), destdir);
			System.out.println("success compilation :: " + destdir + "/" + name);
		}

		assertTrue(true);
	}
}
