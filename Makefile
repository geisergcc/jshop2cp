CLASSPATH = $(CURDIR)/lib/jshop2rt.jar:$(CURDIR)/lib/antlr-2.7.6.jar:.
export CLASSPATH

DEST = $(CURDIR)/src/br/usp/ime/cg/automatedld/server/planner
PACKAGE = br.usp.ime.cg.automatedld.server.planner

#all: lib/JSHOP2.jar lib/antlr.jar
#	mkdir -p $(DEST)
#	echo '(defdomain coursegeneration(' > $(DEST)/coursegeneration
#	for src in $(shell ls common/*.lisp) ; do \
#		cat $$src >> $(DEST)/coursegeneration ; \
#	done
#	for src in $(shell ls clfps/*.lisp) ; do \
#		cat $$src >> $(DEST)/coursegeneration ; \
#	done
#	echo '))' >> $(DEST)/coursegeneration
#
#domain: common/main.lisp common/learner.lisp common/domain.lisp clfps/$(SRC).lisp
#	echo '(defdomain domain(' > $(SRC)/domain.lisp
#	cat common/main.lisp >> $(SRC)/domain.lisp
#	cat common/learner.lisp >> $(SRC)/domain.lisp
#	cat common/domain.lisp >> $(SRC)/domain.lisp
#	cat clfps/common.lisp >> $(SRC)/domain.lisp
#	cat clfps/$(SRC).lisp >> $(SRC)/domain.lisp
#	echo '))' >> $(SRC)/domain.lisp
#
#build: lib/JSHOP2.jar lib/antlr.jar $(SRC)/domain.lisp
#	make SRC="$(SRC)" domain
#	cd $(SRC); java JSHOP2.InternalDomain domain.lisp

c: src/JSHOP2.g
	cp src/JSHOP2.g bin/JSHOP2.g; cd bin; java antlr.Tool JSHOP2.g;
	cp bin/JSHOP2TokenTypes.txt src/com/gamalocus/jshop2rt/JSHOP2TokenTypes.txt
	cp bin/JSHOP2TokenTypes.java src/com/gamalocus/jshop2rt/JSHOP2TokenTypes.java
	cp bin/JSHOP2Lexer.java src/com/gamalocus/jshop2rt/JSHOP2Lexer.java
	cp bin/JSHOP2Parser.java src/com/gamalocus/jshop2rt/JSHOP2Parser.java	

clean:
	rm bin/JSHOP2Lexer.*
	rm bin/JSHOP2Parser.*
	rm bin/JSHOP2TokenTypes.java
	rm bin/JSHOP2TokenTypes.txt

