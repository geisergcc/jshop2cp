package rover;
import com.gamalocus.jshop2rt.*;

public class rover extends Domain
{
	private static final long serialVersionUID = 1355804621434826828L;


	/**
	 * Precondition of Operator #-1 for primitive task !navigate
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition30 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition30(Domain owner, Term[] unifier)
		{
			p = new Precondition[8];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(0, 3, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(1, 3, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(1, 3, new TermList(owner.getTermVariable(2), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[4] = new PreconditionAtomic(new Predicate(2, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL)))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[5] = new PreconditionAtomic(new Predicate(3, 3, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[6] = new PreconditionAtomic(new Predicate(4, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[7] = new PreconditionAtomic(new Predicate(5, 3, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier);
			b = new Term[8][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
			b[6] = null;
			b[7] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[7] == null)
			{
				boolean b6changed = false;
				while (b[6] == null)
				{
					boolean b5changed = false;
					while (b[5] == null)
					{
						boolean b4changed = false;
						while (b[4] == null)
						{
							boolean b3changed = false;
							while (b[3] == null)
							{
								boolean b2changed = false;
								while (b[2] == null)
								{
									boolean b1changed = false;
									while (b[1] == null)
									{
										b[1] = p[1].nextBinding(state);
										if (b[1] == null)
											return null;
										else
											bestMatch = Math.max(bestMatch, 1);
										b1changed = true;
									}
									if ( b1changed ) {
										p[2].reset(state);
										p[2].bind(Term.merge(b, 2));
									}
									b[2] = p[2].nextBinding(state);
									if (b[2] == null)
										b[1] = null;
									else
										bestMatch = Math.max(bestMatch, 2);
									b2changed = true;
								}
								if ( b2changed ) {
									p[3].reset(state);
									p[3].bind(Term.merge(b, 3));
								}
								b[3] = p[3].nextBinding(state);
								if (b[3] == null)
									b[2] = null;
								else
									bestMatch = Math.max(bestMatch, 3);
								b3changed = true;
							}
							if ( b3changed ) {
								p[4].reset(state);
								p[4].bind(Term.merge(b, 4));
							}
							b[4] = p[4].nextBinding(state);
							if (b[4] == null)
								b[3] = null;
							else
								bestMatch = Math.max(bestMatch, 4);
							b4changed = true;
						}
						if ( b4changed ) {
							p[5].reset(state);
							p[5].bind(Term.merge(b, 5));
						}
						b[5] = p[5].nextBinding(state);
						if (b[5] == null)
							b[4] = null;
						else
							bestMatch = Math.max(bestMatch, 5);
						b5changed = true;
					}
					if ( b5changed ) {
						p[6].reset(state);
						p[6].bind(Term.merge(b, 6));
					}
					b[6] = p[6].nextBinding(state);
					if (b[6] == null)
						b[5] = null;
					else
						bestMatch = Math.max(bestMatch, 6);
					b6changed = true;
				}
				if ( b6changed ) {
					p[7].reset(state);
					p[7].bind(Term.merge(b, 7));
				}
				b[7] = p[7].nextBinding(state);
				if (b[7] == null)
					b[6] = null;
				else
					bestMatch = Math.max(bestMatch, 7);
			}

			Term[] retVal = Term.merge(b, 8);
			b[7] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			p[4].reset(state);
			p[5].reset(state);
			p[6].reset(state);
			p[7].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
			b[6] = null;
			b[7] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of Operator #-1 for primitive task !navigate [unknown source pos]";
		}
	}

	/**
	 * Operator #-1 for primitive task !navigate
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator23 extends Operator
{
	/**
	 * Operator #-1 for primitive task !navigate
	 */
		public Operator23(Domain owner)
		{
			super(owner, new Predicate(0, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL)))), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[1];
			delIn[0] = new DelAddAtomic(new Predicate(4, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[1];
			addIn[0] = new DelAddAtomic(new Predicate(4, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !navigate [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new Precondition30(owner, unifier)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Precondition of Operator #-1 for primitive task !sample_soil
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition31 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition31(Domain owner, Term[] unifier)
		{
			p = new Precondition[9];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(0, 3, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(6, 3, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(1, 3, new TermList(owner.getTermVariable(2), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[4] = new PreconditionAtomic(new Predicate(4, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[5] = new PreconditionAtomic(new Predicate(7, 3, new TermList(owner.getTermVariable(2), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[6] = new PreconditionAtomic(new Predicate(8, 3, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[7] = new PreconditionAtomic(new Predicate(9, 3, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(0), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[8] = new PreconditionAtomic(new Predicate(10, 3, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			b = new Term[9][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
			b[6] = null;
			b[7] = null;
			b[8] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[8] == null)
			{
				boolean b7changed = false;
				while (b[7] == null)
				{
					boolean b6changed = false;
					while (b[6] == null)
					{
						boolean b5changed = false;
						while (b[5] == null)
						{
							boolean b4changed = false;
							while (b[4] == null)
							{
								boolean b3changed = false;
								while (b[3] == null)
								{
									boolean b2changed = false;
									while (b[2] == null)
									{
										boolean b1changed = false;
										while (b[1] == null)
										{
											b[1] = p[1].nextBinding(state);
											if (b[1] == null)
												return null;
											else
												bestMatch = Math.max(bestMatch, 1);
											b1changed = true;
										}
										if ( b1changed ) {
											p[2].reset(state);
											p[2].bind(Term.merge(b, 2));
										}
										b[2] = p[2].nextBinding(state);
										if (b[2] == null)
											b[1] = null;
										else
											bestMatch = Math.max(bestMatch, 2);
										b2changed = true;
									}
									if ( b2changed ) {
										p[3].reset(state);
										p[3].bind(Term.merge(b, 3));
									}
									b[3] = p[3].nextBinding(state);
									if (b[3] == null)
										b[2] = null;
									else
										bestMatch = Math.max(bestMatch, 3);
									b3changed = true;
								}
								if ( b3changed ) {
									p[4].reset(state);
									p[4].bind(Term.merge(b, 4));
								}
								b[4] = p[4].nextBinding(state);
								if (b[4] == null)
									b[3] = null;
								else
									bestMatch = Math.max(bestMatch, 4);
								b4changed = true;
							}
							if ( b4changed ) {
								p[5].reset(state);
								p[5].bind(Term.merge(b, 5));
							}
							b[5] = p[5].nextBinding(state);
							if (b[5] == null)
								b[4] = null;
							else
								bestMatch = Math.max(bestMatch, 5);
							b5changed = true;
						}
						if ( b5changed ) {
							p[6].reset(state);
							p[6].bind(Term.merge(b, 6));
						}
						b[6] = p[6].nextBinding(state);
						if (b[6] == null)
							b[5] = null;
						else
							bestMatch = Math.max(bestMatch, 6);
						b6changed = true;
					}
					if ( b6changed ) {
						p[7].reset(state);
						p[7].bind(Term.merge(b, 7));
					}
					b[7] = p[7].nextBinding(state);
					if (b[7] == null)
						b[6] = null;
					else
						bestMatch = Math.max(bestMatch, 7);
					b7changed = true;
				}
				if ( b7changed ) {
					p[8].reset(state);
					p[8].bind(Term.merge(b, 8));
				}
				b[8] = p[8].nextBinding(state);
				if (b[8] == null)
					b[7] = null;
				else
					bestMatch = Math.max(bestMatch, 8);
			}

			Term[] retVal = Term.merge(b, 9);
			b[8] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			p[4].reset(state);
			p[5].reset(state);
			p[6].reset(state);
			p[7].reset(state);
			p[8].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
			b[6] = null;
			b[7] = null;
			b[8] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of Operator #-1 for primitive task !sample_soil [unknown source pos]";
		}
	}

	/**
	 * Operator #-1 for primitive task !sample_soil
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator24 extends Operator
{
	/**
	 * Operator #-1 for primitive task !sample_soil
	 */
		public Operator24(Domain owner)
		{
			super(owner, new Predicate(1, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL)))), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[2];
			delIn[0] = new DelAddAtomic(new Predicate(10, 3, new TermList(owner.getTermVariable(1), TermList.NIL)));
			delIn[1] = new DelAddAtomic(new Predicate(7, 3, new TermList(owner.getTermVariable(2), TermList.NIL)));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[2];
			addIn[0] = new DelAddAtomic(new Predicate(11, 3, new TermList(owner.getTermVariable(1), TermList.NIL)));
			addIn[1] = new DelAddAtomic(new Predicate(12, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !sample_soil [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new Precondition31(owner, unifier)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Precondition of Operator #-1 for primitive task !sample_rock
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition32 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition32(Domain owner, Term[] unifier)
		{
			p = new Precondition[9];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(0, 3, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(6, 3, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(1, 3, new TermList(owner.getTermVariable(2), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[4] = new PreconditionAtomic(new Predicate(4, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[5] = new PreconditionAtomic(new Predicate(13, 3, new TermList(owner.getTermVariable(2), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[6] = new PreconditionAtomic(new Predicate(14, 3, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[7] = new PreconditionAtomic(new Predicate(9, 3, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(0), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[8] = new PreconditionAtomic(new Predicate(10, 3, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			b = new Term[9][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
			b[6] = null;
			b[7] = null;
			b[8] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[8] == null)
			{
				boolean b7changed = false;
				while (b[7] == null)
				{
					boolean b6changed = false;
					while (b[6] == null)
					{
						boolean b5changed = false;
						while (b[5] == null)
						{
							boolean b4changed = false;
							while (b[4] == null)
							{
								boolean b3changed = false;
								while (b[3] == null)
								{
									boolean b2changed = false;
									while (b[2] == null)
									{
										boolean b1changed = false;
										while (b[1] == null)
										{
											b[1] = p[1].nextBinding(state);
											if (b[1] == null)
												return null;
											else
												bestMatch = Math.max(bestMatch, 1);
											b1changed = true;
										}
										if ( b1changed ) {
											p[2].reset(state);
											p[2].bind(Term.merge(b, 2));
										}
										b[2] = p[2].nextBinding(state);
										if (b[2] == null)
											b[1] = null;
										else
											bestMatch = Math.max(bestMatch, 2);
										b2changed = true;
									}
									if ( b2changed ) {
										p[3].reset(state);
										p[3].bind(Term.merge(b, 3));
									}
									b[3] = p[3].nextBinding(state);
									if (b[3] == null)
										b[2] = null;
									else
										bestMatch = Math.max(bestMatch, 3);
									b3changed = true;
								}
								if ( b3changed ) {
									p[4].reset(state);
									p[4].bind(Term.merge(b, 4));
								}
								b[4] = p[4].nextBinding(state);
								if (b[4] == null)
									b[3] = null;
								else
									bestMatch = Math.max(bestMatch, 4);
								b4changed = true;
							}
							if ( b4changed ) {
								p[5].reset(state);
								p[5].bind(Term.merge(b, 5));
							}
							b[5] = p[5].nextBinding(state);
							if (b[5] == null)
								b[4] = null;
							else
								bestMatch = Math.max(bestMatch, 5);
							b5changed = true;
						}
						if ( b5changed ) {
							p[6].reset(state);
							p[6].bind(Term.merge(b, 6));
						}
						b[6] = p[6].nextBinding(state);
						if (b[6] == null)
							b[5] = null;
						else
							bestMatch = Math.max(bestMatch, 6);
						b6changed = true;
					}
					if ( b6changed ) {
						p[7].reset(state);
						p[7].bind(Term.merge(b, 7));
					}
					b[7] = p[7].nextBinding(state);
					if (b[7] == null)
						b[6] = null;
					else
						bestMatch = Math.max(bestMatch, 7);
					b7changed = true;
				}
				if ( b7changed ) {
					p[8].reset(state);
					p[8].bind(Term.merge(b, 8));
				}
				b[8] = p[8].nextBinding(state);
				if (b[8] == null)
					b[7] = null;
				else
					bestMatch = Math.max(bestMatch, 8);
			}

			Term[] retVal = Term.merge(b, 9);
			b[8] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			p[4].reset(state);
			p[5].reset(state);
			p[6].reset(state);
			p[7].reset(state);
			p[8].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
			b[6] = null;
			b[7] = null;
			b[8] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of Operator #-1 for primitive task !sample_rock [unknown source pos]";
		}
	}

	/**
	 * Operator #-1 for primitive task !sample_rock
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator25 extends Operator
{
	/**
	 * Operator #-1 for primitive task !sample_rock
	 */
		public Operator25(Domain owner)
		{
			super(owner, new Predicate(2, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL)))), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[2];
			delIn[0] = new DelAddAtomic(new Predicate(10, 3, new TermList(owner.getTermVariable(1), TermList.NIL)));
			delIn[1] = new DelAddAtomic(new Predicate(13, 3, new TermList(owner.getTermVariable(2), TermList.NIL)));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[2];
			addIn[0] = new DelAddAtomic(new Predicate(11, 3, new TermList(owner.getTermVariable(1), TermList.NIL)));
			addIn[1] = new DelAddAtomic(new Predicate(15, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !sample_rock [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new Precondition32(owner, unifier)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Precondition of Operator #-1 for primitive task !drop
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition33 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition33(Domain owner, Term[] unifier)
		{
			p = new Precondition[5];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(0, 2, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(6, 2, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(9, 2, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(0), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[4] = new PreconditionAtomic(new Predicate(11, 2, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			b = new Term[5][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[4] == null)
			{
				boolean b3changed = false;
				while (b[3] == null)
				{
					boolean b2changed = false;
					while (b[2] == null)
					{
						boolean b1changed = false;
						while (b[1] == null)
						{
							b[1] = p[1].nextBinding(state);
							if (b[1] == null)
								return null;
							else
								bestMatch = Math.max(bestMatch, 1);
							b1changed = true;
						}
						if ( b1changed ) {
							p[2].reset(state);
							p[2].bind(Term.merge(b, 2));
						}
						b[2] = p[2].nextBinding(state);
						if (b[2] == null)
							b[1] = null;
						else
							bestMatch = Math.max(bestMatch, 2);
						b2changed = true;
					}
					if ( b2changed ) {
						p[3].reset(state);
						p[3].bind(Term.merge(b, 3));
					}
					b[3] = p[3].nextBinding(state);
					if (b[3] == null)
						b[2] = null;
					else
						bestMatch = Math.max(bestMatch, 3);
					b3changed = true;
				}
				if ( b3changed ) {
					p[4].reset(state);
					p[4].bind(Term.merge(b, 4));
				}
				b[4] = p[4].nextBinding(state);
				if (b[4] == null)
					b[3] = null;
				else
					bestMatch = Math.max(bestMatch, 4);
			}

			Term[] retVal = Term.merge(b, 5);
			b[4] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			p[4].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of Operator #-1 for primitive task !drop [unknown source pos]";
		}
	}

	/**
	 * Operator #-1 for primitive task !drop
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator26 extends Operator
{
	/**
	 * Operator #-1 for primitive task !drop
	 */
		public Operator26(Domain owner)
		{
			super(owner, new Predicate(3, 2, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[1];
			delIn[0] = new DelAddAtomic(new Predicate(11, 2, new TermList(owner.getTermVariable(1), TermList.NIL)));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[1];
			addIn[0] = new DelAddAtomic(new Predicate(10, 2, new TermList(owner.getTermVariable(1), TermList.NIL)));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !drop [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new Precondition33(owner, unifier)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Precondition of Operator #-1 for primitive task !calibrate
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition34 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition34(Domain owner, Term[] unifier)
		{
			p = new Precondition[10];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(0, 4, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(16, 4, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(17, 4, new TermList(owner.getTermVariable(2), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[4] = new PreconditionAtomic(new Predicate(1, 4, new TermList(owner.getTermVariable(3), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[5] = new PreconditionAtomic(new Predicate(18, 4, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[6] = new PreconditionAtomic(new Predicate(19, 4, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[7] = new PreconditionAtomic(new Predicate(4, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[8] = new PreconditionAtomic(new Predicate(20, 4, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[9] = new PreconditionAtomic(new Predicate(21, 4, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(0), TermList.NIL))), unifier);
			b = new Term[10][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
			b[6] = null;
			b[7] = null;
			b[8] = null;
			b[9] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[9] == null)
			{
				boolean b8changed = false;
				while (b[8] == null)
				{
					boolean b7changed = false;
					while (b[7] == null)
					{
						boolean b6changed = false;
						while (b[6] == null)
						{
							boolean b5changed = false;
							while (b[5] == null)
							{
								boolean b4changed = false;
								while (b[4] == null)
								{
									boolean b3changed = false;
									while (b[3] == null)
									{
										boolean b2changed = false;
										while (b[2] == null)
										{
											boolean b1changed = false;
											while (b[1] == null)
											{
												b[1] = p[1].nextBinding(state);
												if (b[1] == null)
													return null;
												else
													bestMatch = Math.max(bestMatch, 1);
												b1changed = true;
											}
											if ( b1changed ) {
												p[2].reset(state);
												p[2].bind(Term.merge(b, 2));
											}
											b[2] = p[2].nextBinding(state);
											if (b[2] == null)
												b[1] = null;
											else
												bestMatch = Math.max(bestMatch, 2);
											b2changed = true;
										}
										if ( b2changed ) {
											p[3].reset(state);
											p[3].bind(Term.merge(b, 3));
										}
										b[3] = p[3].nextBinding(state);
										if (b[3] == null)
											b[2] = null;
										else
											bestMatch = Math.max(bestMatch, 3);
										b3changed = true;
									}
									if ( b3changed ) {
										p[4].reset(state);
										p[4].bind(Term.merge(b, 4));
									}
									b[4] = p[4].nextBinding(state);
									if (b[4] == null)
										b[3] = null;
									else
										bestMatch = Math.max(bestMatch, 4);
									b4changed = true;
								}
								if ( b4changed ) {
									p[5].reset(state);
									p[5].bind(Term.merge(b, 5));
								}
								b[5] = p[5].nextBinding(state);
								if (b[5] == null)
									b[4] = null;
								else
									bestMatch = Math.max(bestMatch, 5);
								b5changed = true;
							}
							if ( b5changed ) {
								p[6].reset(state);
								p[6].bind(Term.merge(b, 6));
							}
							b[6] = p[6].nextBinding(state);
							if (b[6] == null)
								b[5] = null;
							else
								bestMatch = Math.max(bestMatch, 6);
							b6changed = true;
						}
						if ( b6changed ) {
							p[7].reset(state);
							p[7].bind(Term.merge(b, 7));
						}
						b[7] = p[7].nextBinding(state);
						if (b[7] == null)
							b[6] = null;
						else
							bestMatch = Math.max(bestMatch, 7);
						b7changed = true;
					}
					if ( b7changed ) {
						p[8].reset(state);
						p[8].bind(Term.merge(b, 8));
					}
					b[8] = p[8].nextBinding(state);
					if (b[8] == null)
						b[7] = null;
					else
						bestMatch = Math.max(bestMatch, 8);
					b8changed = true;
				}
				if ( b8changed ) {
					p[9].reset(state);
					p[9].bind(Term.merge(b, 9));
				}
				b[9] = p[9].nextBinding(state);
				if (b[9] == null)
					b[8] = null;
				else
					bestMatch = Math.max(bestMatch, 9);
			}

			Term[] retVal = Term.merge(b, 10);
			b[9] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			p[4].reset(state);
			p[5].reset(state);
			p[6].reset(state);
			p[7].reset(state);
			p[8].reset(state);
			p[9].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
			b[6] = null;
			b[7] = null;
			b[8] = null;
			b[9] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of Operator #-1 for primitive task !calibrate [unknown source pos]";
		}
	}

	/**
	 * Operator #-1 for primitive task !calibrate
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator27 extends Operator
{
	/**
	 * Operator #-1 for primitive task !calibrate
	 */
		public Operator27(Domain owner)
		{
			super(owner, new Predicate(4, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))))), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[0];

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[1];
			addIn[0] = new DelAddAtomic(new Predicate(22, 4, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(0), TermList.NIL))));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !calibrate [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new Precondition34(owner, unifier)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Precondition of Operator #-1 for primitive task !take_image
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition35 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition35(Domain owner, Term[] unifier)
		{
			p = new Precondition[12];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(0, 5, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(1, 5, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(17, 5, new TermList(owner.getTermVariable(2), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[4] = new PreconditionAtomic(new Predicate(16, 5, new TermList(owner.getTermVariable(3), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[5] = new PreconditionAtomic(new Predicate(23, 5, new TermList(owner.getTermVariable(4), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[6] = new PreconditionAtomic(new Predicate(22, 5, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(0), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[7] = new PreconditionAtomic(new Predicate(21, 5, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(0), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[8] = new PreconditionAtomic(new Predicate(18, 5, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[9] = new PreconditionAtomic(new Predicate(24, 5, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(4), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[10] = new PreconditionAtomic(new Predicate(20, 5, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[11] = new PreconditionAtomic(new Predicate(4, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier);
			b = new Term[12][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
			b[6] = null;
			b[7] = null;
			b[8] = null;
			b[9] = null;
			b[10] = null;
			b[11] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[11] == null)
			{
				boolean b10changed = false;
				while (b[10] == null)
				{
					boolean b9changed = false;
					while (b[9] == null)
					{
						boolean b8changed = false;
						while (b[8] == null)
						{
							boolean b7changed = false;
							while (b[7] == null)
							{
								boolean b6changed = false;
								while (b[6] == null)
								{
									boolean b5changed = false;
									while (b[5] == null)
									{
										boolean b4changed = false;
										while (b[4] == null)
										{
											boolean b3changed = false;
											while (b[3] == null)
											{
												boolean b2changed = false;
												while (b[2] == null)
												{
													boolean b1changed = false;
													while (b[1] == null)
													{
														b[1] = p[1].nextBinding(state);
														if (b[1] == null)
															return null;
														else
															bestMatch = Math.max(bestMatch, 1);
														b1changed = true;
													}
													if ( b1changed ) {
														p[2].reset(state);
														p[2].bind(Term.merge(b, 2));
													}
													b[2] = p[2].nextBinding(state);
													if (b[2] == null)
														b[1] = null;
													else
														bestMatch = Math.max(bestMatch, 2);
													b2changed = true;
												}
												if ( b2changed ) {
													p[3].reset(state);
													p[3].bind(Term.merge(b, 3));
												}
												b[3] = p[3].nextBinding(state);
												if (b[3] == null)
													b[2] = null;
												else
													bestMatch = Math.max(bestMatch, 3);
												b3changed = true;
											}
											if ( b3changed ) {
												p[4].reset(state);
												p[4].bind(Term.merge(b, 4));
											}
											b[4] = p[4].nextBinding(state);
											if (b[4] == null)
												b[3] = null;
											else
												bestMatch = Math.max(bestMatch, 4);
											b4changed = true;
										}
										if ( b4changed ) {
											p[5].reset(state);
											p[5].bind(Term.merge(b, 5));
										}
										b[5] = p[5].nextBinding(state);
										if (b[5] == null)
											b[4] = null;
										else
											bestMatch = Math.max(bestMatch, 5);
										b5changed = true;
									}
									if ( b5changed ) {
										p[6].reset(state);
										p[6].bind(Term.merge(b, 6));
									}
									b[6] = p[6].nextBinding(state);
									if (b[6] == null)
										b[5] = null;
									else
										bestMatch = Math.max(bestMatch, 6);
									b6changed = true;
								}
								if ( b6changed ) {
									p[7].reset(state);
									p[7].bind(Term.merge(b, 7));
								}
								b[7] = p[7].nextBinding(state);
								if (b[7] == null)
									b[6] = null;
								else
									bestMatch = Math.max(bestMatch, 7);
								b7changed = true;
							}
							if ( b7changed ) {
								p[8].reset(state);
								p[8].bind(Term.merge(b, 8));
							}
							b[8] = p[8].nextBinding(state);
							if (b[8] == null)
								b[7] = null;
							else
								bestMatch = Math.max(bestMatch, 8);
							b8changed = true;
						}
						if ( b8changed ) {
							p[9].reset(state);
							p[9].bind(Term.merge(b, 9));
						}
						b[9] = p[9].nextBinding(state);
						if (b[9] == null)
							b[8] = null;
						else
							bestMatch = Math.max(bestMatch, 9);
						b9changed = true;
					}
					if ( b9changed ) {
						p[10].reset(state);
						p[10].bind(Term.merge(b, 10));
					}
					b[10] = p[10].nextBinding(state);
					if (b[10] == null)
						b[9] = null;
					else
						bestMatch = Math.max(bestMatch, 10);
					b10changed = true;
				}
				if ( b10changed ) {
					p[11].reset(state);
					p[11].bind(Term.merge(b, 11));
				}
				b[11] = p[11].nextBinding(state);
				if (b[11] == null)
					b[10] = null;
				else
					bestMatch = Math.max(bestMatch, 11);
			}

			Term[] retVal = Term.merge(b, 12);
			b[11] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			p[4].reset(state);
			p[5].reset(state);
			p[6].reset(state);
			p[7].reset(state);
			p[8].reset(state);
			p[9].reset(state);
			p[10].reset(state);
			p[11].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
			b[6] = null;
			b[7] = null;
			b[8] = null;
			b[9] = null;
			b[10] = null;
			b[11] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of Operator #-1 for primitive task !take_image [unknown source pos]";
		}
	}

	/**
	 * Operator #-1 for primitive task !take_image
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator28 extends Operator
{
	/**
	 * Operator #-1 for primitive task !take_image
	 */
		public Operator28(Domain owner)
		{
			super(owner, new Predicate(5, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(4), TermList.NIL)))))), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[1];
			delIn[0] = new DelAddAtomic(new Predicate(22, 5, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(0), TermList.NIL))));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[1];
			addIn[0] = new DelAddAtomic(new Predicate(25, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(4), TermList.NIL)))));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !take_image [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new Precondition35(owner, unifier)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Precondition of Operator #-1 for primitive task !communicate_soil_data
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition36 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition36(Domain owner, Term[] unifier)
		{
			p = new Precondition[12];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(0, 5, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(26, 5, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(1, 5, new TermList(owner.getTermVariable(2), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[4] = new PreconditionAtomic(new Predicate(1, 5, new TermList(owner.getTermVariable(3), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[5] = new PreconditionAtomic(new Predicate(1, 5, new TermList(owner.getTermVariable(4), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[6] = new PreconditionAtomic(new Predicate(4, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[7] = new PreconditionAtomic(new Predicate(27, 5, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(4), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[8] = new PreconditionAtomic(new Predicate(12, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[9] = new PreconditionAtomic(new Predicate(5, 5, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(4), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[10] = new PreconditionAtomic(new Predicate(3, 5, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[11] = new PreconditionAtomic(new Predicate(28, 5, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			b = new Term[12][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
			b[6] = null;
			b[7] = null;
			b[8] = null;
			b[9] = null;
			b[10] = null;
			b[11] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[11] == null)
			{
				boolean b10changed = false;
				while (b[10] == null)
				{
					boolean b9changed = false;
					while (b[9] == null)
					{
						boolean b8changed = false;
						while (b[8] == null)
						{
							boolean b7changed = false;
							while (b[7] == null)
							{
								boolean b6changed = false;
								while (b[6] == null)
								{
									boolean b5changed = false;
									while (b[5] == null)
									{
										boolean b4changed = false;
										while (b[4] == null)
										{
											boolean b3changed = false;
											while (b[3] == null)
											{
												boolean b2changed = false;
												while (b[2] == null)
												{
													boolean b1changed = false;
													while (b[1] == null)
													{
														b[1] = p[1].nextBinding(state);
														if (b[1] == null)
															return null;
														else
															bestMatch = Math.max(bestMatch, 1);
														b1changed = true;
													}
													if ( b1changed ) {
														p[2].reset(state);
														p[2].bind(Term.merge(b, 2));
													}
													b[2] = p[2].nextBinding(state);
													if (b[2] == null)
														b[1] = null;
													else
														bestMatch = Math.max(bestMatch, 2);
													b2changed = true;
												}
												if ( b2changed ) {
													p[3].reset(state);
													p[3].bind(Term.merge(b, 3));
												}
												b[3] = p[3].nextBinding(state);
												if (b[3] == null)
													b[2] = null;
												else
													bestMatch = Math.max(bestMatch, 3);
												b3changed = true;
											}
											if ( b3changed ) {
												p[4].reset(state);
												p[4].bind(Term.merge(b, 4));
											}
											b[4] = p[4].nextBinding(state);
											if (b[4] == null)
												b[3] = null;
											else
												bestMatch = Math.max(bestMatch, 4);
											b4changed = true;
										}
										if ( b4changed ) {
											p[5].reset(state);
											p[5].bind(Term.merge(b, 5));
										}
										b[5] = p[5].nextBinding(state);
										if (b[5] == null)
											b[4] = null;
										else
											bestMatch = Math.max(bestMatch, 5);
										b5changed = true;
									}
									if ( b5changed ) {
										p[6].reset(state);
										p[6].bind(Term.merge(b, 6));
									}
									b[6] = p[6].nextBinding(state);
									if (b[6] == null)
										b[5] = null;
									else
										bestMatch = Math.max(bestMatch, 6);
									b6changed = true;
								}
								if ( b6changed ) {
									p[7].reset(state);
									p[7].bind(Term.merge(b, 7));
								}
								b[7] = p[7].nextBinding(state);
								if (b[7] == null)
									b[6] = null;
								else
									bestMatch = Math.max(bestMatch, 7);
								b7changed = true;
							}
							if ( b7changed ) {
								p[8].reset(state);
								p[8].bind(Term.merge(b, 8));
							}
							b[8] = p[8].nextBinding(state);
							if (b[8] == null)
								b[7] = null;
							else
								bestMatch = Math.max(bestMatch, 8);
							b8changed = true;
						}
						if ( b8changed ) {
							p[9].reset(state);
							p[9].bind(Term.merge(b, 9));
						}
						b[9] = p[9].nextBinding(state);
						if (b[9] == null)
							b[8] = null;
						else
							bestMatch = Math.max(bestMatch, 9);
						b9changed = true;
					}
					if ( b9changed ) {
						p[10].reset(state);
						p[10].bind(Term.merge(b, 10));
					}
					b[10] = p[10].nextBinding(state);
					if (b[10] == null)
						b[9] = null;
					else
						bestMatch = Math.max(bestMatch, 10);
					b10changed = true;
				}
				if ( b10changed ) {
					p[11].reset(state);
					p[11].bind(Term.merge(b, 11));
				}
				b[11] = p[11].nextBinding(state);
				if (b[11] == null)
					b[10] = null;
				else
					bestMatch = Math.max(bestMatch, 11);
			}

			Term[] retVal = Term.merge(b, 12);
			b[11] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			p[4].reset(state);
			p[5].reset(state);
			p[6].reset(state);
			p[7].reset(state);
			p[8].reset(state);
			p[9].reset(state);
			p[10].reset(state);
			p[11].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
			b[6] = null;
			b[7] = null;
			b[8] = null;
			b[9] = null;
			b[10] = null;
			b[11] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of Operator #-1 for primitive task !communicate_soil_data [unknown source pos]";
		}
	}

	/**
	 * Operator #-1 for primitive task !communicate_soil_data
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator29 extends Operator
{
	/**
	 * Operator #-1 for primitive task !communicate_soil_data
	 */
		public Operator29(Domain owner)
		{
			super(owner, new Predicate(6, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(4), TermList.NIL)))))), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[2];
			delIn[0] = new DelAddAtomic(new Predicate(3, 5, new TermList(owner.getTermVariable(0), TermList.NIL)));
			delIn[1] = new DelAddAtomic(new Predicate(28, 5, new TermList(owner.getTermVariable(1), TermList.NIL)));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[3];
			addIn[0] = new DelAddAtomic(new Predicate(28, 5, new TermList(owner.getTermVariable(1), TermList.NIL)));
			addIn[1] = new DelAddAtomic(new Predicate(29, 5, new TermList(owner.getTermVariable(2), TermList.NIL)));
			addIn[2] = new DelAddAtomic(new Predicate(3, 5, new TermList(owner.getTermVariable(0), TermList.NIL)));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !communicate_soil_data [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new Precondition36(owner, unifier)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Precondition of Operator #-1 for primitive task !communicate_rock_data
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition37 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition37(Domain owner, Term[] unifier)
		{
			p = new Precondition[12];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(0, 5, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(26, 5, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(1, 5, new TermList(owner.getTermVariable(2), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[4] = new PreconditionAtomic(new Predicate(1, 5, new TermList(owner.getTermVariable(3), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[5] = new PreconditionAtomic(new Predicate(1, 5, new TermList(owner.getTermVariable(4), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[6] = new PreconditionAtomic(new Predicate(4, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[7] = new PreconditionAtomic(new Predicate(27, 5, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(4), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[8] = new PreconditionAtomic(new Predicate(15, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[9] = new PreconditionAtomic(new Predicate(5, 5, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(4), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[10] = new PreconditionAtomic(new Predicate(3, 5, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[11] = new PreconditionAtomic(new Predicate(28, 5, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			b = new Term[12][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
			b[6] = null;
			b[7] = null;
			b[8] = null;
			b[9] = null;
			b[10] = null;
			b[11] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[11] == null)
			{
				boolean b10changed = false;
				while (b[10] == null)
				{
					boolean b9changed = false;
					while (b[9] == null)
					{
						boolean b8changed = false;
						while (b[8] == null)
						{
							boolean b7changed = false;
							while (b[7] == null)
							{
								boolean b6changed = false;
								while (b[6] == null)
								{
									boolean b5changed = false;
									while (b[5] == null)
									{
										boolean b4changed = false;
										while (b[4] == null)
										{
											boolean b3changed = false;
											while (b[3] == null)
											{
												boolean b2changed = false;
												while (b[2] == null)
												{
													boolean b1changed = false;
													while (b[1] == null)
													{
														b[1] = p[1].nextBinding(state);
														if (b[1] == null)
															return null;
														else
															bestMatch = Math.max(bestMatch, 1);
														b1changed = true;
													}
													if ( b1changed ) {
														p[2].reset(state);
														p[2].bind(Term.merge(b, 2));
													}
													b[2] = p[2].nextBinding(state);
													if (b[2] == null)
														b[1] = null;
													else
														bestMatch = Math.max(bestMatch, 2);
													b2changed = true;
												}
												if ( b2changed ) {
													p[3].reset(state);
													p[3].bind(Term.merge(b, 3));
												}
												b[3] = p[3].nextBinding(state);
												if (b[3] == null)
													b[2] = null;
												else
													bestMatch = Math.max(bestMatch, 3);
												b3changed = true;
											}
											if ( b3changed ) {
												p[4].reset(state);
												p[4].bind(Term.merge(b, 4));
											}
											b[4] = p[4].nextBinding(state);
											if (b[4] == null)
												b[3] = null;
											else
												bestMatch = Math.max(bestMatch, 4);
											b4changed = true;
										}
										if ( b4changed ) {
											p[5].reset(state);
											p[5].bind(Term.merge(b, 5));
										}
										b[5] = p[5].nextBinding(state);
										if (b[5] == null)
											b[4] = null;
										else
											bestMatch = Math.max(bestMatch, 5);
										b5changed = true;
									}
									if ( b5changed ) {
										p[6].reset(state);
										p[6].bind(Term.merge(b, 6));
									}
									b[6] = p[6].nextBinding(state);
									if (b[6] == null)
										b[5] = null;
									else
										bestMatch = Math.max(bestMatch, 6);
									b6changed = true;
								}
								if ( b6changed ) {
									p[7].reset(state);
									p[7].bind(Term.merge(b, 7));
								}
								b[7] = p[7].nextBinding(state);
								if (b[7] == null)
									b[6] = null;
								else
									bestMatch = Math.max(bestMatch, 7);
								b7changed = true;
							}
							if ( b7changed ) {
								p[8].reset(state);
								p[8].bind(Term.merge(b, 8));
							}
							b[8] = p[8].nextBinding(state);
							if (b[8] == null)
								b[7] = null;
							else
								bestMatch = Math.max(bestMatch, 8);
							b8changed = true;
						}
						if ( b8changed ) {
							p[9].reset(state);
							p[9].bind(Term.merge(b, 9));
						}
						b[9] = p[9].nextBinding(state);
						if (b[9] == null)
							b[8] = null;
						else
							bestMatch = Math.max(bestMatch, 9);
						b9changed = true;
					}
					if ( b9changed ) {
						p[10].reset(state);
						p[10].bind(Term.merge(b, 10));
					}
					b[10] = p[10].nextBinding(state);
					if (b[10] == null)
						b[9] = null;
					else
						bestMatch = Math.max(bestMatch, 10);
					b10changed = true;
				}
				if ( b10changed ) {
					p[11].reset(state);
					p[11].bind(Term.merge(b, 11));
				}
				b[11] = p[11].nextBinding(state);
				if (b[11] == null)
					b[10] = null;
				else
					bestMatch = Math.max(bestMatch, 11);
			}

			Term[] retVal = Term.merge(b, 12);
			b[11] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			p[4].reset(state);
			p[5].reset(state);
			p[6].reset(state);
			p[7].reset(state);
			p[8].reset(state);
			p[9].reset(state);
			p[10].reset(state);
			p[11].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
			b[6] = null;
			b[7] = null;
			b[8] = null;
			b[9] = null;
			b[10] = null;
			b[11] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of Operator #-1 for primitive task !communicate_rock_data [unknown source pos]";
		}
	}

	/**
	 * Operator #-1 for primitive task !communicate_rock_data
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator30 extends Operator
{
	/**
	 * Operator #-1 for primitive task !communicate_rock_data
	 */
		public Operator30(Domain owner)
		{
			super(owner, new Predicate(7, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(4), TermList.NIL)))))), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[2];
			delIn[0] = new DelAddAtomic(new Predicate(3, 5, new TermList(owner.getTermVariable(0), TermList.NIL)));
			delIn[1] = new DelAddAtomic(new Predicate(28, 5, new TermList(owner.getTermVariable(1), TermList.NIL)));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[3];
			addIn[0] = new DelAddAtomic(new Predicate(28, 5, new TermList(owner.getTermVariable(1), TermList.NIL)));
			addIn[1] = new DelAddAtomic(new Predicate(30, 5, new TermList(owner.getTermVariable(2), TermList.NIL)));
			addIn[2] = new DelAddAtomic(new Predicate(3, 5, new TermList(owner.getTermVariable(0), TermList.NIL)));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !communicate_rock_data [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new Precondition37(owner, unifier)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Precondition of Operator #-1 for primitive task !communicate_image_data
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition38 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition38(Domain owner, Term[] unifier)
		{
			p = new Precondition[13];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(0, 6, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(26, 6, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(17, 6, new TermList(owner.getTermVariable(2), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[4] = new PreconditionAtomic(new Predicate(23, 6, new TermList(owner.getTermVariable(3), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[5] = new PreconditionAtomic(new Predicate(1, 6, new TermList(owner.getTermVariable(4), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[6] = new PreconditionAtomic(new Predicate(1, 6, new TermList(owner.getTermVariable(5), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[7] = new PreconditionAtomic(new Predicate(4, 6, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(4), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[8] = new PreconditionAtomic(new Predicate(27, 6, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(5), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[9] = new PreconditionAtomic(new Predicate(25, 6, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL)))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[10] = new PreconditionAtomic(new Predicate(5, 6, new TermList(owner.getTermVariable(4), new TermList(owner.getTermVariable(5), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[11] = new PreconditionAtomic(new Predicate(3, 6, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[12] = new PreconditionAtomic(new Predicate(28, 6, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			b = new Term[13][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
			b[6] = null;
			b[7] = null;
			b[8] = null;
			b[9] = null;
			b[10] = null;
			b[11] = null;
			b[12] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[12] == null)
			{
				boolean b11changed = false;
				while (b[11] == null)
				{
					boolean b10changed = false;
					while (b[10] == null)
					{
						boolean b9changed = false;
						while (b[9] == null)
						{
							boolean b8changed = false;
							while (b[8] == null)
							{
								boolean b7changed = false;
								while (b[7] == null)
								{
									boolean b6changed = false;
									while (b[6] == null)
									{
										boolean b5changed = false;
										while (b[5] == null)
										{
											boolean b4changed = false;
											while (b[4] == null)
											{
												boolean b3changed = false;
												while (b[3] == null)
												{
													boolean b2changed = false;
													while (b[2] == null)
													{
														boolean b1changed = false;
														while (b[1] == null)
														{
															b[1] = p[1].nextBinding(state);
															if (b[1] == null)
																return null;
															else
																bestMatch = Math.max(bestMatch, 1);
															b1changed = true;
														}
														if ( b1changed ) {
															p[2].reset(state);
															p[2].bind(Term.merge(b, 2));
														}
														b[2] = p[2].nextBinding(state);
														if (b[2] == null)
															b[1] = null;
														else
															bestMatch = Math.max(bestMatch, 2);
														b2changed = true;
													}
													if ( b2changed ) {
														p[3].reset(state);
														p[3].bind(Term.merge(b, 3));
													}
													b[3] = p[3].nextBinding(state);
													if (b[3] == null)
														b[2] = null;
													else
														bestMatch = Math.max(bestMatch, 3);
													b3changed = true;
												}
												if ( b3changed ) {
													p[4].reset(state);
													p[4].bind(Term.merge(b, 4));
												}
												b[4] = p[4].nextBinding(state);
												if (b[4] == null)
													b[3] = null;
												else
													bestMatch = Math.max(bestMatch, 4);
												b4changed = true;
											}
											if ( b4changed ) {
												p[5].reset(state);
												p[5].bind(Term.merge(b, 5));
											}
											b[5] = p[5].nextBinding(state);
											if (b[5] == null)
												b[4] = null;
											else
												bestMatch = Math.max(bestMatch, 5);
											b5changed = true;
										}
										if ( b5changed ) {
											p[6].reset(state);
											p[6].bind(Term.merge(b, 6));
										}
										b[6] = p[6].nextBinding(state);
										if (b[6] == null)
											b[5] = null;
										else
											bestMatch = Math.max(bestMatch, 6);
										b6changed = true;
									}
									if ( b6changed ) {
										p[7].reset(state);
										p[7].bind(Term.merge(b, 7));
									}
									b[7] = p[7].nextBinding(state);
									if (b[7] == null)
										b[6] = null;
									else
										bestMatch = Math.max(bestMatch, 7);
									b7changed = true;
								}
								if ( b7changed ) {
									p[8].reset(state);
									p[8].bind(Term.merge(b, 8));
								}
								b[8] = p[8].nextBinding(state);
								if (b[8] == null)
									b[7] = null;
								else
									bestMatch = Math.max(bestMatch, 8);
								b8changed = true;
							}
							if ( b8changed ) {
								p[9].reset(state);
								p[9].bind(Term.merge(b, 9));
							}
							b[9] = p[9].nextBinding(state);
							if (b[9] == null)
								b[8] = null;
							else
								bestMatch = Math.max(bestMatch, 9);
							b9changed = true;
						}
						if ( b9changed ) {
							p[10].reset(state);
							p[10].bind(Term.merge(b, 10));
						}
						b[10] = p[10].nextBinding(state);
						if (b[10] == null)
							b[9] = null;
						else
							bestMatch = Math.max(bestMatch, 10);
						b10changed = true;
					}
					if ( b10changed ) {
						p[11].reset(state);
						p[11].bind(Term.merge(b, 11));
					}
					b[11] = p[11].nextBinding(state);
					if (b[11] == null)
						b[10] = null;
					else
						bestMatch = Math.max(bestMatch, 11);
					b11changed = true;
				}
				if ( b11changed ) {
					p[12].reset(state);
					p[12].bind(Term.merge(b, 12));
				}
				b[12] = p[12].nextBinding(state);
				if (b[12] == null)
					b[11] = null;
				else
					bestMatch = Math.max(bestMatch, 12);
			}

			Term[] retVal = Term.merge(b, 13);
			b[12] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			p[4].reset(state);
			p[5].reset(state);
			p[6].reset(state);
			p[7].reset(state);
			p[8].reset(state);
			p[9].reset(state);
			p[10].reset(state);
			p[11].reset(state);
			p[12].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
			b[6] = null;
			b[7] = null;
			b[8] = null;
			b[9] = null;
			b[10] = null;
			b[11] = null;
			b[12] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of Operator #-1 for primitive task !communicate_image_data [unknown source pos]";
		}
	}

	/**
	 * Operator #-1 for primitive task !communicate_image_data
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator31 extends Operator
{
	/**
	 * Operator #-1 for primitive task !communicate_image_data
	 */
		public Operator31(Domain owner)
		{
			super(owner, new Predicate(8, 6, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(4), new TermList(owner.getTermVariable(5), TermList.NIL))))))), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[2];
			delIn[0] = new DelAddAtomic(new Predicate(3, 6, new TermList(owner.getTermVariable(0), TermList.NIL)));
			delIn[1] = new DelAddAtomic(new Predicate(28, 6, new TermList(owner.getTermVariable(1), TermList.NIL)));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[3];
			addIn[0] = new DelAddAtomic(new Predicate(28, 6, new TermList(owner.getTermVariable(1), TermList.NIL)));
			addIn[1] = new DelAddAtomic(new Predicate(31, 6, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))));
			addIn[2] = new DelAddAtomic(new Predicate(3, 6, new TermList(owner.getTermVariable(0), TermList.NIL)));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !communicate_image_data [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new Precondition38(owner, unifier)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Operator #-1 for primitive task !visit
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator32 extends Operator
{
	/**
	 * Operator #-1 for primitive task !visit
	 */
		public Operator32(Domain owner)
		{
			super(owner, new Predicate(9, 1, new TermList(owner.getTermVariable(0), TermList.NIL)), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[0];

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[1];
			addIn[0] = new DelAddAtomic(new Predicate(32, 1, new TermList(owner.getTermVariable(0), TermList.NIL)));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !visit [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(1)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Operator #-1 for primitive task !unvisit
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator33 extends Operator
{
	/**
	 * Operator #-1 for primitive task !unvisit
	 */
		public Operator33(Domain owner)
		{
			super(owner, new Predicate(10, 1, new TermList(owner.getTermVariable(0), TermList.NIL)), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[1];
			delIn[0] = new DelAddAtomic(new Predicate(32, 1, new TermList(owner.getTermVariable(0), TermList.NIL)));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[0];

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !unvisit [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(1)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Method -1 for compound task empty-store
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method24 extends Method
	{
	/**
	 * Method -1 for compound task empty-store
	 */
		public Method24(Domain owner)
		{
			super(owner, new Predicate(0, 2, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			TaskList[] subsIn = new TaskList[2];

			subsIn[0] = TaskList.empty;
			subsIn[1] = createTaskList1();

			setSubs(subsIn);
		}

		TaskList createTaskList1()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(3, 2, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(0), TermList.NIL))), false, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task empty-store [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionAtomic(new Predicate(10, 2, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier)).setComparator(null);
				break;
				case 1:
					p = (new PreconditionNil(2)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
				case 1:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Case1";
				case 1: return "Case2";
				default: return null;
			}
		}
	}

	/**
	 * Method -1 for compound task navigate
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method25 extends Method
	{
	/**
	 * Method -1 for compound task navigate
	 */
		public Method25(Domain owner)
		{
			super(owner, new Predicate(1, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(3, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(9, 3, new TermList(owner.getTermVariable(2), TermList.NIL)), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(1, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(1), TermList.NIL)))), false, false));
			retVal.subtasks[2] = new TaskList(new TaskAtom(new Predicate(10, 3, new TermList(owner.getTermVariable(2), TermList.NIL)), false, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task navigate [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionAtomic(new Predicate(4, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method1Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #2 of Method -1 for compound task navigate
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition39 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition39(Domain owner, Term[] unifier)
		{
			p = new Precondition[4];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(1, 4, new TermList(owner.getTermVariable(3), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(2, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(3), TermList.NIL)))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[3] = new PreconditionNegation(new PreconditionAtomic(new Predicate(32, 4, new TermList(owner.getTermVariable(3), TermList.NIL)), unifier), 4);
			b = new Term[4][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[3] == null)
			{
				boolean b2changed = false;
				while (b[2] == null)
				{
					boolean b1changed = false;
					while (b[1] == null)
					{
						b[1] = p[1].nextBinding(state);
						if (b[1] == null)
							return null;
						else
							bestMatch = Math.max(bestMatch, 1);
						b1changed = true;
					}
					if ( b1changed ) {
						p[2].reset(state);
						p[2].bind(Term.merge(b, 2));
					}
					b[2] = p[2].nextBinding(state);
					if (b[2] == null)
						b[1] = null;
					else
						bestMatch = Math.max(bestMatch, 2);
					b2changed = true;
				}
				if ( b2changed ) {
					p[3].reset(state);
					p[3].bind(Term.merge(b, 3));
				}
				b[3] = p[3].nextBinding(state);
				if (b[3] == null)
					b[2] = null;
				else
					bestMatch = Math.max(bestMatch, 3);
			}

			Term[] retVal = Term.merge(b, 4);
			b[3] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #2 of Method -1 for compound task navigate [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task navigate
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method26 extends Method
	{
	/**
	 * Method -1 for compound task navigate
	 */
		public Method26(Domain owner)
		{
			super(owner, new Predicate(1, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL)))));
			TaskList[] subsIn = new TaskList[3];

			subsIn[0] = TaskList.empty;
			subsIn[1] = createTaskList1();
			subsIn[2] = createTaskList2();

			setSubs(subsIn);
		}

		TaskList createTaskList1()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL)))), false, true));

			return retVal;
		}

		TaskList createTaskList2()
		{
			TaskList retVal;

			retVal = new TaskList(4, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(3), TermList.NIL)))), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(9, 4, new TermList(owner.getTermVariable(3), TermList.NIL)), false, true));
			retVal.subtasks[2] = new TaskList(new TaskAtom(new Predicate(1, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(2), TermList.NIL)))), false, false));
			retVal.subtasks[3] = new TaskList(new TaskAtom(new Predicate(10, 4, new TermList(owner.getTermVariable(3), TermList.NIL)), false, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task navigate [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionAtomic(new Predicate(4, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier)).setComparator(null);
				break;
				case 1:
					p = (new PreconditionAtomic(new Predicate(2, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL)))), unifier)).setComparator(null);
				break;
				case 2:
					p = (new Precondition39(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
				case 1:
				break;
				case 2:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Case1";
				case 1: return "Case2";
				case 2: return "Case3";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task send_soil_data
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition40 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition40(Domain owner, Term[] unifier)
		{
			p = new Precondition[4];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(26, 5, new TermList(owner.getTermVariable(2), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(27, 5, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(5, 5, new TermList(owner.getTermVariable(4), new TermList(owner.getTermVariable(3), TermList.NIL))), unifier);
			b = new Term[4][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[3] == null)
			{
				boolean b2changed = false;
				while (b[2] == null)
				{
					boolean b1changed = false;
					while (b[1] == null)
					{
						b[1] = p[1].nextBinding(state);
						if (b[1] == null)
							return null;
						else
							bestMatch = Math.max(bestMatch, 1);
						b1changed = true;
					}
					if ( b1changed ) {
						p[2].reset(state);
						p[2].bind(Term.merge(b, 2));
					}
					b[2] = p[2].nextBinding(state);
					if (b[2] == null)
						b[1] = null;
					else
						bestMatch = Math.max(bestMatch, 2);
					b2changed = true;
				}
				if ( b2changed ) {
					p[3].reset(state);
					p[3].bind(Term.merge(b, 3));
				}
				b[3] = p[3].nextBinding(state);
				if (b[3] == null)
					b[2] = null;
				else
					bestMatch = Math.max(bestMatch, 3);
			}

			Term[] retVal = Term.merge(b, 4);
			b[3] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task send_soil_data [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task send_soil_data
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method27 extends Method
	{
	/**
	 * Method -1 for compound task send_soil_data
	 */
		public Method27(Domain owner)
		{
			super(owner, new Predicate(2, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(2, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(1, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(4), TermList.NIL))), false, false));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(6, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(4), new TermList(owner.getTermVariable(3), TermList.NIL)))))), false, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task send_soil_data [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition40(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method3Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task get_soil_data
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition41 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition41(Domain owner, Term[] unifier)
		{
			p = new Precondition[5];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(1, 3, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(0, 3, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(9, 3, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[4] = new PreconditionAtomic(new Predicate(8, 3, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			b = new Term[5][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[4] == null)
			{
				boolean b3changed = false;
				while (b[3] == null)
				{
					boolean b2changed = false;
					while (b[2] == null)
					{
						boolean b1changed = false;
						while (b[1] == null)
						{
							b[1] = p[1].nextBinding(state);
							if (b[1] == null)
								return null;
							else
								bestMatch = Math.max(bestMatch, 1);
							b1changed = true;
						}
						if ( b1changed ) {
							p[2].reset(state);
							p[2].bind(Term.merge(b, 2));
						}
						b[2] = p[2].nextBinding(state);
						if (b[2] == null)
							b[1] = null;
						else
							bestMatch = Math.max(bestMatch, 2);
						b2changed = true;
					}
					if ( b2changed ) {
						p[3].reset(state);
						p[3].bind(Term.merge(b, 3));
					}
					b[3] = p[3].nextBinding(state);
					if (b[3] == null)
						b[2] = null;
					else
						bestMatch = Math.max(bestMatch, 3);
					b3changed = true;
				}
				if ( b3changed ) {
					p[4].reset(state);
					p[4].bind(Term.merge(b, 4));
				}
				b[4] = p[4].nextBinding(state);
				if (b[4] == null)
					b[3] = null;
				else
					bestMatch = Math.max(bestMatch, 4);
			}

			Term[] retVal = Term.merge(b, 5);
			b[4] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			p[4].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task get_soil_data [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task get_soil_data
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method28 extends Method
	{
	/**
	 * Method -1 for compound task get_soil_data
	 */
		public Method28(Domain owner)
		{
			super(owner, new Predicate(3, 3, new TermList(owner.getTermVariable(0), TermList.NIL)));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(4, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(1, 3, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(0), TermList.NIL))), false, false));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(0, 3, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(1), TermList.NIL))), false, false));
			retVal.subtasks[2] = new TaskList(new TaskAtom(new Predicate(1, 3, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(0), TermList.NIL)))), false, true));
			retVal.subtasks[3] = new TaskList(new TaskAtom(new Predicate(2, 3, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(0), TermList.NIL))), false, false));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task get_soil_data [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition41(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method4Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task send_rock_data
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition42 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition42(Domain owner, Term[] unifier)
		{
			p = new Precondition[4];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(26, 5, new TermList(owner.getTermVariable(2), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(27, 5, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(5, 5, new TermList(owner.getTermVariable(4), new TermList(owner.getTermVariable(3), TermList.NIL))), unifier);
			b = new Term[4][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[3] == null)
			{
				boolean b2changed = false;
				while (b[2] == null)
				{
					boolean b1changed = false;
					while (b[1] == null)
					{
						b[1] = p[1].nextBinding(state);
						if (b[1] == null)
							return null;
						else
							bestMatch = Math.max(bestMatch, 1);
						b1changed = true;
					}
					if ( b1changed ) {
						p[2].reset(state);
						p[2].bind(Term.merge(b, 2));
					}
					b[2] = p[2].nextBinding(state);
					if (b[2] == null)
						b[1] = null;
					else
						bestMatch = Math.max(bestMatch, 2);
					b2changed = true;
				}
				if ( b2changed ) {
					p[3].reset(state);
					p[3].bind(Term.merge(b, 3));
				}
				b[3] = p[3].nextBinding(state);
				if (b[3] == null)
					b[2] = null;
				else
					bestMatch = Math.max(bestMatch, 3);
			}

			Term[] retVal = Term.merge(b, 4);
			b[3] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task send_rock_data [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task send_rock_data
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method29 extends Method
	{
	/**
	 * Method -1 for compound task send_rock_data
	 */
		public Method29(Domain owner)
		{
			super(owner, new Predicate(4, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(2, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(1, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(4), TermList.NIL))), false, false));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(7, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(4), new TermList(owner.getTermVariable(3), TermList.NIL)))))), false, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task send_rock_data [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition42(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method5Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task get_rock_data
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition43 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition43(Domain owner, Term[] unifier)
		{
			p = new Precondition[5];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(1, 3, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(0, 3, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(14, 3, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[4] = new PreconditionAtomic(new Predicate(9, 3, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier);
			b = new Term[5][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[4] == null)
			{
				boolean b3changed = false;
				while (b[3] == null)
				{
					boolean b2changed = false;
					while (b[2] == null)
					{
						boolean b1changed = false;
						while (b[1] == null)
						{
							b[1] = p[1].nextBinding(state);
							if (b[1] == null)
								return null;
							else
								bestMatch = Math.max(bestMatch, 1);
							b1changed = true;
						}
						if ( b1changed ) {
							p[2].reset(state);
							p[2].bind(Term.merge(b, 2));
						}
						b[2] = p[2].nextBinding(state);
						if (b[2] == null)
							b[1] = null;
						else
							bestMatch = Math.max(bestMatch, 2);
						b2changed = true;
					}
					if ( b2changed ) {
						p[3].reset(state);
						p[3].bind(Term.merge(b, 3));
					}
					b[3] = p[3].nextBinding(state);
					if (b[3] == null)
						b[2] = null;
					else
						bestMatch = Math.max(bestMatch, 3);
					b3changed = true;
				}
				if ( b3changed ) {
					p[4].reset(state);
					p[4].bind(Term.merge(b, 4));
				}
				b[4] = p[4].nextBinding(state);
				if (b[4] == null)
					b[3] = null;
				else
					bestMatch = Math.max(bestMatch, 4);
			}

			Term[] retVal = Term.merge(b, 5);
			b[4] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			p[4].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task get_rock_data [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task get_rock_data
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method30 extends Method
	{
	/**
	 * Method -1 for compound task get_rock_data
	 */
		public Method30(Domain owner)
		{
			super(owner, new Predicate(5, 3, new TermList(owner.getTermVariable(0), TermList.NIL)));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(4, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(1, 3, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(0), TermList.NIL))), false, false));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(0, 3, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(1), TermList.NIL))), false, false));
			retVal.subtasks[2] = new TaskList(new TaskAtom(new Predicate(2, 3, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(0), TermList.NIL)))), false, true));
			retVal.subtasks[3] = new TaskList(new TaskAtom(new Predicate(4, 3, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(0), TermList.NIL))), false, false));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task get_rock_data [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition43(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method6Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task send_image_data
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition44 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition44(Domain owner, Term[] unifier)
		{
			p = new Precondition[4];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(26, 6, new TermList(owner.getTermVariable(3), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(27, 6, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(4), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(5, 6, new TermList(owner.getTermVariable(5), new TermList(owner.getTermVariable(4), TermList.NIL))), unifier);
			b = new Term[4][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[3] == null)
			{
				boolean b2changed = false;
				while (b[2] == null)
				{
					boolean b1changed = false;
					while (b[1] == null)
					{
						b[1] = p[1].nextBinding(state);
						if (b[1] == null)
							return null;
						else
							bestMatch = Math.max(bestMatch, 1);
						b1changed = true;
					}
					if ( b1changed ) {
						p[2].reset(state);
						p[2].bind(Term.merge(b, 2));
					}
					b[2] = p[2].nextBinding(state);
					if (b[2] == null)
						b[1] = null;
					else
						bestMatch = Math.max(bestMatch, 2);
					b2changed = true;
				}
				if ( b2changed ) {
					p[3].reset(state);
					p[3].bind(Term.merge(b, 3));
				}
				b[3] = p[3].nextBinding(state);
				if (b[3] == null)
					b[2] = null;
				else
					bestMatch = Math.max(bestMatch, 3);
			}

			Term[] retVal = Term.merge(b, 4);
			b[3] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task send_image_data [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task send_image_data
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method31 extends Method
	{
	/**
	 * Method -1 for compound task send_image_data
	 */
		public Method31(Domain owner)
		{
			super(owner, new Predicate(6, 6, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL)))));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(2, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(1, 6, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(5), TermList.NIL))), false, false));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(8, 6, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(5), new TermList(owner.getTermVariable(4), TermList.NIL))))))), false, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task send_image_data [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition44(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method7Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task get_image_data
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition45 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition45(Domain owner, Term[] unifier)
		{
			p = new Precondition[6];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(17, 5, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(18, 5, new TermList(owner.getTermVariable(2), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(21, 5, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[4] = new PreconditionAtomic(new Predicate(24, 5, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[5] = new PreconditionAtomic(new Predicate(20, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(4), TermList.NIL))), unifier);
			b = new Term[6][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[5] == null)
			{
				boolean b4changed = false;
				while (b[4] == null)
				{
					boolean b3changed = false;
					while (b[3] == null)
					{
						boolean b2changed = false;
						while (b[2] == null)
						{
							boolean b1changed = false;
							while (b[1] == null)
							{
								b[1] = p[1].nextBinding(state);
								if (b[1] == null)
									return null;
								else
									bestMatch = Math.max(bestMatch, 1);
								b1changed = true;
							}
							if ( b1changed ) {
								p[2].reset(state);
								p[2].bind(Term.merge(b, 2));
							}
							b[2] = p[2].nextBinding(state);
							if (b[2] == null)
								b[1] = null;
							else
								bestMatch = Math.max(bestMatch, 2);
							b2changed = true;
						}
						if ( b2changed ) {
							p[3].reset(state);
							p[3].bind(Term.merge(b, 3));
						}
						b[3] = p[3].nextBinding(state);
						if (b[3] == null)
							b[2] = null;
						else
							bestMatch = Math.max(bestMatch, 3);
						b3changed = true;
					}
					if ( b3changed ) {
						p[4].reset(state);
						p[4].bind(Term.merge(b, 4));
					}
					b[4] = p[4].nextBinding(state);
					if (b[4] == null)
						b[3] = null;
					else
						bestMatch = Math.max(bestMatch, 4);
					b4changed = true;
				}
				if ( b4changed ) {
					p[5].reset(state);
					p[5].bind(Term.merge(b, 5));
				}
				b[5] = p[5].nextBinding(state);
				if (b[5] == null)
					b[4] = null;
				else
					bestMatch = Math.max(bestMatch, 5);
			}

			Term[] retVal = Term.merge(b, 6);
			b[5] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			p[4].reset(state);
			p[5].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task get_image_data [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task get_image_data
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method32 extends Method
	{
	/**
	 * Method -1 for compound task get_image_data
	 */
		public Method32(Domain owner)
		{
			super(owner, new Predicate(8, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(4, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(7, 5, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))), false, false));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(1, 5, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(4), TermList.NIL))), false, false));
			retVal.subtasks[2] = new TaskList(new TaskAtom(new Predicate(5, 5, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(4), new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(1), TermList.NIL)))))), false, true));
			retVal.subtasks[3] = new TaskList(new TaskAtom(new Predicate(6, 5, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL)))), false, false));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task get_image_data [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition45(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method8Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task calibrate
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition46 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition46(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(19, 4, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(20, 4, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task calibrate [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task calibrate
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method33 extends Method
	{
	/**
	 * Method -1 for compound task calibrate
	 */
		public Method33(Domain owner)
		{
			super(owner, new Predicate(7, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(2, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(1, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), TermList.NIL))), false, false));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(4, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))))), false, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task calibrate [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition46(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method9Branch0";
				default: return null;
			}
		}
	}

	public static final String sourcePath = "/Users/gcc/workspace/jshop2cp/examples/rover/rover";
	public static final long sourceLastModified = 1328666443000L;

	public rover()
	{
		constants = new String[33];
		constants[0] = "rover";
		constants[1] = "waypoint";
		constants[2] = "can_traverse";
		constants[3] = "available";
		constants[4] = "at";
		constants[5] = "visible";
		constants[6] = "store";
		constants[7] = "at_soil_sample";
		constants[8] = "equipped_for_soil_analysis";
		constants[9] = "store_of";
		constants[10] = "empty";
		constants[11] = "full";
		constants[12] = "have_soil_analysis";
		constants[13] = "at_rock_sample";
		constants[14] = "equipped_for_rock_analysis";
		constants[15] = "have_rock_analysis";
		constants[16] = "camera";
		constants[17] = "objective";
		constants[18] = "equipped_for_imaging";
		constants[19] = "calibration_target";
		constants[20] = "visible_from";
		constants[21] = "on_board";
		constants[22] = "calibrated";
		constants[23] = "mode";
		constants[24] = "supports";
		constants[25] = "have_image";
		constants[26] = "lander";
		constants[27] = "at_lander";
		constants[28] = "channel_free";
		constants[29] = "communicated_soil_data";
		constants[30] = "communicated_rock_data";
		constants[31] = "communicated_image_data";
		constants[32] = "visited";

		compoundTasks = new String[9];
		compoundTasks[0] = "empty-store";
		compoundTasks[1] = "navigate";
		compoundTasks[2] = "send_soil_data";
		compoundTasks[3] = "get_soil_data";
		compoundTasks[4] = "send_rock_data";
		compoundTasks[5] = "get_rock_data";
		compoundTasks[6] = "send_image_data";
		compoundTasks[7] = "calibrate";
		compoundTasks[8] = "get_image_data";

		primitiveTasks = new String[11];
		primitiveTasks[0] = "!navigate";
		primitiveTasks[1] = "!sample_soil";
		primitiveTasks[2] = "!sample_rock";
		primitiveTasks[3] = "!drop";
		primitiveTasks[4] = "!calibrate";
		primitiveTasks[5] = "!take_image";
		primitiveTasks[6] = "!communicate_soil_data";
		primitiveTasks[7] = "!communicate_rock_data";
		primitiveTasks[8] = "!communicate_image_data";
		primitiveTasks[9] = "!visit";
		primitiveTasks[10] = "!unvisit";

		initializeTermVariables(6);

		initializeTermConstants();

		methods = new Method[9][];

		methods[0] = new Method[1];
		methods[0][0] = new Method24(this);

		methods[1] = new Method[2];
		methods[1][0] = new Method25(this);
		methods[1][1] = new Method26(this);

		methods[2] = new Method[1];
		methods[2][0] = new Method27(this);

		methods[3] = new Method[1];
		methods[3][0] = new Method28(this);

		methods[4] = new Method[1];
		methods[4][0] = new Method29(this);

		methods[5] = new Method[1];
		methods[5][0] = new Method30(this);

		methods[6] = new Method[1];
		methods[6][0] = new Method31(this);

		methods[7] = new Method[1];
		methods[7][0] = new Method33(this);

		methods[8] = new Method[1];
		methods[8][0] = new Method32(this);


		ops = new Operator[11][];

		ops[0] = new Operator[1];
		ops[0][0] = new Operator23(this);

		ops[1] = new Operator[1];
		ops[1][0] = new Operator24(this);

		ops[2] = new Operator[1];
		ops[2][0] = new Operator25(this);

		ops[3] = new Operator[1];
		ops[3][0] = new Operator26(this);

		ops[4] = new Operator[1];
		ops[4][0] = new Operator27(this);

		ops[5] = new Operator[1];
		ops[5][0] = new Operator28(this);

		ops[6] = new Operator[1];
		ops[6][0] = new Operator29(this);

		ops[7] = new Operator[1];
		ops[7][0] = new Operator30(this);

		ops[8] = new Operator[1];
		ops[8][0] = new Operator31(this);

		ops[9] = new Operator[1];
		ops[9][0] = new Operator32(this);

		ops[10] = new Operator[1];
		ops[10][0] = new Operator33(this);

		axioms = new Axiom[33][];

		axioms[0] = new Axiom[0];

		axioms[1] = new Axiom[0];

		axioms[2] = new Axiom[0];

		axioms[3] = new Axiom[0];

		axioms[4] = new Axiom[0];

		axioms[5] = new Axiom[0];

		axioms[6] = new Axiom[0];

		axioms[7] = new Axiom[0];

		axioms[8] = new Axiom[0];

		axioms[9] = new Axiom[0];

		axioms[10] = new Axiom[0];

		axioms[11] = new Axiom[0];

		axioms[12] = new Axiom[0];

		axioms[13] = new Axiom[0];

		axioms[14] = new Axiom[0];

		axioms[15] = new Axiom[0];

		axioms[16] = new Axiom[0];

		axioms[17] = new Axiom[0];

		axioms[18] = new Axiom[0];

		axioms[19] = new Axiom[0];

		axioms[20] = new Axiom[0];

		axioms[21] = new Axiom[0];

		axioms[22] = new Axiom[0];

		axioms[23] = new Axiom[0];

		axioms[24] = new Axiom[0];

		axioms[25] = new Axiom[0];

		axioms[26] = new Axiom[0];

		axioms[27] = new Axiom[0];

		axioms[28] = new Axiom[0];

		axioms[29] = new Axiom[0];

		axioms[30] = new Axiom[0];

		axioms[31] = new Axiom[0];

		axioms[32] = new Axiom[0];

	}
}