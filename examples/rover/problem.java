package rover;
import java.util.LinkedList;
import com.gamalocus.jshop2rt.*;

public class problem
{
	public static final String sourcePath = "/Users/gcc/workspace/jshop2cp/examples/rover/problem";
	public static final long sourceLastModified = 1328666449000L;

	public static final Domain owner = new rover();

	private static String[] defineConstants(Domain owner)
	{
		String[] problemConstants = new String[95];

		owner.addConstant("GENERAL");
		owner.addConstant("LANDER");
		owner.addConstant("COLOUR");
		owner.addConstant("MODE");
		owner.addConstant("HIGH_RES");
		owner.addConstant("LOW_RES");
		owner.addConstant("ROVER0");
		owner.addConstant("ROVER");
		owner.addConstant("ROVER1");
		owner.addConstant("ROVER2");
		owner.addConstant("ROVER3");
		owner.addConstant("ROVER4");
		owner.addConstant("ROVER5");
		owner.addConstant("ROVER6");
		owner.addConstant("ROVER7");
		owner.addConstant("ROVER0STORE");
		owner.addConstant("STORE");
		owner.addConstant("ROVER1STORE");
		owner.addConstant("ROVER2STORE");
		owner.addConstant("ROVER3STORE");
		owner.addConstant("ROVER4STORE");
		owner.addConstant("ROVER5STORE");
		owner.addConstant("ROVER6STORE");
		owner.addConstant("ROVER7STORE");
		owner.addConstant("WAYPOINT0");
		owner.addConstant("WAYPOINT");
		owner.addConstant("WAYPOINT1");
		owner.addConstant("WAYPOINT2");
		owner.addConstant("WAYPOINT3");
		owner.addConstant("WAYPOINT4");
		owner.addConstant("WAYPOINT5");
		owner.addConstant("WAYPOINT6");
		owner.addConstant("WAYPOINT7");
		owner.addConstant("WAYPOINT8");
		owner.addConstant("WAYPOINT9");
		owner.addConstant("WAYPOINT10");
		owner.addConstant("WAYPOINT11");
		owner.addConstant("WAYPOINT12");
		owner.addConstant("WAYPOINT13");
		owner.addConstant("WAYPOINT14");
		owner.addConstant("WAYPOINT15");
		owner.addConstant("WAYPOINT16");
		owner.addConstant("WAYPOINT17");
		owner.addConstant("WAYPOINT18");
		owner.addConstant("WAYPOINT19");
		owner.addConstant("WAYPOINT20");
		owner.addConstant("WAYPOINT21");
		owner.addConstant("WAYPOINT22");
		owner.addConstant("WAYPOINT23");
		owner.addConstant("WAYPOINT24");
		owner.addConstant("WAYPOINT25");
		owner.addConstant("WAYPOINT26");
		owner.addConstant("WAYPOINT27");
		owner.addConstant("WAYPOINT28");
		owner.addConstant("WAYPOINT29");
		owner.addConstant("CAMERA0");
		owner.addConstant("CAMERA");
		owner.addConstant("CAMERA1");
		owner.addConstant("CAMERA2");
		owner.addConstant("CAMERA3");
		owner.addConstant("CAMERA4");
		owner.addConstant("CAMERA5");
		owner.addConstant("CAMERA6");
		owner.addConstant("CAMERA7");
		owner.addConstant("CAMERA8");
		owner.addConstant("CAMERA9");
		owner.addConstant("CAMERA10");
		owner.addConstant("OBJECTIVE0");
		owner.addConstant("OBJECTIVE");
		owner.addConstant("OBJECTIVE1");
		owner.addConstant("OBJECTIVE2");
		owner.addConstant("OBJECTIVE3");
		owner.addConstant("OBJECTIVE4");
		owner.addConstant("OBJECTIVE5");
		owner.addConstant("OBJECTIVE6");
		owner.addConstant("OBJECTIVE7");
		owner.addConstant("OBJECTIVE8");
		owner.addConstant("OBJECTIVE9");
		owner.addConstant("VISIBLE");
		owner.addConstant("AT_SOIL_SAMPLE");
		owner.addConstant("AT_ROCK_SAMPLE");
		owner.addConstant("AT_LANDER");
		owner.addConstant("CHANNEL_FREE");
		owner.addConstant("AT");
		owner.addConstant("AVAILABLE");
		owner.addConstant("STORE_OF");
		owner.addConstant("EMPTY");
		owner.addConstant("EQUIPPED_FOR_ROCK_ANALYSIS");
		owner.addConstant("CAN_TRAVERSE");
		owner.addConstant("EQUIPPED_FOR_IMAGING");
		owner.addConstant("EQUIPPED_FOR_SOIL_ANALYSIS");
		owner.addConstant("ON_BOARD");
		owner.addConstant("CALIBRATION_TARGET");
		owner.addConstant("SUPPORTS");
		owner.addConstant("VISIBLE_FROM");

		return problemConstants;
	}

	private static void createState0(Domain owner, State s)	{
	}

	public static LinkedList<Plan> getPlans()
	{
		LinkedList<Plan> returnedPlans = new LinkedList<Plan>();
		defineConstants(owner);

		State s0 = new State(owner.getAxioms());

		createState0(owner, s0); // jShop2Planner.initialize(d, s0);

		TaskList tl;
			tl = new TaskList(25, true);
			tl.subtasks[0] = new TaskList(new TaskAtom(new Predicate(9, 0, new TermList(owner.getTermConstant("WAYPOINT5") /*63*/, TermList.NIL)), false, false));
			tl.subtasks[1] = new TaskList(new TaskAtom(new Predicate(9, 0, new TermList(owner.getTermConstant("WAYPOINT13") /*71*/, TermList.NIL)), false, false));
			tl.subtasks[2] = new TaskList(new TaskAtom(new Predicate(9, 0, new TermList(owner.getTermConstant("WAYPOINT4") /*62*/, TermList.NIL)), false, false));
			tl.subtasks[3] = new TaskList(new TaskAtom(new Predicate(9, 0, new TermList(owner.getTermConstant("WAYPOINT23") /*81*/, TermList.NIL)), false, false));
			tl.subtasks[4] = new TaskList(new TaskAtom(new Predicate(9, 0, new TermList(owner.getTermConstant("WAYPOINT15") /*73*/, TermList.NIL)), false, false));
			tl.subtasks[5] = new TaskList(new TaskAtom(new Predicate(9, 0, new TermList(owner.getTermConstant("WAYPOINT7") /*65*/, TermList.NIL)), false, false));
			tl.subtasks[6] = new TaskList(new TaskAtom(new Predicate(9, 0, new TermList(owner.getTermConstant("WAYPOINT20") /*78*/, TermList.NIL)), false, false));
			tl.subtasks[7] = new TaskList(new TaskAtom(new Predicate(9, 0, new TermList(owner.getTermConstant("WAYPOINT26") /*84*/, TermList.NIL)), false, false));
			tl.subtasks[8] = new TaskList(new TaskAtom(new Predicate(9, 0, new TermList(owner.getTermConstant("WAYPOINT2") /*60*/, TermList.NIL)), false, false));
			tl.subtasks[9] = new TaskList(new TaskAtom(new Predicate(10, 0, new TermList(owner.getTermConstant("WAYPOINT27") /*85*/, TermList.NIL)), false, false));
			tl.subtasks[10] = new TaskList(new TaskAtom(new Predicate(10, 0, new TermList(owner.getTermConstant("WAYPOINT19") /*77*/, TermList.NIL)), false, false));
			tl.subtasks[11] = new TaskList(new TaskAtom(new Predicate(10, 0, new TermList(owner.getTermConstant("WAYPOINT1") /*59*/, TermList.NIL)), false, false));
			tl.subtasks[12] = new TaskList(new TaskAtom(new Predicate(10, 0, new TermList(owner.getTermConstant("WAYPOINT3") /*61*/, TermList.NIL)), false, false));
			tl.subtasks[13] = new TaskList(new TaskAtom(new Predicate(10, 0, new TermList(owner.getTermConstant("WAYPOINT7") /*65*/, TermList.NIL)), false, false));
			tl.subtasks[14] = new TaskList(new TaskAtom(new Predicate(10, 0, new TermList(owner.getTermConstant("WAYPOINT20") /*78*/, TermList.NIL)), false, false));
			tl.subtasks[15] = new TaskList(new TaskAtom(new Predicate(10, 0, new TermList(owner.getTermConstant("WAYPOINT22") /*80*/, TermList.NIL)), false, false));
			tl.subtasks[16] = new TaskList(new TaskAtom(new Predicate(11, 0, new TermList(owner.getTermConstant("OBJECTIVE2") /*103*/, new TermList(owner.getTermConstant("LOW_RES") /*38*/, TermList.NIL))), false, false));
			tl.subtasks[17] = new TaskList(new TaskAtom(new Predicate(11, 0, new TermList(owner.getTermConstant("OBJECTIVE6") /*107*/, new TermList(owner.getTermConstant("LOW_RES") /*38*/, TermList.NIL))), false, false));
			tl.subtasks[18] = new TaskList(new TaskAtom(new Predicate(11, 0, new TermList(owner.getTermConstant("OBJECTIVE3") /*104*/, new TermList(owner.getTermConstant("LOW_RES") /*38*/, TermList.NIL))), false, false));
			tl.subtasks[19] = new TaskList(new TaskAtom(new Predicate(11, 0, new TermList(owner.getTermConstant("OBJECTIVE6") /*107*/, new TermList(owner.getTermConstant("COLOUR") /*35*/, TermList.NIL))), false, false));
			tl.subtasks[20] = new TaskList(new TaskAtom(new Predicate(11, 0, new TermList(owner.getTermConstant("OBJECTIVE3") /*104*/, new TermList(owner.getTermConstant("HIGH_RES") /*37*/, TermList.NIL))), false, false));
			tl.subtasks[21] = new TaskList(new TaskAtom(new Predicate(11, 0, new TermList(owner.getTermConstant("OBJECTIVE1") /*102*/, new TermList(owner.getTermConstant("COLOUR") /*35*/, TermList.NIL))), false, false));
			tl.subtasks[22] = new TaskList(new TaskAtom(new Predicate(11, 0, new TermList(owner.getTermConstant("OBJECTIVE6") /*107*/, new TermList(owner.getTermConstant("HIGH_RES") /*37*/, TermList.NIL))), false, false));
			tl.subtasks[23] = new TaskList(new TaskAtom(new Predicate(11, 0, new TermList(owner.getTermConstant("OBJECTIVE3") /*104*/, new TermList(owner.getTermConstant("COLOUR") /*35*/, TermList.NIL))), false, false));
			tl.subtasks[24] = new TaskList(new TaskAtom(new Predicate(11, 0, new TermList(owner.getTermConstant("OBJECTIVE2") /*103*/, new TermList(owner.getTermConstant("HIGH_RES") /*37*/, TermList.NIL))), false, false));

		JSHOP2 jShop2Planner = new JSHOP2(tl, 100000, new DoubleCost(0.0), owner, s0);
		JSHOP2Provider.setJSHOP2(jShop2Planner);
		while (jShop2Planner.run()) { 
		}
		returnedPlans.addAll(jShop2Planner.getPlans());
		return returnedPlans;
	}

	public static LinkedList<Predicate> getFirstPlanOps() {
		return getPlans().getFirst().getOps();
	}
}