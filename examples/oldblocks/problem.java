package oldblocks;
import java.util.LinkedList;
import com.gamalocus.jshop2rt.*;

public class problem
{
	public static final String sourcePath = "/Users/gcc/workspace/jshop2cp/examples/oldblocks/problem";
	public static final long sourceLastModified = 1328619440000L;

	public static final Domain owner = new oldblocks();

	private static String[] defineConstants(Domain owner)
	{
		String[] problemConstants = new String[10];

		owner.addConstant("b1");
		owner.addConstant("b2");
		owner.addConstant("b7");
		owner.addConstant("b3");
		owner.addConstant("b4");
		owner.addConstant("b10");
		owner.addConstant("b5");
		owner.addConstant("b6");
		owner.addConstant("b8");
		owner.addConstant("b9");

		return problemConstants;
	}

	private static void createState0(Domain owner, State s)	{
		s.add(new Predicate(1, 0, new TermList(owner.getTermConstant("b1") /*8*/, TermList.NIL)));
		s.add(new Predicate(3, 0, new TermList(owner.getTermConstant("b2") /*9*/, new TermList(owner.getTermConstant("b1") /*8*/, TermList.NIL))));
		s.add(new Predicate(3, 0, new TermList(owner.getTermConstant("b7") /*10*/, new TermList(owner.getTermConstant("b2") /*9*/, TermList.NIL))));
		s.add(new Predicate(0, 0, new TermList(owner.getTermConstant("b7") /*10*/, TermList.NIL)));
		s.add(new Predicate(1, 0, new TermList(owner.getTermConstant("b3") /*11*/, TermList.NIL)));
		s.add(new Predicate(3, 0, new TermList(owner.getTermConstant("b4") /*12*/, new TermList(owner.getTermConstant("b3") /*11*/, TermList.NIL))));
		s.add(new Predicate(3, 0, new TermList(owner.getTermConstant("b10") /*13*/, new TermList(owner.getTermConstant("b4") /*12*/, TermList.NIL))));
		s.add(new Predicate(0, 0, new TermList(owner.getTermConstant("b10") /*13*/, TermList.NIL)));
		s.add(new Predicate(1, 0, new TermList(owner.getTermConstant("b5") /*14*/, TermList.NIL)));
		s.add(new Predicate(3, 0, new TermList(owner.getTermConstant("b6") /*15*/, new TermList(owner.getTermConstant("b5") /*14*/, TermList.NIL))));
		s.add(new Predicate(0, 0, new TermList(owner.getTermConstant("b6") /*15*/, TermList.NIL)));
		s.add(new Predicate(1, 0, new TermList(owner.getTermConstant("b8") /*16*/, TermList.NIL)));
		s.add(new Predicate(0, 0, new TermList(owner.getTermConstant("b8") /*16*/, TermList.NIL)));
		s.add(new Predicate(1, 0, new TermList(owner.getTermConstant("b9") /*17*/, TermList.NIL)));
		s.add(new Predicate(0, 0, new TermList(owner.getTermConstant("b9") /*17*/, TermList.NIL)));
	}

	public static LinkedList<Plan> getPlans()
	{
		LinkedList<Plan> returnedPlans = new LinkedList<Plan>();
		defineConstants(owner);

		State s0 = new State(owner.getAxioms());

		createState0(owner, s0); // jShop2Planner.initialize(d, s0);

		TaskList tl;
			tl = new TaskList(1, true);
			tl.subtasks[0] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(new TermList(new TermList(owner.getTermConstant("on-table") /*1*/, new TermList(owner.getTermConstant("b1") /*8*/, TermList.NIL)), new TermList(new TermList(owner.getTermConstant("on") /*3*/, new TermList(owner.getTermConstant("b10") /*13*/, new TermList(owner.getTermConstant("b1") /*8*/, TermList.NIL))), new TermList(new TermList(owner.getTermConstant("clear") /*0*/, new TermList(owner.getTermConstant("b10") /*13*/, TermList.NIL)), new TermList(new TermList(owner.getTermConstant("on-table") /*1*/, new TermList(owner.getTermConstant("b2") /*9*/, TermList.NIL)), new TermList(new TermList(owner.getTermConstant("on") /*3*/, new TermList(owner.getTermConstant("b3") /*11*/, new TermList(owner.getTermConstant("b2") /*9*/, TermList.NIL))), new TermList(new TermList(owner.getTermConstant("on") /*3*/, new TermList(owner.getTermConstant("b4") /*12*/, new TermList(owner.getTermConstant("b3") /*11*/, TermList.NIL))), new TermList(new TermList(owner.getTermConstant("on") /*3*/, new TermList(owner.getTermConstant("b5") /*14*/, new TermList(owner.getTermConstant("b4") /*12*/, TermList.NIL))), new TermList(new TermList(owner.getTermConstant("clear") /*0*/, new TermList(owner.getTermConstant("b5") /*14*/, TermList.NIL)), new TermList(new TermList(owner.getTermConstant("on-table") /*1*/, new TermList(owner.getTermConstant("b6") /*15*/, TermList.NIL)), new TermList(new TermList(owner.getTermConstant("on") /*3*/, new TermList(owner.getTermConstant("b7") /*10*/, new TermList(owner.getTermConstant("b6") /*15*/, TermList.NIL))), new TermList(new TermList(owner.getTermConstant("on") /*3*/, new TermList(owner.getTermConstant("b8") /*16*/, new TermList(owner.getTermConstant("b7") /*10*/, TermList.NIL))), new TermList(new TermList(owner.getTermConstant("clear") /*0*/, new TermList(owner.getTermConstant("b8") /*16*/, TermList.NIL)), new TermList(new TermList(owner.getTermConstant("on-table") /*1*/, new TermList(owner.getTermConstant("b9") /*17*/, TermList.NIL)), new TermList(new TermList(owner.getTermConstant("clear") /*0*/, new TermList(owner.getTermConstant("b9") /*17*/, TermList.NIL)), TermList.NIL)))))))))))))), TermList.NIL)), false, false));

		JSHOP2 jShop2Planner = new JSHOP2(tl, 100000, new DoubleCost(0.0), owner, s0);
		JSHOP2Provider.setJSHOP2(jShop2Planner);
		while (jShop2Planner.run()) { 
		}
		returnedPlans.addAll(jShop2Planner.getPlans());
		return returnedPlans;
	}

	public static LinkedList<Predicate> getFirstPlanOps() {
		return getPlans().getFirst().getOps();
	}
}