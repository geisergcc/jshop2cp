package oldblocks;
import com.gamalocus.jshop2rt.*;

public class oldblocks extends Domain
{
	private static final long serialVersionUID = 850069636647716492L;


	/**
	 * Operator #-1 for primitive task !pickup
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator15 extends Operator
{
	/**
	 * Operator #-1 for primitive task !pickup
	 */
		public Operator15(Domain owner)
		{
			super(owner, new Predicate(0, 1, new TermList(owner.getTermVariable(0), TermList.NIL)), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[2];
			delIn[0] = new DelAddAtomic(new Predicate(0, 1, new TermList(owner.getTermVariable(0), TermList.NIL)));
			delIn[1] = new DelAddAtomic(new Predicate(1, 1, new TermList(owner.getTermVariable(0), TermList.NIL)));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[1];
			addIn[0] = new DelAddAtomic(new Predicate(2, 1, new TermList(owner.getTermVariable(0), TermList.NIL)));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !pickup [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(1)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Operator #-1 for primitive task !putdown
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator16 extends Operator
{
	/**
	 * Operator #-1 for primitive task !putdown
	 */
		public Operator16(Domain owner)
		{
			super(owner, new Predicate(1, 1, new TermList(owner.getTermVariable(0), TermList.NIL)), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[1];
			delIn[0] = new DelAddAtomic(new Predicate(2, 1, new TermList(owner.getTermVariable(0), TermList.NIL)));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[2];
			addIn[0] = new DelAddAtomic(new Predicate(1, 1, new TermList(owner.getTermVariable(0), TermList.NIL)));
			addIn[1] = new DelAddAtomic(new Predicate(0, 1, new TermList(owner.getTermVariable(0), TermList.NIL)));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !putdown [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(1)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Operator #-1 for primitive task !stack
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator17 extends Operator
{
	/**
	 * Operator #-1 for primitive task !stack
	 */
		public Operator17(Domain owner)
		{
			super(owner, new Predicate(2, 2, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[2];
			delIn[0] = new DelAddAtomic(new Predicate(2, 2, new TermList(owner.getTermVariable(0), TermList.NIL)));
			delIn[1] = new DelAddAtomic(new Predicate(0, 2, new TermList(owner.getTermVariable(1), TermList.NIL)));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[2];
			addIn[0] = new DelAddAtomic(new Predicate(3, 2, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			addIn[1] = new DelAddAtomic(new Predicate(0, 2, new TermList(owner.getTermVariable(0), TermList.NIL)));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !stack [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(2)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Operator #-1 for primitive task !unstack
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator18 extends Operator
{
	/**
	 * Operator #-1 for primitive task !unstack
	 */
		public Operator18(Domain owner)
		{
			super(owner, new Predicate(3, 2, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[2];
			delIn[0] = new DelAddAtomic(new Predicate(0, 2, new TermList(owner.getTermVariable(0), TermList.NIL)));
			delIn[1] = new DelAddAtomic(new Predicate(3, 2, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[2];
			addIn[0] = new DelAddAtomic(new Predicate(2, 2, new TermList(owner.getTermVariable(0), TermList.NIL)));
			addIn[1] = new DelAddAtomic(new Predicate(0, 2, new TermList(owner.getTermVariable(1), TermList.NIL)));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !unstack [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(2)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Operator #-1 for primitive task !!assert
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator19 extends Operator
{
	/**
	 * Operator #-1 for primitive task !!assert
	 */
		public Operator19(Domain owner)
		{
			super(owner, new Predicate(4, 1, new TermList(owner.getTermVariable(0), TermList.NIL)), -1, 0, new TermNumber(0.0));


			DelAddElement[] delIn = new DelAddElement[0];

			setDel(delIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !!assert [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(1)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Method -1 for compound task achieve-goals
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method18 extends Method
	{
	/**
	 * Method -1 for compound task achieve-goals
	 */
		public Method18(Domain owner)
		{
			super(owner, new Predicate(2, 1, new TermList(owner.getTermVariable(0), TermList.NIL)));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(2, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 1, new TermList(owner.getTermVariable(0), new TermList(TermList.NIL, TermList.NIL))), false, false));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(1, 1, TermList.NIL), false, false));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task achieve-goals [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionNil(1)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method0Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Method -1 for compound task assert-goals
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method19 extends Method
	{
	/**
	 * Method -1 for compound task assert-goals
	 */
		public Method19(Domain owner)
		{
			super(owner, new Predicate(0, 3, new TermList(new TermList(owner.getTermVariable(0), owner.getTermVariable(1)), new TermList(owner.getTermVariable(2), TermList.NIL))));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 3, new TermList(owner.getTermVariable(1), new TermList(new TermList(new TermList(owner.getTermConstant(4) /*goal*/, new TermList(owner.getTermVariable(0), TermList.NIL)), owner.getTermVariable(2)), TermList.NIL))), false, false));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task assert-goals [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionNil(3)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method1Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Method -1 for compound task assert-goals
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method20 extends Method
	{
	/**
	 * Method -1 for compound task assert-goals
	 */
		public Method20(Domain owner)
		{
			super(owner, new Predicate(0, 1, new TermList(TermList.NIL, new TermList(owner.getTermVariable(0), TermList.NIL))));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(4, 1, new TermList(owner.getTermVariable(0), TermList.NIL)), false, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task assert-goals [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionNil(1)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method2Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task move-block
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition20 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition20(Domain owner, Term[] unifier)
		{
			p = new Precondition[8];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(0, 3, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[2] = new PreconditionNegation(new PreconditionAtomic(new Predicate(5, 3, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier), 3);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(3, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[4] = new PreconditionAtomic(new Predicate(4, 3, new TermList(new TermList(owner.getTermConstant(3) /*on*/, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[5] = new PreconditionNegation(new PreconditionAtomic(new Predicate(6, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier), 3);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[6] = new PreconditionAtomic(new Predicate(0, 3, new TermList(owner.getTermVariable(2), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[7] = new PreconditionNegation(new PreconditionAtomic(new Predicate(7, 3, new TermList(owner.getTermVariable(2), TermList.NIL)), unifier), 3);
			b = new Term[8][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
			b[6] = null;
			b[7] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[7] == null)
			{
				boolean b6changed = false;
				while (b[6] == null)
				{
					boolean b5changed = false;
					while (b[5] == null)
					{
						boolean b4changed = false;
						while (b[4] == null)
						{
							boolean b3changed = false;
							while (b[3] == null)
							{
								boolean b2changed = false;
								while (b[2] == null)
								{
									boolean b1changed = false;
									while (b[1] == null)
									{
										b[1] = p[1].nextBinding(state);
										if (b[1] == null)
											return null;
										else
											bestMatch = Math.max(bestMatch, 1);
										b1changed = true;
									}
									if ( b1changed ) {
										p[2].reset(state);
										p[2].bind(Term.merge(b, 2));
									}
									b[2] = p[2].nextBinding(state);
									if (b[2] == null)
										b[1] = null;
									else
										bestMatch = Math.max(bestMatch, 2);
									b2changed = true;
								}
								if ( b2changed ) {
									p[3].reset(state);
									p[3].bind(Term.merge(b, 3));
								}
								b[3] = p[3].nextBinding(state);
								if (b[3] == null)
									b[2] = null;
								else
									bestMatch = Math.max(bestMatch, 3);
								b3changed = true;
							}
							if ( b3changed ) {
								p[4].reset(state);
								p[4].bind(Term.merge(b, 4));
							}
							b[4] = p[4].nextBinding(state);
							if (b[4] == null)
								b[3] = null;
							else
								bestMatch = Math.max(bestMatch, 4);
							b4changed = true;
						}
						if ( b4changed ) {
							p[5].reset(state);
							p[5].bind(Term.merge(b, 5));
						}
						b[5] = p[5].nextBinding(state);
						if (b[5] == null)
							b[4] = null;
						else
							bestMatch = Math.max(bestMatch, 5);
						b5changed = true;
					}
					if ( b5changed ) {
						p[6].reset(state);
						p[6].bind(Term.merge(b, 6));
					}
					b[6] = p[6].nextBinding(state);
					if (b[6] == null)
						b[5] = null;
					else
						bestMatch = Math.max(bestMatch, 6);
					b6changed = true;
				}
				if ( b6changed ) {
					p[7].reset(state);
					p[7].bind(Term.merge(b, 7));
				}
				b[7] = p[7].nextBinding(state);
				if (b[7] == null)
					b[6] = null;
				else
					bestMatch = Math.max(bestMatch, 7);
			}

			Term[] retVal = Term.merge(b, 8);
			b[7] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			p[4].reset(state);
			p[5].reset(state);
			p[6].reset(state);
			p[7].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
			b[6] = null;
			b[7] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task move-block [unknown source pos]";
		}
	}

	/**
	 * Precondition #1 of Method -1 for compound task move-block
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition21 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition21(Domain owner, Term[] unifier)
		{
			p = new Precondition[5];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(0, 3, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[2] = new PreconditionNegation(new PreconditionAtomic(new Predicate(5, 3, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier), 3);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(3, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[4] = new PreconditionAtomic(new Predicate(4, 3, new TermList(new TermList(owner.getTermConstant(1) /*on-table*/, new TermList(owner.getTermVariable(0), TermList.NIL)), TermList.NIL)), unifier);
			b = new Term[5][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[4] == null)
			{
				boolean b3changed = false;
				while (b[3] == null)
				{
					boolean b2changed = false;
					while (b[2] == null)
					{
						boolean b1changed = false;
						while (b[1] == null)
						{
							b[1] = p[1].nextBinding(state);
							if (b[1] == null)
								return null;
							else
								bestMatch = Math.max(bestMatch, 1);
							b1changed = true;
						}
						if ( b1changed ) {
							p[2].reset(state);
							p[2].bind(Term.merge(b, 2));
						}
						b[2] = p[2].nextBinding(state);
						if (b[2] == null)
							b[1] = null;
						else
							bestMatch = Math.max(bestMatch, 2);
						b2changed = true;
					}
					if ( b2changed ) {
						p[3].reset(state);
						p[3].bind(Term.merge(b, 3));
					}
					b[3] = p[3].nextBinding(state);
					if (b[3] == null)
						b[2] = null;
					else
						bestMatch = Math.max(bestMatch, 3);
					b3changed = true;
				}
				if ( b3changed ) {
					p[4].reset(state);
					p[4].bind(Term.merge(b, 4));
				}
				b[4] = p[4].nextBinding(state);
				if (b[4] == null)
					b[3] = null;
				else
					bestMatch = Math.max(bestMatch, 4);
			}

			Term[] retVal = Term.merge(b, 5);
			b[4] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			p[4].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #1 of Method -1 for compound task move-block [unknown source pos]";
		}
	}

	/**
	 * Precondition #2 of Method -1 for compound task move-block
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition22 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition22(Domain owner, Term[] unifier)
		{
			p = new Precondition[7];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(0, 3, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[2] = new PreconditionNegation(new PreconditionAtomic(new Predicate(5, 3, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier), 3);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(1, 3, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[4] = new PreconditionAtomic(new Predicate(4, 3, new TermList(new TermList(owner.getTermConstant(3) /*on*/, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[5] = new PreconditionAtomic(new Predicate(0, 3, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[6] = new PreconditionNegation(new PreconditionAtomic(new Predicate(7, 3, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier), 3);
			b = new Term[7][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
			b[6] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[6] == null)
			{
				boolean b5changed = false;
				while (b[5] == null)
				{
					boolean b4changed = false;
					while (b[4] == null)
					{
						boolean b3changed = false;
						while (b[3] == null)
						{
							boolean b2changed = false;
							while (b[2] == null)
							{
								boolean b1changed = false;
								while (b[1] == null)
								{
									b[1] = p[1].nextBinding(state);
									if (b[1] == null)
										return null;
									else
										bestMatch = Math.max(bestMatch, 1);
									b1changed = true;
								}
								if ( b1changed ) {
									p[2].reset(state);
									p[2].bind(Term.merge(b, 2));
								}
								b[2] = p[2].nextBinding(state);
								if (b[2] == null)
									b[1] = null;
								else
									bestMatch = Math.max(bestMatch, 2);
								b2changed = true;
							}
							if ( b2changed ) {
								p[3].reset(state);
								p[3].bind(Term.merge(b, 3));
							}
							b[3] = p[3].nextBinding(state);
							if (b[3] == null)
								b[2] = null;
							else
								bestMatch = Math.max(bestMatch, 3);
							b3changed = true;
						}
						if ( b3changed ) {
							p[4].reset(state);
							p[4].bind(Term.merge(b, 4));
						}
						b[4] = p[4].nextBinding(state);
						if (b[4] == null)
							b[3] = null;
						else
							bestMatch = Math.max(bestMatch, 4);
						b4changed = true;
					}
					if ( b4changed ) {
						p[5].reset(state);
						p[5].bind(Term.merge(b, 5));
					}
					b[5] = p[5].nextBinding(state);
					if (b[5] == null)
						b[4] = null;
					else
						bestMatch = Math.max(bestMatch, 5);
					b5changed = true;
				}
				if ( b5changed ) {
					p[6].reset(state);
					p[6].bind(Term.merge(b, 6));
				}
				b[6] = p[6].nextBinding(state);
				if (b[6] == null)
					b[5] = null;
				else
					bestMatch = Math.max(bestMatch, 6);
			}

			Term[] retVal = Term.merge(b, 7);
			b[6] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			p[4].reset(state);
			p[5].reset(state);
			p[6].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
			b[6] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #2 of Method -1 for compound task move-block [unknown source pos]";
		}
	}

	/**
	 * Precondition #3 of Method -1 for compound task move-block
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition23 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition23(Domain owner, Term[] unifier)
		{
			p = new Precondition[5];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(0, 3, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[2] = new PreconditionNegation(new PreconditionAtomic(new Predicate(5, 3, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier), 3);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(3, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[4] = new PreconditionAtomic(new Predicate(7, 3, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			b = new Term[5][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[4] == null)
			{
				boolean b3changed = false;
				while (b[3] == null)
				{
					boolean b2changed = false;
					while (b[2] == null)
					{
						boolean b1changed = false;
						while (b[1] == null)
						{
							b[1] = p[1].nextBinding(state);
							if (b[1] == null)
								return null;
							else
								bestMatch = Math.max(bestMatch, 1);
							b1changed = true;
						}
						if ( b1changed ) {
							p[2].reset(state);
							p[2].bind(Term.merge(b, 2));
						}
						b[2] = p[2].nextBinding(state);
						if (b[2] == null)
							b[1] = null;
						else
							bestMatch = Math.max(bestMatch, 2);
						b2changed = true;
					}
					if ( b2changed ) {
						p[3].reset(state);
						p[3].bind(Term.merge(b, 3));
					}
					b[3] = p[3].nextBinding(state);
					if (b[3] == null)
						b[2] = null;
					else
						bestMatch = Math.max(bestMatch, 3);
					b3changed = true;
				}
				if ( b3changed ) {
					p[4].reset(state);
					p[4].bind(Term.merge(b, 4));
				}
				b[4] = p[4].nextBinding(state);
				if (b[4] == null)
					b[3] = null;
				else
					bestMatch = Math.max(bestMatch, 4);
			}

			Term[] retVal = Term.merge(b, 5);
			b[4] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			p[4].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #3 of Method -1 for compound task move-block [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task move-block
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method21 extends Method
	{
	/**
	 * Method -1 for compound task move-block
	 */
		public Method21(Domain owner)
		{
			super(owner, new Predicate(1, 3, TermList.NIL));
			TaskList[] subsIn = new TaskList[5];

			subsIn[0] = createTaskList0();
			subsIn[1] = createTaskList1();
			subsIn[2] = createTaskList2();
			subsIn[3] = createTaskList3();
			subsIn[4] = TaskList.empty;

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(4, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(3, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(2, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), false, true));
			retVal.subtasks[2] = new TaskList(new TaskAtom(new Predicate(4, 3, new TermList(new TermList(new TermList(owner.getTermConstant(5) /*nomove*/, new TermList(owner.getTermVariable(0), TermList.NIL)), TermList.NIL), TermList.NIL)), false, true));
			retVal.subtasks[3] = new TaskList(new TaskAtom(new Predicate(1, 3, TermList.NIL), false, false));

			return retVal;
		}

		TaskList createTaskList1()
		{
			TaskList retVal;

			retVal = new TaskList(4, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(3, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(1, 3, new TermList(owner.getTermVariable(0), TermList.NIL)), false, true));
			retVal.subtasks[2] = new TaskList(new TaskAtom(new Predicate(4, 3, new TermList(new TermList(new TermList(owner.getTermConstant(5) /*nomove*/, new TermList(owner.getTermVariable(0), TermList.NIL)), TermList.NIL), TermList.NIL)), false, true));
			retVal.subtasks[3] = new TaskList(new TaskAtom(new Predicate(1, 3, TermList.NIL), false, false));

			return retVal;
		}

		TaskList createTaskList2()
		{
			TaskList retVal;

			retVal = new TaskList(4, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 3, new TermList(owner.getTermVariable(0), TermList.NIL)), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(2, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), false, true));
			retVal.subtasks[2] = new TaskList(new TaskAtom(new Predicate(4, 3, new TermList(new TermList(new TermList(owner.getTermConstant(5) /*nomove*/, new TermList(owner.getTermVariable(0), TermList.NIL)), TermList.NIL), TermList.NIL)), false, true));
			retVal.subtasks[3] = new TaskList(new TaskAtom(new Predicate(1, 3, TermList.NIL), false, false));

			return retVal;
		}

		TaskList createTaskList3()
		{
			TaskList retVal;

			retVal = new TaskList(3, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(3, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(1, 3, new TermList(owner.getTermVariable(0), TermList.NIL)), false, true));
			retVal.subtasks[2] = new TaskList(new TaskAtom(new Predicate(1, 3, TermList.NIL), false, false));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task move-block [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition20(owner, unifier)).setComparator(null);
				break;
				case 1:
					p = (new Precondition21(owner, unifier)).setComparator(null);
				break;
				case 2:
					p = (new Precondition22(owner, unifier)).setComparator(null);
				break;
				case 3:
					p = (new Precondition23(owner, unifier)).setComparator(null);
				break;
				case 4:
					p = (new PreconditionNil(3)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
				case 1:
				break;
				case 2:
				break;
				case 3:
				break;
				case 4:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method3Branch0";
				case 1: return "Method3Branch1";
				case 2: return "Method3Branch2";
				case 3: return "Method3Branch3";
				case 4: return "Method3Branch4";
				default: return null;
			}
		}
	}

	/**
	 * Branch -1 for axiom same
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalAxiom)
	 */
	public static class Axiom12 extends Axiom
{
	/**
	 * Branch -1 for axiom same
	 */
		public Axiom12(Domain owner)
		{
			super(owner, new Predicate(6, 1, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(0), TermList.NIL))), 1);
		}

		@Override
		public String toString()
		{
			return "Branch -1 for axiom same [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionNil(1)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Axiom0Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Precondition of branch #0 of Branch -1 for axiom need-to-move
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition24 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition24(Domain owner, Term[] unifier)
		{
			p = new Precondition[4];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(3, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(4, 4, new TermList(new TermList(owner.getTermConstant(3) /*on*/, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[3] = new PreconditionNegation(new PreconditionAtomic(new Predicate(6, 4, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier), 4);
			b = new Term[4][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[3] == null)
			{
				boolean b2changed = false;
				while (b[2] == null)
				{
					boolean b1changed = false;
					while (b[1] == null)
					{
						b[1] = p[1].nextBinding(state);
						if (b[1] == null)
							return null;
						else
							bestMatch = Math.max(bestMatch, 1);
						b1changed = true;
					}
					if ( b1changed ) {
						p[2].reset(state);
						p[2].bind(Term.merge(b, 2));
					}
					b[2] = p[2].nextBinding(state);
					if (b[2] == null)
						b[1] = null;
					else
						bestMatch = Math.max(bestMatch, 2);
					b2changed = true;
				}
				if ( b2changed ) {
					p[3].reset(state);
					p[3].bind(Term.merge(b, 3));
				}
				b[3] = p[3].nextBinding(state);
				if (b[3] == null)
					b[2] = null;
				else
					bestMatch = Math.max(bestMatch, 3);
			}

			Term[] retVal = Term.merge(b, 4);
			b[3] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of branch #0 of Branch -1 for axiom need-to-move [unknown source pos]";
		}
	}

	/**
	 * Precondition of branch #1 of Branch -1 for axiom need-to-move
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition25 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition25(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(1, 4, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(4, 4, new TermList(new TermList(owner.getTermConstant(3) /*on*/, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), TermList.NIL)), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of branch #1 of Branch -1 for axiom need-to-move [unknown source pos]";
		}
	}

	/**
	 * Precondition of branch #2 of Branch -1 for axiom need-to-move
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition26 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition26(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(3, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(4, 4, new TermList(new TermList(owner.getTermConstant(1) /*on-table*/, new TermList(owner.getTermVariable(0), TermList.NIL)), TermList.NIL)), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of branch #2 of Branch -1 for axiom need-to-move [unknown source pos]";
		}
	}

	/**
	 * Precondition of branch #3 of Branch -1 for axiom need-to-move
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition27 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition27(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(3, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(4, 4, new TermList(new TermList(owner.getTermConstant(0) /*clear*/, new TermList(owner.getTermVariable(1), TermList.NIL)), TermList.NIL)), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of branch #3 of Branch -1 for axiom need-to-move [unknown source pos]";
		}
	}

	/**
	 * Precondition of branch #4 of Branch -1 for axiom need-to-move
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition28 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition28(Domain owner, Term[] unifier)
		{
			p = new Precondition[4];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(3, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(4, 4, new TermList(new TermList(owner.getTermConstant(3) /*on*/, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL))), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[3] = new PreconditionNegation(new PreconditionAtomic(new Predicate(6, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier), 4);
			b = new Term[4][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[3] == null)
			{
				boolean b2changed = false;
				while (b[2] == null)
				{
					boolean b1changed = false;
					while (b[1] == null)
					{
						b[1] = p[1].nextBinding(state);
						if (b[1] == null)
							return null;
						else
							bestMatch = Math.max(bestMatch, 1);
						b1changed = true;
					}
					if ( b1changed ) {
						p[2].reset(state);
						p[2].bind(Term.merge(b, 2));
					}
					b[2] = p[2].nextBinding(state);
					if (b[2] == null)
						b[1] = null;
					else
						bestMatch = Math.max(bestMatch, 2);
					b2changed = true;
				}
				if ( b2changed ) {
					p[3].reset(state);
					p[3].bind(Term.merge(b, 3));
				}
				b[3] = p[3].nextBinding(state);
				if (b[3] == null)
					b[2] = null;
				else
					bestMatch = Math.max(bestMatch, 3);
			}

			Term[] retVal = Term.merge(b, 4);
			b[3] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of branch #4 of Branch -1 for axiom need-to-move [unknown source pos]";
		}
	}

	/**
	 * Precondition of branch #5 of Branch -1 for axiom need-to-move
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition29 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition29(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(3, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(7, 4, new TermList(owner.getTermVariable(3), TermList.NIL)), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of branch #5 of Branch -1 for axiom need-to-move [unknown source pos]";
		}
	}

	/**
	 * Branch -1 for axiom need-to-move
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalAxiom)
	 */
	public static class Axiom13 extends Axiom
{
	/**
	 * Branch -1 for axiom need-to-move
	 */
		public Axiom13(Domain owner)
		{
			super(owner, new Predicate(7, 4, new TermList(owner.getTermVariable(0), TermList.NIL)), 6);
		}

		@Override
		public String toString()
		{
			return "Branch -1 for axiom need-to-move [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition24(owner, unifier)).setComparator(null);
				break;
				case 1:
					p = (new Precondition25(owner, unifier)).setComparator(null);
				break;
				case 2:
					p = (new Precondition26(owner, unifier)).setComparator(null);
				break;
				case 3:
					p = (new Precondition27(owner, unifier)).setComparator(null);
				break;
				case 4:
					p = (new Precondition28(owner, unifier)).setComparator(null);
				break;
				case 5:
					p = (new Precondition29(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Axiom1Branch0";
				case 1: return "Axiom1Branch1";
				case 2: return "Axiom1Branch2";
				case 3: return "Axiom1Branch3";
				case 4: return "Axiom1Branch4";
				case 5: return "Axiom1Branch5";
				default: return null;
			}
		}
	}

	public static final String sourcePath = "/Users/gcc/workspace/jshop2cp/examples/oldblocks/oldblocks";
	public static final long sourceLastModified = 1328619440000L;

	public oldblocks()
	{
		constants = new String[8];
		constants[0] = "clear";
		constants[1] = "on-table";
		constants[2] = "holding";
		constants[3] = "on";
		constants[4] = "goal";
		constants[5] = "nomove";
		constants[6] = "same";
		constants[7] = "need-to-move";

		compoundTasks = new String[3];
		compoundTasks[0] = "assert-goals";
		compoundTasks[1] = "move-block";
		compoundTasks[2] = "achieve-goals";

		primitiveTasks = new String[5];
		primitiveTasks[0] = "!pickup";
		primitiveTasks[1] = "!putdown";
		primitiveTasks[2] = "!stack";
		primitiveTasks[3] = "!unstack";
		primitiveTasks[4] = "!!assert";

		initializeTermVariables(4);

		initializeTermConstants();

		methods = new Method[3][];

		methods[0] = new Method[2];
		methods[0][0] = new Method19(this);
		methods[0][1] = new Method20(this);

		methods[1] = new Method[1];
		methods[1][0] = new Method21(this);

		methods[2] = new Method[1];
		methods[2][0] = new Method18(this);


		ops = new Operator[5][];

		ops[0] = new Operator[1];
		ops[0][0] = new Operator15(this);

		ops[1] = new Operator[1];
		ops[1][0] = new Operator16(this);

		ops[2] = new Operator[1];
		ops[2][0] = new Operator17(this);

		ops[3] = new Operator[1];
		ops[3][0] = new Operator18(this);

		ops[4] = new Operator[1];
		ops[4][0] = new Operator19(this);

		axioms = new Axiom[8][];

		axioms[0] = new Axiom[0];

		axioms[1] = new Axiom[0];

		axioms[2] = new Axiom[0];

		axioms[3] = new Axiom[0];

		axioms[4] = new Axiom[0];

		axioms[5] = new Axiom[0];

		axioms[6] = new Axiom[1];
		axioms[6][0] = new Axiom12(this);

		axioms[7] = new Axiom[1];
		axioms[7][0] = new Axiom13(this);

	}
}