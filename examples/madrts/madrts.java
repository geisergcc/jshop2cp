package madrts;
import com.gamalocus.jshop2rt.*;

public class madrts extends Domain
{
	private static final long serialVersionUID = 1403317833075711010L;


	/**
	 * Operator #-1 for primitive task !ACT_MOVE
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator14 extends Operator
{
	/**
	 * Operator #-1 for primitive task !ACT_MOVE
	 */
		public Operator14(Domain owner)
		{
			super(owner, new Predicate(0, 5, new TermList(new TermList(owner.getTermVariable(0), TermList.NIL), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL)))), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[1];
			delIn[0] = new DelAddAtomic(new Predicate(0, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(4), new TermList(new TermNumber(0.0), TermList.NIL))))));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[1];
			addIn[0] = new DelAddAtomic(new Predicate(0, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(new TermNumber(0.0), TermList.NIL))))));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !ACT_MOVE [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionAtomic(new Predicate(0, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(4), new TermList(new TermNumber(0.0), TermList.NIL))))), unifier)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Method -1 for compound task move
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method15 extends Method
	{
	/**
	 * Method -1 for compound task move
	 */
		public Method15(Domain owner)
		{
			super(owner, new Predicate(0, 5, new TermList(new TermList(owner.getTermVariable(0), TermList.NIL), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL)))));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 5, new TermList(new TermList(owner.getTermVariable(0), TermList.NIL), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL)))), false, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task move [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionAtomic(new Predicate(0, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(4), new TermList(new TermNumber(0.0), TermList.NIL))))), unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method0Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task move
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition19 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition19(Domain owner, Term[] unifier)
		{
			p = new Precondition[4];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(1, 6, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(2, 6, new TermList(owner.getTermVariable(0), new TermList(new TermNumber(1.0), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(0, 6, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(4), new TermList(owner.getTermVariable(5), new TermList(new TermNumber(0.0), TermList.NIL))))), unifier);
			b = new Term[4][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[3] == null)
			{
				boolean b2changed = false;
				while (b[2] == null)
				{
					boolean b1changed = false;
					while (b[1] == null)
					{
						b[1] = p[1].nextBinding(state);
						if (b[1] == null)
							return null;
						else
							bestMatch = Math.max(bestMatch, 1);
						b1changed = true;
					}
					if ( b1changed ) {
						p[2].reset(state);
						p[2].bind(Term.merge(b, 2));
					}
					b[2] = p[2].nextBinding(state);
					if (b[2] == null)
						b[1] = null;
					else
						bestMatch = Math.max(bestMatch, 2);
					b2changed = true;
				}
				if ( b2changed ) {
					p[3].reset(state);
					p[3].bind(Term.merge(b, 3));
				}
				b[3] = p[3].nextBinding(state);
				if (b[3] == null)
					b[2] = null;
				else
					bestMatch = Math.max(bestMatch, 3);
			}

			Term[] retVal = Term.merge(b, 4);
			b[3] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task move [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task move
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method16 extends Method
	{
	/**
	 * Method -1 for compound task move
	 */
		public Method16(Domain owner)
		{
			super(owner, new Predicate(0, 6, new TermList(new TermList(owner.getTermVariable(0), owner.getTermVariable(1)), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL)))));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(2, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 6, new TermList(new TermList(owner.getTermVariable(0), TermList.NIL), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL)))), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(0, 6, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL)))), false, false));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task move [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition19(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method1Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Method -1 for compound task transport
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method17 extends Method
	{
	/**
	 * Method -1 for compound task transport
	 */
		public Method17(Domain owner)
		{
			super(owner, new Predicate(1, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL)))));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL)))), false, false));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task transport [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionNil(3)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method2Branch0";
				default: return null;
			}
		}
	}

	public static final String sourcePath = "/Users/gcc/workspace/jshop2cp/examples/madrts/madrts";
	public static final long sourceLastModified = 1328619440000L;

	public madrts()
	{
		constants = new String[3];
		constants[0] = "m_Loc";
		constants[1] = "m_Unit";
		constants[2] = "m_PlayerID";

		compoundTasks = new String[2];
		compoundTasks[0] = "move";
		compoundTasks[1] = "transport";

		primitiveTasks = new String[1];
		primitiveTasks[0] = "!ACT_MOVE";

		initializeTermVariables(6);

		initializeTermConstants();

		methods = new Method[2][];

		methods[0] = new Method[2];
		methods[0][0] = new Method15(this);
		methods[0][1] = new Method16(this);

		methods[1] = new Method[1];
		methods[1][0] = new Method17(this);


		ops = new Operator[1][];

		ops[0] = new Operator[1];
		ops[0][0] = new Operator14(this);

		axioms = new Axiom[3][];

		axioms[0] = new Axiom[0];

		axioms[1] = new Axiom[0];

		axioms[2] = new Axiom[0];

	}
}