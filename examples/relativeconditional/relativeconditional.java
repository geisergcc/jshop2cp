package relativeconditional;
import com.gamalocus.jshop2rt.*;

public class relativeconditional extends Domain
{
	private static final long serialVersionUID = 6136674939299508798L;


	/**
	 * Operator #-1 for primitive task !!addInWorldState
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator0 extends Operator
{
	/**
	 * Operator #-1 for primitive task !!addInWorldState
	 */
		public Operator0(Domain owner)
		{
			super(owner, new Predicate(0, 1, new TermList(owner.getTermVariable(0), TermList.NIL)), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[0];

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[1];
			addIn[0] = new DelAddAtomic(new Predicate(0, 1));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !!addInWorldState [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(1)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Operator #-1 for primitive task !!removeFromWorldState
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator1 extends Operator
{
	/**
	 * Operator #-1 for primitive task !!removeFromWorldState
	 */
		public Operator1(Domain owner)
		{
			super(owner, new Predicate(1, 1, new TermList(owner.getTermVariable(0), TermList.NIL)), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[1];
			delIn[0] = new DelAddAtomic(new Predicate(0, 1));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[0];

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !!removeFromWorldState [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(1)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Method -1 for compound task transport
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method0 extends Method
	{
	/**
	 * Method -1 for compound task transport
	 */
		public Method0(Domain owner)
		{
			super(owner, new Predicate(0, 1, new TermList(TermList.NIL, new TermList(owner.getTermVariable(0), TermList.NIL))));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = TaskList.empty;

			setSubs(subsIn);
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task transport [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionNil(1)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method0Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task transport
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition0 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition0(Domain owner, Term[] unifier)
		{
			p = new Precondition[4];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(3, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(5, 5, new TermList(owner.getTermConstant(4) /*transport*/, new TermList(owner.getTermVariable(4), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[3] = new PreconditionNegation(new PreconditionAtomic(new Predicate(6, 5, new TermList(owner.getTermVariable(4), TermList.NIL)), unifier), 5);
			b = new Term[4][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[3] == null)
			{
				boolean b2changed = false;
				while (b[2] == null)
				{
					boolean b1changed = false;
					while (b[1] == null)
					{
						b[1] = p[1].nextBinding(state);
						if (b[1] == null)
							return null;
						else
							bestMatch = Math.max(bestMatch, 1);
						b1changed = true;
					}
					if ( b1changed ) {
						p[2].reset(state);
						p[2].bind(Term.merge(b, 2));
					}
					b[2] = p[2].nextBinding(state);
					if (b[2] == null)
						b[1] = null;
					else
						bestMatch = Math.max(bestMatch, 2);
					b2changed = true;
				}
				if ( b2changed ) {
					p[3].reset(state);
					p[3].bind(Term.merge(b, 3));
				}
				b[3] = p[3].nextBinding(state);
				if (b[3] == null)
					b[2] = null;
				else
					bestMatch = Math.max(bestMatch, 3);
			}

			Term[] retVal = Term.merge(b, 4);
			b[3] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task transport [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task transport
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method1 extends Method
	{
	/**
	 * Method -1 for compound task transport
	 */
		public Method1(Domain owner)
		{
			super(owner, new Predicate(0, 5, new TermList(new TermList(owner.getTermVariable(0), owner.getTermVariable(1)), new TermList(owner.getTermVariable(2), TermList.NIL))));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(2, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(4), new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(2), TermList.NIL))))), false, false));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(0, 5, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL))), false, false));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task transport [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition0(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method1Branch0";
				default: return null;
			}
		}
	}

	/**
	 * premise of Relative precondition #0 in branch #0 of Method -1 for compound task transport
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition1 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition1(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(5, 5, new TermList(owner.getTermConstant(7) /*bus*/, new TermList(owner.getTermVariable(4), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(1, 5, new TermList(owner.getTermVariable(4), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "premise of Relative precondition #0 in branch #0 of Method -1 for compound task transport [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task transport
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method2 extends Method
	{
	/**
	 * Method -1 for compound task transport
	 */
		public Method2(Domain owner)
		{
			super(owner, new Predicate(0, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))))));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(4, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 5, new TermList(new TermList(owner.getTermConstant(6) /*reserved*/, new TermList(owner.getTermVariable(1), TermList.NIL)), TermList.NIL)), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(1, 5, new TermList(new TermList(owner.getTermConstant(3) /*at*/, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), TermList.NIL)), false, true));
			retVal.subtasks[2] = new TaskList(new TaskAtom(new Predicate(0, 5, new TermList(new TermList(owner.getTermConstant(3) /*at*/, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), TermList.NIL))), TermList.NIL)), false, true));
			retVal.subtasks[3] = new TaskList(new TaskAtom(new Predicate(1, 5, new TermList(new TermList(owner.getTermConstant(6) /*reserved*/, new TermList(owner.getTermVariable(1), TermList.NIL)), TermList.NIL)), false, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task transport [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionAtomic(new Predicate(5, 5, new TermList(owner.getTermConstant(7) /*bus*/, new TermList(owner.getTermVariable(1), TermList.NIL))), unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
					p = (new PreconditionForAll(new Precondition1(owner, unifier), new PreconditionAtomic(new Predicate(6, 5, new TermList(owner.getTermVariable(4), TermList.NIL)), unifier), 5)).setComparator(null);
					p.reset(state);
					if (p.nextBinding(state) != null) {
						toReturn = toReturn + -1;
					}
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "transport-with-bus";
				default: return null;
			}
		}
	}

	/**
	 * Method -1 for compound task transport
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method3 extends Method
	{
	/**
	 * Method -1 for compound task transport
	 */
		public Method3(Domain owner)
		{
			super(owner, new Predicate(0, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))))));
			TaskList[] subsIn = new TaskList[2];

			subsIn[0] = createTaskList0();
			subsIn[1] = createTaskList1();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(5, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 5, new TermList(new TermList(owner.getTermConstant(10) /*way1*/, new TermList(owner.getTermVariable(1), TermList.NIL)), TermList.NIL)), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(0, 5, new TermList(new TermList(owner.getTermConstant(6) /*reserved*/, new TermList(owner.getTermVariable(1), TermList.NIL)), TermList.NIL)), false, true));
			retVal.subtasks[2] = new TaskList(new TaskAtom(new Predicate(1, 5, new TermList(new TermList(owner.getTermConstant(3) /*at*/, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), TermList.NIL)), false, true));
			retVal.subtasks[3] = new TaskList(new TaskAtom(new Predicate(0, 5, new TermList(new TermList(owner.getTermConstant(3) /*at*/, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), TermList.NIL))), TermList.NIL)), false, true));
			retVal.subtasks[4] = new TaskList(new TaskAtom(new Predicate(1, 5, new TermList(new TermList(owner.getTermConstant(6) /*reserved*/, new TermList(owner.getTermVariable(1), TermList.NIL)), TermList.NIL)), false, true));

			return retVal;
		}

		TaskList createTaskList1()
		{
			TaskList retVal;

			retVal = new TaskList(5, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 5, new TermList(new TermList(owner.getTermConstant(12) /*way2*/, new TermList(owner.getTermVariable(1), TermList.NIL)), TermList.NIL)), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(0, 5, new TermList(new TermList(owner.getTermConstant(6) /*reserved*/, new TermList(owner.getTermVariable(1), TermList.NIL)), TermList.NIL)), false, true));
			retVal.subtasks[2] = new TaskList(new TaskAtom(new Predicate(1, 5, new TermList(new TermList(owner.getTermConstant(3) /*at*/, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), TermList.NIL)), false, true));
			retVal.subtasks[3] = new TaskList(new TaskAtom(new Predicate(0, 5, new TermList(new TermList(owner.getTermConstant(3) /*at*/, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), TermList.NIL))), TermList.NIL)), false, true));
			retVal.subtasks[4] = new TaskList(new TaskAtom(new Predicate(1, 5, new TermList(new TermList(owner.getTermConstant(6) /*reserved*/, new TermList(owner.getTermVariable(1), TermList.NIL)), TermList.NIL)), false, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task transport [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionAtomic(new Predicate(5, 5, new TermList(owner.getTermConstant(8) /*truck*/, new TermList(owner.getTermVariable(1), TermList.NIL))), unifier)).setComparator(null);
				break;
				case 1:
					p = (new PreconditionAtomic(new Predicate(5, 5, new TermList(owner.getTermConstant(8) /*truck*/, new TermList(owner.getTermVariable(1), TermList.NIL))), unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
					p = (new PreconditionForAll(new PreconditionAtomic(new Predicate(5, 5, new TermList(owner.getTermConstant(7) /*bus*/, new TermList(owner.getTermVariable(4), TermList.NIL))), unifier), new PreconditionAtomic(new Predicate(6, 5, new TermList(owner.getTermVariable(4), TermList.NIL)), unifier), 5)).setComparator(null);
					p.reset(state);
					if (p.nextBinding(state) != null) {
						toReturn = toReturn + 2;
					}
					p = (new PreconditionAtomic(new Predicate(5, 5, new TermList(owner.getTermConstant(9) /*subtruck1*/, new TermList(owner.getTermVariable(1), TermList.NIL))), unifier)).setComparator(null);
					p.reset(state);
					if (p.nextBinding(state) != null) {
						toReturn = toReturn + -1;
					}
				break;
				case 1:
					p = (new PreconditionAtomic(new Predicate(5, 5, new TermList(owner.getTermConstant(11) /*subtruck2*/, new TermList(owner.getTermVariable(1), TermList.NIL))), unifier)).setComparator(null);
					p.reset(state);
					if (p.nextBinding(state) != null) {
						toReturn = toReturn + 1;
					}
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "transport-with-truck-way1";
				case 1: return "transport-with-truck-way2";
				default: return null;
			}
		}
	}

	/**
	 * Branch -1 for axiom same
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalAxiom)
	 */
	public static class Axiom0 extends Axiom
{
	/**
	 * Branch -1 for axiom same
	 */
		public Axiom0(Domain owner)
		{
			super(owner, new Predicate(0, 1, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(0), TermList.NIL))), 1);
		}

		@Override
		public String toString()
		{
			return "Branch -1 for axiom same [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionNil(1)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Axiom0Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Branch -1 for axiom different
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalAxiom)
	 */
	public static class Axiom1 extends Axiom
{
	/**
	 * Branch -1 for axiom different
	 */
		public Axiom1(Domain owner)
		{
			super(owner, new Predicate(1, 2, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), 1);
		}

		@Override
		public String toString()
		{
			return "Branch -1 for axiom different [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionNegation(new PreconditionAtomic(new Predicate(0, 2, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier), 2)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Axiom1Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Branch -1 for axiom exist
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalAxiom)
	 */
	public static class Axiom2 extends Axiom
{
	/**
	 * Branch -1 for axiom exist
	 */
		public Axiom2(Domain owner)
		{
			super(owner, new Predicate(2, 2, new TermList(owner.getTermVariable(0), new TermList(new TermList(owner.getTermVariable(0), owner.getTermVariable(1)), TermList.NIL))), 1);
		}

		@Override
		public String toString()
		{
			return "Branch -1 for axiom exist [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionNil(2)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Axiom2Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Branch -1 for axiom exist
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalAxiom)
	 */
	public static class Axiom3 extends Axiom
{
	/**
	 * Branch -1 for axiom exist
	 */
		public Axiom3(Domain owner)
		{
			super(owner, new Predicate(2, 3, new TermList(owner.getTermVariable(0), new TermList(new TermList(owner.getTermVariable(1), owner.getTermVariable(2)), TermList.NIL))), 1);
		}

		@Override
		public String toString()
		{
			return "Branch -1 for axiom exist [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionAtomic(new Predicate(2, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Axiom3Branch0";
				default: return null;
			}
		}
	}

	public static final String sourcePath = "/Users/gcc/workspace/jshop2cp/examples/relativeconditional/relativeconditional";
	public static final long sourceLastModified = 1330036257000L;

	public relativeconditional()
	{
		constants = new String[13];
		constants[0] = "same";
		constants[1] = "different";
		constants[2] = "exist";
		constants[3] = "at";
		constants[4] = "transport";
		constants[5] = "class";
		constants[6] = "reserved";
		constants[7] = "bus";
		constants[8] = "truck";
		constants[9] = "subtruck1";
		constants[10] = "way1";
		constants[11] = "subtruck2";
		constants[12] = "way2";

		compoundTasks = new String[1];
		compoundTasks[0] = "transport";

		primitiveTasks = new String[2];
		primitiveTasks[0] = "!!addInWorldState";
		primitiveTasks[1] = "!!removeFromWorldState";

		initializeTermVariables(5);

		initializeTermConstants();

		methods = new Method[1][];

		methods[0] = new Method[4];
		methods[0][0] = new Method0(this);
		methods[0][1] = new Method1(this);
		methods[0][2] = new Method2(this);
		methods[0][3] = new Method3(this);


		ops = new Operator[2][];

		ops[0] = new Operator[1];
		ops[0][0] = new Operator0(this);

		ops[1] = new Operator[1];
		ops[1][0] = new Operator1(this);

		axioms = new Axiom[13][];

		axioms[0] = new Axiom[1];
		axioms[0][0] = new Axiom0(this);

		axioms[1] = new Axiom[1];
		axioms[1][0] = new Axiom1(this);

		axioms[2] = new Axiom[2];
		axioms[2][0] = new Axiom2(this);
		axioms[2][1] = new Axiom3(this);

		axioms[3] = new Axiom[0];

		axioms[4] = new Axiom[0];

		axioms[5] = new Axiom[0];

		axioms[6] = new Axiom[0];

		axioms[7] = new Axiom[0];

		axioms[8] = new Axiom[0];

		axioms[9] = new Axiom[0];

		axioms[10] = new Axiom[0];

		axioms[11] = new Axiom[0];

		axioms[12] = new Axiom[0];

	}
}