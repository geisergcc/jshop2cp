package relativeconditional;
import java.util.LinkedList;
import com.gamalocus.jshop2rt.*;

public class problem
{
	public static final String sourcePath = "/Users/gcc/workspace/jshop2cp/examples/relativeconditional/problem";
	public static final long sourceLastModified = 1330039493000L;

	public static final Domain owner = new relativeconditional();

	private static String[] defineConstants(Domain owner)
	{
		String[] problemConstants = new String[14];

		owner.addConstant("t1");
		owner.addConstant("t2");
		owner.addConstant("t3");
		owner.addConstant("t4");
		owner.addConstant("t5");
		owner.addConstant("t6");
		owner.addConstant("package");
		owner.addConstant("p1");
		owner.addConstant("l1");
		owner.addConstant("p2");
		owner.addConstant("l2");
		owner.addConstant("p3");
		owner.addConstant("l3");
		owner.addConstant("l4");

		return problemConstants;
	}

	private static void createState0(Domain owner, State s)	{
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("transport") /*4*/, new TermList(owner.getTermConstant("t1") /*13*/, TermList.NIL))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("truck") /*8*/, new TermList(owner.getTermConstant("t1") /*13*/, TermList.NIL))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("subtruck1") /*9*/, new TermList(owner.getTermConstant("t1") /*13*/, TermList.NIL))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("transport") /*4*/, new TermList(owner.getTermConstant("t2") /*14*/, TermList.NIL))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("truck") /*8*/, new TermList(owner.getTermConstant("t2") /*14*/, TermList.NIL))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("subtruck1") /*9*/, new TermList(owner.getTermConstant("t2") /*14*/, TermList.NIL))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("transport") /*4*/, new TermList(owner.getTermConstant("t3") /*15*/, TermList.NIL))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("truck") /*8*/, new TermList(owner.getTermConstant("t3") /*15*/, TermList.NIL))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("subtruck2") /*11*/, new TermList(owner.getTermConstant("t3") /*15*/, TermList.NIL))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("transport") /*4*/, new TermList(owner.getTermConstant("t4") /*16*/, TermList.NIL))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("bus") /*7*/, new TermList(owner.getTermConstant("t4") /*16*/, TermList.NIL))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("transport") /*4*/, new TermList(owner.getTermConstant("t5") /*17*/, TermList.NIL))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("bus") /*7*/, new TermList(owner.getTermConstant("t5") /*17*/, TermList.NIL))));
		s.add(new Predicate(6, 0, new TermList(owner.getTermConstant("t5") /*17*/, TermList.NIL)));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("transport") /*4*/, new TermList(owner.getTermConstant("t6") /*18*/, TermList.NIL))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("bus") /*7*/, new TermList(owner.getTermConstant("t6") /*18*/, TermList.NIL))));
		s.add(new Predicate(6, 0, new TermList(owner.getTermConstant("t6") /*18*/, TermList.NIL)));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("package") /*19*/, new TermList(owner.getTermConstant("p1") /*20*/, TermList.NIL))));
		s.add(new Predicate(3, 0, new TermList(owner.getTermConstant("p1") /*20*/, new TermList(owner.getTermConstant("l1") /*21*/, TermList.NIL))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("package") /*19*/, new TermList(owner.getTermConstant("p2") /*22*/, TermList.NIL))));
		s.add(new Predicate(3, 0, new TermList(owner.getTermConstant("p2") /*22*/, new TermList(owner.getTermConstant("l2") /*23*/, TermList.NIL))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("package") /*19*/, new TermList(owner.getTermConstant("p3") /*24*/, TermList.NIL))));
		s.add(new Predicate(3, 0, new TermList(owner.getTermConstant("p3") /*24*/, new TermList(owner.getTermConstant("l3") /*25*/, TermList.NIL))));
	}

	public static LinkedList<Plan> getPlans()
	{
		LinkedList<Plan> returnedPlans = new LinkedList<Plan>();
		defineConstants(owner);

		State s0 = new State(owner.getAxioms());

		createState0(owner, s0); // jShop2Planner.initialize(d, s0);

		TaskList tl;
			tl = new TaskList(1, true);
			tl.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 0, new TermList(new TermList(owner.getTermConstant("p1") /*20*/, new TermList(owner.getTermConstant("p2") /*22*/, new TermList(owner.getTermConstant("p3") /*24*/, TermList.NIL))), new TermList(owner.getTermConstant("l4") /*26*/, TermList.NIL))), false, false));

		JSHOP2 jShop2Planner = new JSHOP2(tl, 100000, new DoubleCost(0.0), owner, s0);
		JSHOP2Provider.setJSHOP2(jShop2Planner);
		while (jShop2Planner.run()) { 
		}
		returnedPlans.addAll(jShop2Planner.getPlans());
		return returnedPlans;
	}

	public static LinkedList<Predicate> getFirstPlanOps() {
		return getPlans().getFirst().getOps();
	}
}