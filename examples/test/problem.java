package test;
import java.util.LinkedList;
import com.gamalocus.jshop2rt.*;

public class problem
{
	public static final String sourcePath = "/Users/gcc/workspace/jshop2cp/examples/test/problem";
	public static final long sourceLastModified = 1328619440000L;

	public static final Domain owner = new test();

	private static String[] defineConstants(Domain owner)
	{
		String[] problemConstants = new String[3];

		owner.addConstant("obj5");
		owner.addConstant("obj7");
		owner.addConstant("obj8");

		return problemConstants;
	}

	private static void createState0(Domain owner, State s)	{
		s.add(new Predicate(0, 0, new TermList(owner.getTermConstant("obj1") /*14*/, TermList.NIL)));
		s.add(new Predicate(7, 0, new TermList(owner.getTermConstant("obj1") /*14*/, new TermList(owner.getTermConstant("obj2") /*8*/, new TermList(owner.getTermConstant("obj3") /*10*/, TermList.NIL)))));
		s.add(new Predicate(9, 0, new TermList(owner.getTermConstant("obj1") /*14*/, new TermList(owner.getTermConstant("obj2") /*8*/, new TermList(owner.getTermConstant("obj4") /*11*/, TermList.NIL)))));
		s.add(new Predicate(2, 0, new TermList(owner.getTermConstant("obj4") /*11*/, new TermList(owner.getTermConstant("obj5") /*20*/, TermList.NIL))));
		s.add(new Predicate(3, 0, new TermList(owner.getTermConstant("obj4") /*11*/, new TermList(new TermNumber(6.0), TermList.NIL))));
		s.add(new Predicate(2, 0, new TermList(owner.getTermConstant("obj4") /*11*/, new TermList(owner.getTermConstant("obj7") /*21*/, TermList.NIL))));
		s.add(new Predicate(3, 0, new TermList(owner.getTermConstant("obj4") /*11*/, new TermList(new TermNumber(5.0), TermList.NIL))));
		s.add(new Predicate(2, 0, new TermList(owner.getTermConstant("obj4") /*11*/, new TermList(owner.getTermConstant("obj8") /*22*/, TermList.NIL))));
		s.add(new Predicate(3, 0, new TermList(owner.getTermConstant("obj4") /*11*/, new TermList(new TermNumber(2.0), TermList.NIL))));
		s.add(new Predicate(17, 0, new TermList(owner.getTermConstant("uuu") /*16*/, TermList.NIL)));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("obj1") /*14*/, TermList.NIL)));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("obj5") /*20*/, TermList.NIL)));
		s.add(new Predicate(1, 0, new TermList(new TermNumber(3.0), TermList.NIL)));
		s.add(new Predicate(1, 0, new TermList(new TermNumber(2.0), TermList.NIL)));
		s.add(new Predicate(1, 0, new TermList(new TermNumber(13.0), TermList.NIL)));
		s.add(new Predicate(1, 0, new TermList(new TermNumber(25.0), TermList.NIL)));
		s.add(new Predicate(1, 0, new TermList(new TermNumber(1.5), TermList.NIL)));
		s.add(new Predicate(1, 0, new TermList(new TermNumber(18.0), TermList.NIL)));
	}

	public static LinkedList<Plan> getPlans()
	{
		LinkedList<Plan> returnedPlans = new LinkedList<Plan>();
		defineConstants(owner);

		State s0 = new State(owner.getAxioms());

		createState0(owner, s0); // jShop2Planner.initialize(d, s0);

		TaskList tl;
			tl = new TaskList(1, true);
			tl.subtasks[0] = new TaskList(new TaskAtom(new Predicate(1, 0, new TermList(new TermList(owner.getTermConstant("obj2") /*8*/, new TermList(owner.getTermConstant("ttt") /*17*/, new TermList(owner.getTermConstant("uuu") /*16*/, TermList.NIL))), TermList.NIL)), false, false));

		JSHOP2 jShop2Planner = new JSHOP2(tl, 100000, new DoubleCost(0.0), owner, s0);
		JSHOP2Provider.setJSHOP2(jShop2Planner);
		while (jShop2Planner.run()) { 
		}
		returnedPlans.addAll(jShop2Planner.getPlans());
		return returnedPlans;
	}

	public static LinkedList<Predicate> getFirstPlanOps() {
		return getPlans().getFirst().getOps();
	}
}