package test;
import com.gamalocus.jshop2rt.*;

public class test extends Domain
{
	private static final long serialVersionUID = -7286051003069499121L;


	/**
	 * Precondition of Operator #-1 for primitive task !op0
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition0 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition0(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[1] = new PreconditionNegation(new PreconditionAtomic(new Predicate(0, 5, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier), 5);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(1, 5, new TermList(owner.getTermVariable(2), TermList.NIL)), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of Operator #-1 for primitive task !op0 [unknown source pos]";
		}
	}

	/**
	 * Bindings for precondition of add part of DelAddElement #1 of Operator #-1 for primitive task !op0
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition1 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition1(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(2, 5, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(0), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(3, 5, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(4), TermList.NIL))), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Bindings for precondition of add part of DelAddElement #1 of Operator #-1 for primitive task !op0 [unknown source pos]";
		}
	}

	/**
	 * Operator #-1 for primitive task !op0
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator0 extends Operator
{
	/**
	 * Operator #-1 for primitive task !op0
	 */
		public Operator0(Domain owner)
		{
			super(owner, new Predicate(0, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL)))), -1, -1, new TermNumber(1.0));

			Term[] unifier;


			DelAddElement[] delIn = new DelAddElement[1];
			delIn[0] = new DelAddAtomic(new Predicate(1, 5));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[1];
		// Add list of DelAddElement #1 of Operator #-1 for primitive task !op0
		unifier = new Term[5];

		unifier[0] = null;
		unifier[1] = null;
		unifier[2] = null;
		unifier[3] = null;
		unifier[4] = null;

		Predicate[] atoms0 = {
			new Predicate(4, 5, new TermList(owner.getTermVariable(4), TermList.NIL)) };
			addIn[0] = new DelAddForAll(new Precondition1(owner, unifier), atoms0);

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !op0 [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new Precondition0(owner, unifier)).setComparator(((test)owner).compmore2);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Conjunct #0 of Precondition of Operator #-1 for primitive task !op1
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionDisjunction)
	 */
	public static class Precondition2 extends Precondition
	{
		Precondition[] p;
		Term[] b;
		int whichClause;

		public Precondition2(Domain owner, Term[] unifier)
		{
			p = new Precondition[2];
			p[0] = new PreconditionAtomic(new Predicate(2, 4, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))), unifier);

			p[1] = new PreconditionAtomic(new Predicate(3, 4, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))), unifier);

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			p[0].bind(binding);
			p[1].bind(binding);
		}

		protected Term[] nextBindingHelper(State state)
		{
			while (whichClause < 2)
			{
				b = p[whichClause].nextBinding(state);
				if (b != null)
					 return b;
				whichClause++;
			}

			return null;
		}

		@Override
		public String toString()
		{
			return "Conjunct #0 of Precondition of Operator #-1 for primitive task !op1 [unknown source pos]";
		}
		protected void resetHelper(State state)
		{
			p[0].reset(state);
			p[1].reset(state);
			whichClause = 0;
		}
	}

	/**
	 * Precondition of Operator #-1 for primitive task !op1
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition3 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition3(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionDisjunction)
			p[1] = new Precondition2(owner, unifier) /*Conjunct 1 of Precondition of Operator #-1 for primitive task !op1*/;
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(4, 4, new TermList(owner.getTermVariable(3), TermList.NIL)), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of Operator #-1 for primitive task !op1 [unknown source pos]";
		}
	}

	/**
	 * Operator #-1 for primitive task !op1
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator1 extends Operator
{
	/**
	 * Operator #-1 for primitive task !op1
	 */
		public Operator1(Domain owner)
		{
			super(owner, new Predicate(1, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))))), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[0];

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[0];

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !op1 [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new Precondition3(owner, unifier)).setComparator(((test)owner).mycomparator3);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Operator #-1 for primitive task !op2
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator2 extends Operator
{
	/**
	 * Operator #-1 for primitive task !op2
	 */
		public Operator2(Domain owner)
		{
			super(owner, new Predicate(2, 2, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), -1, -1, new TermCall(new List(new TermNumber(2.0), new TermList(owner.getTermVariable(1), TermList.NIL)), StdLib.power, "StdLib.power"));


			DelAddElement[] delIn = new DelAddElement[0];

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[0];

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !op2 [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionCall(new TermCall(new List(owner.getTermVariable(1), new TermList(new TermCall(new List(new TermNumber(1.01), new TermList(new TermNumber(2.0), TermList.NIL)), StdLib.plus, "StdLib.plus"), new TermList(new TermNumber(3.5), new TermList(new TermNumber(4.0), TermList.NIL)))), StdLib.less, "StdLib.less"), unifier)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Method -1 for compound task m1
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method0 extends Method
	{
	/**
	 * Method -1 for compound task m1
	 */
		public Method0(Domain owner)
		{
			super(owner, new Predicate(1, 2, new TermList(owner.getTermVariable(0), TermList.NIL)));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 2, new TermList(owner.getTermVariable(1), TermList.NIL)), false, false));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task m1 [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionAtomic(new Predicate(5, 2, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier)).setComparator(null);
					p.setFirst(true);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method0Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task m1
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition4 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition4(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(6, 7, new TermList(owner.getTermVariable(2), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(7, 7, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), TermList.NIL)))), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task m1 [unknown source pos]";
		}
	}

	/**
	 * Conjunct #0 of Conjunct #1 of Precondition #1 of Method -1 for compound task m1
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionDisjunction)
	 */
	public static class Precondition5 extends Precondition
	{
		Precondition[] p;
		Term[] b;
		int whichClause;

		public Precondition5(Domain owner, Term[] unifier)
		{
			p = new Precondition[2];
			p[0] = new PreconditionAtomic(new Predicate(7, 7, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), TermList.NIL)))), unifier);

			p[1] = new PreconditionAtomic(new Predicate(9, 7, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), TermList.NIL)))), unifier);

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			p[0].bind(binding);
			p[1].bind(binding);
		}

		protected Term[] nextBindingHelper(State state)
		{
			while (whichClause < 2)
			{
				b = p[whichClause].nextBinding(state);
				if (b != null)
					 return b;
				whichClause++;
			}

			return null;
		}

		@Override
		public String toString()
		{
			return "Conjunct #0 of Conjunct #1 of Precondition #1 of Method -1 for compound task m1 [unknown source pos]";
		}
		protected void resetHelper(State state)
		{
			p[0].reset(state);
			p[1].reset(state);
			whichClause = 0;
		}
	}

	/**
	 * Conjunct #1 of Precondition #1 of Method -1 for compound task m1
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition6 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition6(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionDisjunction)
			p[1] = new Precondition5(owner, unifier) /*Conjunct 1 of Conjunct #1 of Precondition #1 of Method -1 for compound task m1*/;
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionCall)
			p[2] = new PreconditionCall(new TermCall(new List(new TermNumber(2.0), new TermList(new TermNumber(2.5), new TermList(new TermNumber(2.5), new TermList(new TermNumber(3.1), TermList.NIL)))), StdLib.lessEq, "StdLib.lessEq"), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Conjunct #1 of Precondition #1 of Method -1 for compound task m1 [unknown source pos]";
		}
	}

	/**
	 * Conjunct #2 of Precondition #1 of Method -1 for compound task m1
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition7 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition7(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionCall)
			p[1] = new PreconditionCall(new TermCall(new List(owner.getTermVariable(0), new TermList(new TermList(owner.getTermConstant(8) /*obj2*/, new TermList(owner.getTermConstant(10) /*obj3*/, new TermList(owner.getTermConstant(11) /*obj4*/, TermList.NIL))), TermList.NIL)), StdLib.member, "StdLib.member"), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionCall)
			p[2] = new PreconditionCall(new TermCall(new List(new TermNumber(2.0), new TermList(new TermNumber(1.0), TermList.NIL)), StdLib.less, "StdLib.less"), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Conjunct #2 of Precondition #1 of Method -1 for compound task m1 [unknown source pos]";
		}
	}

	/**
	 * Conjunct #5 of Precondition #1 of Method -1 for compound task m1
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionDisjunction)
	 */
	public static class Precondition8 extends Precondition
	{
		Precondition[] p;
		Term[] b;
		int whichClause;

		public Precondition8(Domain owner, Term[] unifier)
		{
			p = new Precondition[2];
			p[0] = new PreconditionNegation(new PreconditionAtomic(new Predicate(0, 7, new TermList(owner.getTermConstant(14) /*obj1*/, TermList.NIL)), unifier), 7);

			p[1] = new PreconditionAtomic(new Predicate(5, 7, new TermList(owner.getTermVariable(5), TermList.NIL)), unifier);

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			p[0].bind(binding);
			p[1].bind(binding);
		}

		protected Term[] nextBindingHelper(State state)
		{
			while (whichClause < 2)
			{
				b = p[whichClause].nextBinding(state);
				if (b != null)
					 return b;
				whichClause++;
			}

			return null;
		}

		@Override
		public String toString()
		{
			return "Conjunct #5 of Precondition #1 of Method -1 for compound task m1 [unknown source pos]";
		}
		protected void resetHelper(State state)
		{
			p[0].reset(state);
			p[1].reset(state);
			whichClause = 0;
		}
	}

	/**
	 * Precondition #1 of Method -1 for compound task m1
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition9 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition9(Domain owner, Term[] unifier)
		{
			p = new Precondition[7];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(0, 7, new TermList(owner.getTermVariable(2), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
			p[2] = new Precondition6(owner, unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[3] = new PreconditionNegation(new Precondition7(owner, unifier), 7);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionCall)
			p[4] = new PreconditionCall(new TermCall(new List(new TermList(owner.getTermConstant(0) /*a*/, new TermList(owner.getTermConstant(7) /*b*/, new TermList(owner.getTermConstant(9) /*c*/, TermList.NIL))), new TermList(new TermList(new TermList(owner.getTermConstant(0) /*a*/, new TermList(owner.getTermConstant(7) /*b*/, new TermList(owner.getTermConstant(9) /*c*/, TermList.NIL))), new TermList(owner.getTermConstant(12) /*d*/, new TermList(owner.getTermConstant(13) /*e*/, TermList.NIL))), TermList.NIL)), StdLib.member, "StdLib.member"), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionCall)
			p[5] = new PreconditionCall(new TermCall(new List(new TermCall(new List(new TermNumber(2.0), new TermList(new TermNumber(2.0), TermList.NIL)), StdLib.mult, "StdLib.mult"), new TermList(new TermList(new TermNumber(2.0), new TermList(new TermNumber(3.0), new TermList(new TermCall(new List(new TermNumber(1.0), new TermList(new TermNumber(3.0), TermList.NIL)), StdLib.plus, "StdLib.plus"), new TermList(new TermNumber(5.0), TermList.NIL)))), TermList.NIL)), StdLib.member, "StdLib.member"), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionDisjunction)
			p[6] = new Precondition8(owner, unifier) /*Conjunct 6 of Precondition #1 of Method -1 for compound task m1*/;
			b = new Term[7][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
			b[6] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[6] == null)
			{
				boolean b5changed = false;
				while (b[5] == null)
				{
					boolean b4changed = false;
					while (b[4] == null)
					{
						boolean b3changed = false;
						while (b[3] == null)
						{
							boolean b2changed = false;
							while (b[2] == null)
							{
								boolean b1changed = false;
								while (b[1] == null)
								{
									b[1] = p[1].nextBinding(state);
									if (b[1] == null)
										return null;
									else
										bestMatch = Math.max(bestMatch, 1);
									b1changed = true;
								}
								if ( b1changed ) {
									p[2].reset(state);
									p[2].bind(Term.merge(b, 2));
								}
								b[2] = p[2].nextBinding(state);
								if (b[2] == null)
									b[1] = null;
								else
									bestMatch = Math.max(bestMatch, 2);
								b2changed = true;
							}
							if ( b2changed ) {
								p[3].reset(state);
								p[3].bind(Term.merge(b, 3));
							}
							b[3] = p[3].nextBinding(state);
							if (b[3] == null)
								b[2] = null;
							else
								bestMatch = Math.max(bestMatch, 3);
							b3changed = true;
						}
						if ( b3changed ) {
							p[4].reset(state);
							p[4].bind(Term.merge(b, 4));
						}
						b[4] = p[4].nextBinding(state);
						if (b[4] == null)
							b[3] = null;
						else
							bestMatch = Math.max(bestMatch, 4);
						b4changed = true;
					}
					if ( b4changed ) {
						p[5].reset(state);
						p[5].bind(Term.merge(b, 5));
					}
					b[5] = p[5].nextBinding(state);
					if (b[5] == null)
						b[4] = null;
					else
						bestMatch = Math.max(bestMatch, 5);
					b5changed = true;
				}
				if ( b5changed ) {
					p[6].reset(state);
					p[6].bind(Term.merge(b, 6));
				}
				b[6] = p[6].nextBinding(state);
				if (b[6] == null)
					b[5] = null;
				else
					bestMatch = Math.max(bestMatch, 6);
			}

			Term[] retVal = Term.merge(b, 7);
			b[6] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			p[4].reset(state);
			p[5].reset(state);
			p[6].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
			b[6] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #1 of Method -1 for compound task m1 [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task m1
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method1 extends Method
	{
	/**
	 * Method -1 for compound task m1
	 */
		public Method1(Domain owner)
		{
			super(owner, new Predicate(1, 7, new TermList(new TermList(owner.getTermVariable(0), owner.getTermVariable(1)), TermList.NIL)));
			TaskList[] subsIn = new TaskList[2];

			subsIn[0] = createTaskList0();
			subsIn[1] = createTaskList1();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 7, new TermList(new TermNumber(1.0), new TermList(new TermList(owner.getTermConstant(8) /*obj2*/, TermList.NIL), new TermList(owner.getTermVariable(4), TermList.NIL)))), false, true));

			return retVal;
		}

		TaskList createTaskList1()
		{
			TaskList retVal;

			retVal = new TaskList(3, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 7, new TermList(owner.getTermVariable(5), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(4), TermList.NIL)))), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(1, 7, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(6), TermList.NIL))))), false, true));
			retVal.subtasks[2] = new TaskList(new TaskAtom(new Predicate(2, 7, TermList.NIL), false, false));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task m1 [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition4(owner, unifier)).setComparator(null);
				break;
				case 1:
					p = (new Precondition9(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
				case 1:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method1Branch0";
				case 1: return "Method1Branch1";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task m2
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition10 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition10(Domain owner, Term[] unifier)
		{
			p = new Precondition[5];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(15, 4, new TermList(owner.getTermConstant(8) /*obj2*/, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(new TermCall(new List(new TermNumber(1.5), new TermList(new TermNumber(2.0), new TermList(new TermNumber(2.0), TermList.NIL))), StdLib.mult, "StdLib.mult"), TermList.NIL))))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[2] = new PreconditionNegation(new PreconditionAtomic(new Predicate(17, 4, new TermList(owner.getTermConstant(16) /*uuu*/, TermList.NIL)), unifier), 4);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAssignment)
			p[3] = new PreconditionAssign(new TermList(new TermNumber(12.0), new TermList(new TermCall(new List(new TermNumber(12.0), new TermList(new TermNumber(6.0), new TermList(new TermNumber(5.0), TermList.NIL))), StdLib.div, "StdLib.div"), new TermList(new TermList(owner.getTermVariable(0), new TermList(owner.getTermConstant(0) /*a*/, new TermList(owner.getTermConstant(7) /*b*/, owner.getTermConstant(9) /*c*/))), TermList.NIL))), unifier, 2);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAssignment)
			p[4] = new PreconditionAssign(new TermList(owner.getTermConstant(18) /*www*/, new TermList(new TermCall(new List(new TermNumber(29.0), new TermList(new TermNumber(-12.5), new TermList(new TermCall(new List(new TermNumber(2.0), new TermList(new TermNumber(2.0), new TermList(new TermNumber(3.0), TermList.NIL))), StdLib.power, "StdLib.power"), new TermList(owner.getTermVariable(1), new TermList(new TermCall(new List(new TermNumber(2.0), new TermList(new TermNumber(12.0), new TermList(new TermNumber(5.0), TermList.NIL))), StdLib.minus, "StdLib.minus"), TermList.NIL))))), StdLib.plus, "StdLib.plus"), TermList.NIL)), unifier, 3);
			b = new Term[5][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[4] == null)
			{
				boolean b3changed = false;
				while (b[3] == null)
				{
					boolean b2changed = false;
					while (b[2] == null)
					{
						boolean b1changed = false;
						while (b[1] == null)
						{
							b[1] = p[1].nextBinding(state);
							if (b[1] == null)
								return null;
							else
								bestMatch = Math.max(bestMatch, 1);
							b1changed = true;
						}
						if ( b1changed ) {
							p[2].reset(state);
							p[2].bind(Term.merge(b, 2));
						}
						b[2] = p[2].nextBinding(state);
						if (b[2] == null)
							b[1] = null;
						else
							bestMatch = Math.max(bestMatch, 2);
						b2changed = true;
					}
					if ( b2changed ) {
						p[3].reset(state);
						p[3].bind(Term.merge(b, 3));
					}
					b[3] = p[3].nextBinding(state);
					if (b[3] == null)
						b[2] = null;
					else
						bestMatch = Math.max(bestMatch, 3);
					b3changed = true;
				}
				if ( b3changed ) {
					p[4].reset(state);
					p[4].bind(Term.merge(b, 4));
				}
				b[4] = p[4].nextBinding(state);
				if (b[4] == null)
					b[3] = null;
				else
					bestMatch = Math.max(bestMatch, 4);
			}

			Term[] retVal = Term.merge(b, 5);
			b[4] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			p[4].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task m2 [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task m2
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method2 extends Method
	{
	/**
	 * Method -1 for compound task m2
	 */
		public Method2(Domain owner)
		{
			super(owner, new Predicate(2, 4, TermList.NIL));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(2, 4, new TermList(new TermList(owner.getTermVariable(2), owner.getTermVariable(3)), new TermList(new TermCall(new List(new TermNumber(1.0), new TermList(new TermCall(new List(new TermNumber(1.0), new TermList(new TermNumber(1.0), TermList.NIL)), ((test)owner).calculateMyFunc, "((test)owner).calculateMyFunc"), TermList.NIL)), StdLib.plus, "StdLib.plus"), TermList.NIL))), false, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task m2 [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition10(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method2Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Method -1 for compound task m3
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method3 extends Method
	{
	/**
	 * Method -1 for compound task m3
	 */
		public Method3(Domain owner)
		{
			super(owner, new Predicate(0, 1, new TermList(owner.getTermVariable(0), TermList.NIL)));
			TaskList[] subsIn = new TaskList[2];

			subsIn[0] = createTaskList0();
			subsIn[1] = TaskList.empty;

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(3, 1, TermList.NIL), false, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task m3 [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionAtomic(new Predicate(0, 1, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier)).setComparator(null);
				break;
				case 1:
					p = (new PreconditionNil(1)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
				case 1:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method3Branch0";
				case 1: return "Method3Branch1";
				default: return null;
			}
		}
	}

	/**
	 * Branch -1 for axiom ax1
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalAxiom)
	 */
	public static class Axiom0 extends Axiom
{
	/**
	 * Branch -1 for axiom ax1
	 */
		public Axiom0(Domain owner)
		{
			super(owner, new Predicate(15, 3, new TermList(owner.getTermVariable(0), new TermList(new TermList(owner.getTermVariable(1), new TermList(owner.getTermConstant(19) /*yyy*/, new TermList(owner.getTermVariable(2), TermList.NIL))), new TermList(new TermCall(new List(new TermNumber(1.0), new TermList(new TermNumber(1.0), TermList.NIL)), StdLib.plus, "StdLib.plus"), new TermList(new TermCall(new List(new TermNumber(6.0), new TermList(new TermNumber(1.0), TermList.NIL)), StdLib.power, "StdLib.power"), TermList.NIL))))), 2);
		}

		@Override
		public String toString()
		{
			return "Branch -1 for axiom ax1 [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionAtomic(new Predicate(9, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL)))), unifier)).setComparator(null);
				break;
				case 1:
					p = (new PreconditionAtomic(new Predicate(15, 3, new TermList(owner.getTermVariable(1), new TermList(new TermList(owner.getTermVariable(0), new TermList(owner.getTermConstant(19) /*yyy*/, new TermList(owner.getTermVariable(2), TermList.NIL))), new TermList(new TermNumber(2.0), new TermList(new TermCall(new List(new TermNumber(3.0), new TermList(new TermCall(new List(new TermNumber(1.5), new TermList(new TermNumber(2.0), TermList.NIL)), StdLib.mult, "StdLib.mult"), TermList.NIL)), StdLib.plus, "StdLib.plus"), TermList.NIL))))), unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Axiom0Branch0";
				case 1: return "Axiom0Branch1";
				default: return null;
			}
		}
	}

	/**
	 * Branch -1 for axiom ax2
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalAxiom)
	 */
	public static class Axiom1 extends Axiom
{
	/**
	 * Branch -1 for axiom ax2
	 */
		public Axiom1(Domain owner)
		{
			super(owner, new Predicate(6, 2, new TermList(owner.getTermVariable(0), TermList.NIL)), 2);
		}

		@Override
		public String toString()
		{
			return "Branch -1 for axiom ax2 [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionAtomic(new Predicate(2, 2, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier)).setComparator(null);
				break;
				case 1:
					p = (new PreconditionAtomic(new Predicate(0, 2, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Axiom1Branch0";
				case 1: return "Axiom1Branch1";
				default: return null;
			}
		}
	}

	public static final String sourcePath = "/Users/gcc/workspace/jshop2cp/examples/test/test";
	public static final long sourceLastModified = 1328619440000L;

	public MyFunc calculateMyFunc = new MyFunc();

	public MyComparator mycomparator3 = new MyComparator(3);

	public CompMore compmore2 = new CompMore(2);

	public test()
	{
		constants = new String[20];
		constants[0] = "a";
		constants[1] = "t";
		constants[2] = "q";
		constants[3] = "r";
		constants[4] = "s";
		constants[5] = "v";
		constants[6] = "ax2";
		constants[7] = "b";
		constants[8] = "obj2";
		constants[9] = "c";
		constants[10] = "obj3";
		constants[11] = "obj4";
		constants[12] = "d";
		constants[13] = "e";
		constants[14] = "obj1";
		constants[15] = "ax1";
		constants[16] = "uuu";
		constants[17] = "ttt";
		constants[18] = "www";
		constants[19] = "yyy";

		compoundTasks = new String[3];
		compoundTasks[0] = "m3";
		compoundTasks[1] = "m1";
		compoundTasks[2] = "m2";

		primitiveTasks = new String[4];
		primitiveTasks[0] = "!op0";
		primitiveTasks[1] = "!op1";
		primitiveTasks[2] = "!op2";
		primitiveTasks[3] = "!op4";

		initializeTermVariables(7);

		initializeTermConstants();

		methods = new Method[3][];

		methods[0] = new Method[1];
		methods[0][0] = new Method3(this);

		methods[1] = new Method[2];
		methods[1][0] = new Method0(this);
		methods[1][1] = new Method1(this);

		methods[2] = new Method[1];
		methods[2][0] = new Method2(this);


		ops = new Operator[4][];

		ops[0] = new Operator[1];
		ops[0][0] = new Operator0(this);

		ops[1] = new Operator[1];
		ops[1][0] = new Operator1(this);

		ops[2] = new Operator[1];
		ops[2][0] = new Operator2(this);

		ops[3] = new Operator[0];

		axioms = new Axiom[20][];

		axioms[0] = new Axiom[0];

		axioms[1] = new Axiom[0];

		axioms[2] = new Axiom[0];

		axioms[3] = new Axiom[0];

		axioms[4] = new Axiom[0];

		axioms[5] = new Axiom[0];

		axioms[6] = new Axiom[1];
		axioms[6][0] = new Axiom1(this);

		axioms[7] = new Axiom[0];

		axioms[8] = new Axiom[0];

		axioms[9] = new Axiom[0];

		axioms[10] = new Axiom[0];

		axioms[11] = new Axiom[0];

		axioms[12] = new Axiom[0];

		axioms[13] = new Axiom[0];

		axioms[14] = new Axiom[0];

		axioms[15] = new Axiom[1];
		axioms[15][0] = new Axiom0(this);

		axioms[16] = new Axiom[0];

		axioms[17] = new Axiom[0];

		axioms[18] = new Axiom[0];

		axioms[19] = new Axiom[0];

	}
}