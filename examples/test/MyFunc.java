package test;

import com.gamalocus.jshop2rt.Calculate;
import com.gamalocus.jshop2rt.List;
import com.gamalocus.jshop2rt.Term;
import com.gamalocus.jshop2rt.TermNumber;


public class MyFunc implements Calculate {
  public Term call(List l)
  {
    double sum = 0;

    while (l != null)
    {
      sum += ((TermNumber)l.getHead()).getNumber();
      l = l.getRest();
    }

    return new TermNumber(sum);
  }
}
