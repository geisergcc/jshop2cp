package logistics;
import com.gamalocus.jshop2rt.*;

public class logistics extends Domain
{
	private static final long serialVersionUID = -3543468021281430809L;


	/**
	 * Operator #-1 for primitive task !load-truck
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator6 extends Operator
{
	/**
	 * Operator #-1 for primitive task !load-truck
	 */
		public Operator6(Domain owner)
		{
			super(owner, new Predicate(0, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL)))), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[2];
			delIn[0] = new DelAddAtomic(new Predicate(0, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))));
			delIn[1] = new DelAddProtection(new Predicate(1, 3, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL))));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[1];
			addIn[0] = new DelAddAtomic(new Predicate(2, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !load-truck [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(3)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Operator #-1 for primitive task !unload-truck
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator7 extends Operator
{
	/**
	 * Operator #-1 for primitive task !unload-truck
	 */
		public Operator7(Domain owner)
		{
			super(owner, new Predicate(1, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL)))), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[2];
			delIn[0] = new DelAddAtomic(new Predicate(2, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			delIn[1] = new DelAddProtection(new Predicate(1, 3, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL))));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[1];
			addIn[0] = new DelAddAtomic(new Predicate(0, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !unload-truck [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(3)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Operator #-1 for primitive task !load-airplane
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator8 extends Operator
{
	/**
	 * Operator #-1 for primitive task !load-airplane
	 */
		public Operator8(Domain owner)
		{
			super(owner, new Predicate(2, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL)))), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[2];
			delIn[0] = new DelAddAtomic(new Predicate(0, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))));
			delIn[1] = new DelAddProtection(new Predicate(3, 3, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL))));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[1];
			addIn[0] = new DelAddAtomic(new Predicate(4, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !load-airplane [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(3)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Operator #-1 for primitive task !unload-airplane
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator9 extends Operator
{
	/**
	 * Operator #-1 for primitive task !unload-airplane
	 */
		public Operator9(Domain owner)
		{
			super(owner, new Predicate(3, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL)))), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[2];
			delIn[0] = new DelAddAtomic(new Predicate(4, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			delIn[1] = new DelAddProtection(new Predicate(3, 3, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL))));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[1];
			addIn[0] = new DelAddAtomic(new Predicate(0, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !unload-airplane [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(3)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Operator #-1 for primitive task !drive-truck
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator10 extends Operator
{
	/**
	 * Operator #-1 for primitive task !drive-truck
	 */
		public Operator10(Domain owner)
		{
			super(owner, new Predicate(4, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL)))), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[1];
			delIn[0] = new DelAddAtomic(new Predicate(1, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[2];
			addIn[0] = new DelAddAtomic(new Predicate(1, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))));
			addIn[1] = new DelAddProtection(new Predicate(1, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !drive-truck [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(3)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Operator #-1 for primitive task !fly-airplane
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator11 extends Operator
{
	/**
	 * Operator #-1 for primitive task !fly-airplane
	 */
		public Operator11(Domain owner)
		{
			super(owner, new Predicate(5, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL)))), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[1];
			delIn[0] = new DelAddAtomic(new Predicate(3, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[2];
			addIn[0] = new DelAddAtomic(new Predicate(3, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))));
			addIn[1] = new DelAddProtection(new Predicate(3, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !fly-airplane [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(3)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Operator #-1 for primitive task !add-protection
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator12 extends Operator
{
	/**
	 * Operator #-1 for primitive task !add-protection
	 */
		public Operator12(Domain owner)
		{
			super(owner, new Predicate(6, 1, new TermList(owner.getTermVariable(0), TermList.NIL)), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[0];

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[1];
			addIn[0] = new DelAddProtection(new Predicate(0, 1));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !add-protection [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(1)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Operator #-1 for primitive task !delete-protection
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator13 extends Operator
{
	/**
	 * Operator #-1 for primitive task !delete-protection
	 */
		public Operator13(Domain owner)
		{
			super(owner, new Predicate(7, 1, new TermList(owner.getTermVariable(0), TermList.NIL)), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[1];
			delIn[0] = new DelAddProtection(new Predicate(0, 1));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[0];

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !delete-protection [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(1)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task obj-at
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition14 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition14(Domain owner, Term[] unifier)
		{
			p = new Precondition[5];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(5, 10, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(0, 10, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(5, 10, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[4] = new PreconditionAtomic(new Predicate(6, 10, new TermList(owner.getTermVariable(4), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier);
			b = new Term[5][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[4] == null)
			{
				boolean b3changed = false;
				while (b[3] == null)
				{
					boolean b2changed = false;
					while (b[2] == null)
					{
						boolean b1changed = false;
						while (b[1] == null)
						{
							b[1] = p[1].nextBinding(state);
							if (b[1] == null)
								return null;
							else
								bestMatch = Math.max(bestMatch, 1);
							b1changed = true;
						}
						if ( b1changed ) {
							p[2].reset(state);
							p[2].bind(Term.merge(b, 2));
						}
						b[2] = p[2].nextBinding(state);
						if (b[2] == null)
							b[1] = null;
						else
							bestMatch = Math.max(bestMatch, 2);
						b2changed = true;
					}
					if ( b2changed ) {
						p[3].reset(state);
						p[3].bind(Term.merge(b, 3));
					}
					b[3] = p[3].nextBinding(state);
					if (b[3] == null)
						b[2] = null;
					else
						bestMatch = Math.max(bestMatch, 3);
					b3changed = true;
				}
				if ( b3changed ) {
					p[4].reset(state);
					p[4].bind(Term.merge(b, 4));
				}
				b[4] = p[4].nextBinding(state);
				if (b[4] == null)
					b[3] = null;
				else
					bestMatch = Math.max(bestMatch, 4);
			}

			Term[] retVal = Term.merge(b, 5);
			b[4] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			p[4].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task obj-at [unknown source pos]";
		}
	}

	/**
	 * Precondition #1 of Method -1 for compound task obj-at
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition15 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition15(Domain owner, Term[] unifier)
		{
			p = new Precondition[11];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(5, 10, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(0, 10, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(5, 10, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(5), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[4] = new PreconditionAtomic(new Predicate(7, 10, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(5), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[5] = new PreconditionAtomic(new Predicate(6, 10, new TermList(owner.getTermVariable(6), new TermList(owner.getTermVariable(5), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[6] = new PreconditionAtomic(new Predicate(6, 10, new TermList(owner.getTermVariable(7), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[7] = new PreconditionAtomic(new Predicate(8, 10, new TermList(owner.getTermVariable(8), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[8] = new PreconditionAtomic(new Predicate(5, 10, new TermList(owner.getTermVariable(8), new TermList(owner.getTermVariable(5), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[9] = new PreconditionAtomic(new Predicate(8, 10, new TermList(owner.getTermVariable(9), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[10] = new PreconditionAtomic(new Predicate(5, 10, new TermList(owner.getTermVariable(9), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier);
			b = new Term[11][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
			b[6] = null;
			b[7] = null;
			b[8] = null;
			b[9] = null;
			b[10] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[10] == null)
			{
				boolean b9changed = false;
				while (b[9] == null)
				{
					boolean b8changed = false;
					while (b[8] == null)
					{
						boolean b7changed = false;
						while (b[7] == null)
						{
							boolean b6changed = false;
							while (b[6] == null)
							{
								boolean b5changed = false;
								while (b[5] == null)
								{
									boolean b4changed = false;
									while (b[4] == null)
									{
										boolean b3changed = false;
										while (b[3] == null)
										{
											boolean b2changed = false;
											while (b[2] == null)
											{
												boolean b1changed = false;
												while (b[1] == null)
												{
													b[1] = p[1].nextBinding(state);
													if (b[1] == null)
														return null;
													else
														bestMatch = Math.max(bestMatch, 1);
													b1changed = true;
												}
												if ( b1changed ) {
													p[2].reset(state);
													p[2].bind(Term.merge(b, 2));
												}
												b[2] = p[2].nextBinding(state);
												if (b[2] == null)
													b[1] = null;
												else
													bestMatch = Math.max(bestMatch, 2);
												b2changed = true;
											}
											if ( b2changed ) {
												p[3].reset(state);
												p[3].bind(Term.merge(b, 3));
											}
											b[3] = p[3].nextBinding(state);
											if (b[3] == null)
												b[2] = null;
											else
												bestMatch = Math.max(bestMatch, 3);
											b3changed = true;
										}
										if ( b3changed ) {
											p[4].reset(state);
											p[4].bind(Term.merge(b, 4));
										}
										b[4] = p[4].nextBinding(state);
										if (b[4] == null)
											b[3] = null;
										else
											bestMatch = Math.max(bestMatch, 4);
										b4changed = true;
									}
									if ( b4changed ) {
										p[5].reset(state);
										p[5].bind(Term.merge(b, 5));
									}
									b[5] = p[5].nextBinding(state);
									if (b[5] == null)
										b[4] = null;
									else
										bestMatch = Math.max(bestMatch, 5);
									b5changed = true;
								}
								if ( b5changed ) {
									p[6].reset(state);
									p[6].bind(Term.merge(b, 6));
								}
								b[6] = p[6].nextBinding(state);
								if (b[6] == null)
									b[5] = null;
								else
									bestMatch = Math.max(bestMatch, 6);
								b6changed = true;
							}
							if ( b6changed ) {
								p[7].reset(state);
								p[7].bind(Term.merge(b, 7));
							}
							b[7] = p[7].nextBinding(state);
							if (b[7] == null)
								b[6] = null;
							else
								bestMatch = Math.max(bestMatch, 7);
							b7changed = true;
						}
						if ( b7changed ) {
							p[8].reset(state);
							p[8].bind(Term.merge(b, 8));
						}
						b[8] = p[8].nextBinding(state);
						if (b[8] == null)
							b[7] = null;
						else
							bestMatch = Math.max(bestMatch, 8);
						b8changed = true;
					}
					if ( b8changed ) {
						p[9].reset(state);
						p[9].bind(Term.merge(b, 9));
					}
					b[9] = p[9].nextBinding(state);
					if (b[9] == null)
						b[8] = null;
					else
						bestMatch = Math.max(bestMatch, 9);
					b9changed = true;
				}
				if ( b9changed ) {
					p[10].reset(state);
					p[10].bind(Term.merge(b, 10));
				}
				b[10] = p[10].nextBinding(state);
				if (b[10] == null)
					b[9] = null;
				else
					bestMatch = Math.max(bestMatch, 10);
			}

			Term[] retVal = Term.merge(b, 11);
			b[10] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			p[4].reset(state);
			p[5].reset(state);
			p[6].reset(state);
			p[7].reset(state);
			p[8].reset(state);
			p[9].reset(state);
			p[10].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
			b[6] = null;
			b[7] = null;
			b[8] = null;
			b[9] = null;
			b[10] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #1 of Method -1 for compound task obj-at [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task obj-at
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method10 extends Method
	{
	/**
	 * Method -1 for compound task obj-at
	 */
		public Method10(Domain owner)
		{
			super(owner, new Predicate(2, 10, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			TaskList[] subsIn = new TaskList[2];

			subsIn[0] = createTaskList0();
			subsIn[1] = createTaskList1();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 10, new TermList(owner.getTermVariable(4), new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(1), TermList.NIL))))), false, false));

			return retVal;
		}

		TaskList createTaskList1()
		{
			TaskList retVal;

			retVal = new TaskList(3, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 10, new TermList(owner.getTermVariable(6), new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(8), TermList.NIL))))), false, false));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(1, 10, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(8), new TermList(owner.getTermVariable(9), TermList.NIL)))), false, false));
			retVal.subtasks[2] = new TaskList(new TaskAtom(new Predicate(0, 10, new TermList(owner.getTermVariable(7), new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(9), new TermList(owner.getTermVariable(1), TermList.NIL))))), false, false));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task obj-at [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition14(owner, unifier)).setComparator(null);
				break;
				case 1:
					p = (new Precondition15(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
				case 1:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method0Branch0";
				case 1: return "Method0Branch1";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #1 of Method -1 for compound task in-city-delivery
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition16 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition16(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(5, 5, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(4), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(6, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(4), TermList.NIL))), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #1 of Method -1 for compound task in-city-delivery [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task in-city-delivery
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method11 extends Method
	{
	/**
	 * Method -1 for compound task in-city-delivery
	 */
		public Method11(Domain owner)
		{
			super(owner, new Predicate(0, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))))));
			TaskList[] subsIn = new TaskList[2];

			subsIn[0] = TaskList.empty;
			subsIn[1] = createTaskList1();

			setSubs(subsIn);
		}

		TaskList createTaskList1()
		{
			TaskList retVal;

			retVal = new TaskList(4, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(3, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), false, false));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(0, 5, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL)))), true, true));
			retVal.subtasks[2] = new TaskList(new TaskAtom(new Predicate(3, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), TermList.NIL))), false, false));
			retVal.subtasks[3] = new TaskList(new TaskAtom(new Predicate(1, 5, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), TermList.NIL)))), true, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task in-city-delivery [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionAtomic(new Predicate(9, 5, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))), unifier)).setComparator(null);
				break;
				case 1:
					p = (new Precondition16(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
				case 1:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method1Branch0";
				case 1: return "Method1Branch1";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task truck-at
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition17 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition17(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(1, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(7, 3, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task truck-at [unknown source pos]";
		}
	}

	/**
	 * Precondition #1 of Method -1 for compound task truck-at
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition18 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition18(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(1, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(9, 3, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #1 of Method -1 for compound task truck-at [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task truck-at
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method12 extends Method
	{
	/**
	 * Method -1 for compound task truck-at
	 */
		public Method12(Domain owner)
		{
			super(owner, new Predicate(3, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			TaskList[] subsIn = new TaskList[2];

			subsIn[0] = createTaskList0();
			subsIn[1] = createTaskList1();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(4, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(1), TermList.NIL)))), true, true));

			return retVal;
		}

		TaskList createTaskList1()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(6, 3, new TermList(new TermList(owner.getTermConstant(1) /*truck-at*/, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), TermList.NIL)), true, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task truck-at [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition17(owner, unifier)).setComparator(null);
				break;
				case 1:
					p = (new Precondition18(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
				case 1:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method2Branch0";
				case 1: return "Method2Branch1";
				default: return null;
			}
		}
	}

	/**
	 * Method -1 for compound task air-deliver-obj
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method13 extends Method
	{
	/**
	 * Method -1 for compound task air-deliver-obj
	 */
		public Method13(Domain owner)
		{
			super(owner, new Predicate(1, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL)))));
			TaskList[] subsIn = new TaskList[2];

			subsIn[0] = createTaskList0();
			subsIn[1] = createTaskList1();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(4, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(6, 5, new TermList(new TermList(owner.getTermConstant(3) /*airplane-at*/, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(1), TermList.NIL))), TermList.NIL)), true, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(2, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(1), TermList.NIL)))), false, true));
			retVal.subtasks[2] = new TaskList(new TaskAtom(new Predicate(4, 5, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(2), TermList.NIL))), false, false));
			retVal.subtasks[3] = new TaskList(new TaskAtom(new Predicate(3, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(2), TermList.NIL)))), false, true));

			return retVal;
		}

		TaskList createTaskList1()
		{
			TaskList retVal;

			retVal = new TaskList(4, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(5, 5, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(4), new TermList(owner.getTermVariable(1), TermList.NIL)))), true, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(2, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(1), TermList.NIL)))), false, true));
			retVal.subtasks[2] = new TaskList(new TaskAtom(new Predicate(4, 5, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(2), TermList.NIL))), false, false));
			retVal.subtasks[3] = new TaskList(new TaskAtom(new Predicate(3, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(2), TermList.NIL)))), false, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task air-deliver-obj [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionAtomic(new Predicate(3, 5, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier)).setComparator(null);
				break;
				case 1:
					p = (new PreconditionAtomic(new Predicate(3, 5, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(4), TermList.NIL))), unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
				case 1:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "airplane-at-the-current-airport";
				case 1: return "Method3Branch1";
				default: return null;
			}
		}
	}

	/**
	 * Method -1 for compound task fly-airplane
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method14 extends Method
	{
	/**
	 * Method -1 for compound task fly-airplane
	 */
		public Method14(Domain owner)
		{
			super(owner, new Predicate(4, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			TaskList[] subsIn = new TaskList[2];

			subsIn[0] = createTaskList0();
			subsIn[1] = createTaskList1();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(6, 3, new TermList(new TermList(owner.getTermConstant(3) /*airplane-at*/, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), TermList.NIL)), true, true));

			return retVal;
		}

		TaskList createTaskList1()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(5, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(1), TermList.NIL)))), true, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task fly-airplane [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionAtomic(new Predicate(3, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier)).setComparator(null);
				break;
				case 1:
					p = (new PreconditionAtomic(new Predicate(3, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
				case 1:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "airplane-already-there";
				case 1: return "fly-airplane-in";
				default: return null;
			}
		}
	}

	/**
	 * Branch -1 for axiom same
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalAxiom)
	 */
	public static class Axiom10 extends Axiom
{
	/**
	 * Branch -1 for axiom same
	 */
		public Axiom10(Domain owner)
		{
			super(owner, new Predicate(9, 1, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(0), TermList.NIL))), 1);
		}

		@Override
		public String toString()
		{
			return "Branch -1 for axiom same [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionNil(1)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Axiom0Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Branch -1 for axiom different
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalAxiom)
	 */
	public static class Axiom11 extends Axiom
{
	/**
	 * Branch -1 for axiom different
	 */
		public Axiom11(Domain owner)
		{
			super(owner, new Predicate(7, 2, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), 1);
		}

		@Override
		public String toString()
		{
			return "Branch -1 for axiom different [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionNegation(new PreconditionAtomic(new Predicate(9, 2, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier), 2)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Axiom1Branch0";
				default: return null;
			}
		}
	}

	public static final String sourcePath = "/Users/gcc/workspace/jshop2cp/examples/logistics/logistics";
	public static final long sourceLastModified = 1328619440000L;

	public logistics()
	{
		constants = new String[10];
		constants[0] = "obj-at";
		constants[1] = "truck-at";
		constants[2] = "in-truck";
		constants[3] = "airplane-at";
		constants[4] = "in-airplane";
		constants[5] = "in-city";
		constants[6] = "truck";
		constants[7] = "different";
		constants[8] = "airport";
		constants[9] = "same";

		compoundTasks = new String[5];
		compoundTasks[0] = "in-city-delivery";
		compoundTasks[1] = "air-deliver-obj";
		compoundTasks[2] = "obj-at";
		compoundTasks[3] = "truck-at";
		compoundTasks[4] = "fly-airplane";

		primitiveTasks = new String[8];
		primitiveTasks[0] = "!load-truck";
		primitiveTasks[1] = "!unload-truck";
		primitiveTasks[2] = "!load-airplane";
		primitiveTasks[3] = "!unload-airplane";
		primitiveTasks[4] = "!drive-truck";
		primitiveTasks[5] = "!fly-airplane";
		primitiveTasks[6] = "!add-protection";
		primitiveTasks[7] = "!delete-protection";

		initializeTermVariables(10);

		initializeTermConstants();

		methods = new Method[5][];

		methods[0] = new Method[1];
		methods[0][0] = new Method11(this);

		methods[1] = new Method[1];
		methods[1][0] = new Method13(this);

		methods[2] = new Method[1];
		methods[2][0] = new Method10(this);

		methods[3] = new Method[1];
		methods[3][0] = new Method12(this);

		methods[4] = new Method[1];
		methods[4][0] = new Method14(this);


		ops = new Operator[8][];

		ops[0] = new Operator[1];
		ops[0][0] = new Operator6(this);

		ops[1] = new Operator[1];
		ops[1][0] = new Operator7(this);

		ops[2] = new Operator[1];
		ops[2][0] = new Operator8(this);

		ops[3] = new Operator[1];
		ops[3][0] = new Operator9(this);

		ops[4] = new Operator[1];
		ops[4][0] = new Operator10(this);

		ops[5] = new Operator[1];
		ops[5][0] = new Operator11(this);

		ops[6] = new Operator[1];
		ops[6][0] = new Operator12(this);

		ops[7] = new Operator[1];
		ops[7][0] = new Operator13(this);

		axioms = new Axiom[10][];

		axioms[0] = new Axiom[0];

		axioms[1] = new Axiom[0];

		axioms[2] = new Axiom[0];

		axioms[3] = new Axiom[0];

		axioms[4] = new Axiom[0];

		axioms[5] = new Axiom[0];

		axioms[6] = new Axiom[0];

		axioms[7] = new Axiom[1];
		axioms[7][0] = new Axiom11(this);

		axioms[8] = new Axiom[0];

		axioms[9] = new Axiom[1];
		axioms[9][0] = new Axiom10(this);

	}
}