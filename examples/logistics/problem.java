package logistics;
import java.util.LinkedList;
import com.gamalocus.jshop2rt.*;

public class problem
{
	public static final String sourcePath = "/Users/gcc/workspace/jshop2cp/examples/logistics/problem";
	public static final long sourceLastModified = 1328619440000L;

	public static final Domain owner = new logistics();

	private static String[] defineConstants(Domain owner)
	{
		String[] problemConstants = new String[78];

		owner.addConstant("plane1");
		owner.addConstant("LOC1-1");
		owner.addConstant("plane2");
		owner.addConstant("LOC4-1");
		owner.addConstant("plane3");
		owner.addConstant("LOC5-1");
		owner.addConstant("AIRPORT");
		owner.addConstant("truck1-1");
		owner.addConstant("city1");
		owner.addConstant("TRUCK");
		owner.addConstant("IN-CITY");
		owner.addConstant("LOC1-2");
		owner.addConstant("LOC1-3");
		owner.addConstant("LOC2-1");
		owner.addConstant("truck2-1");
		owner.addConstant("city2");
		owner.addConstant("LOC2-2");
		owner.addConstant("LOC2-3");
		owner.addConstant("LOC3-1");
		owner.addConstant("truck3-1");
		owner.addConstant("city3");
		owner.addConstant("LOC3-2");
		owner.addConstant("LOC3-3");
		owner.addConstant("truck4-1");
		owner.addConstant("city4");
		owner.addConstant("LOC4-2");
		owner.addConstant("LOC4-3");
		owner.addConstant("truck5-1");
		owner.addConstant("city5");
		owner.addConstant("LOC5-2");
		owner.addConstant("LOC5-3");
		owner.addConstant("LOC6-1");
		owner.addConstant("truck6-1");
		owner.addConstant("city6");
		owner.addConstant("LOC6-2");
		owner.addConstant("LOC6-3");
		owner.addConstant("LOC7-1");
		owner.addConstant("truck7-1");
		owner.addConstant("city7");
		owner.addConstant("LOC7-2");
		owner.addConstant("LOC7-3");
		owner.addConstant("LOC8-1");
		owner.addConstant("truck8-1");
		owner.addConstant("city8");
		owner.addConstant("LOC8-2");
		owner.addConstant("LOC8-3");
		owner.addConstant("package1");
		owner.addConstant("loc8-3");
		owner.addConstant("package2");
		owner.addConstant("loc1-1");
		owner.addConstant("package3");
		owner.addConstant("loc2-2");
		owner.addConstant("package4");
		owner.addConstant("loc6-3");
		owner.addConstant("package5");
		owner.addConstant("loc5-1");
		owner.addConstant("package6");
		owner.addConstant("loc2-3");
		owner.addConstant("package7");
		owner.addConstant("loc1-2");
		owner.addConstant("package8");
		owner.addConstant("package9");
		owner.addConstant("loc5-2");
		owner.addConstant("package10");
		owner.addConstant("loc7-1");
		owner.addConstant("package11");
		owner.addConstant("package12");
		owner.addConstant("package13");
		owner.addConstant("loc7-2");
		owner.addConstant("package14");
		owner.addConstant("package15");
		owner.addConstant("loc8-2");
		owner.addConstant("loc8-1");
		owner.addConstant("loc2-1");
		owner.addConstant("loc6-2");
		owner.addConstant("loc4-2");
		owner.addConstant("loc3-2");
		owner.addConstant("loc3-3");

		return problemConstants;
	}

	private static void createState0(Domain owner, State s)	{
		s.add(new Predicate(3, 0, new TermList(owner.getTermConstant("plane1") /*10*/, new TermList(owner.getTermConstant("LOC1-1") /*11*/, TermList.NIL))));
		s.add(new Predicate(3, 0, new TermList(owner.getTermConstant("plane2") /*12*/, new TermList(owner.getTermConstant("LOC4-1") /*13*/, TermList.NIL))));
		s.add(new Predicate(3, 0, new TermList(owner.getTermConstant("plane3") /*14*/, new TermList(owner.getTermConstant("LOC5-1") /*15*/, TermList.NIL))));
		s.add(new Predicate(1, 0, new TermList(owner.getTermConstant("truck1-1") /*17*/, new TermList(owner.getTermConstant("LOC1-1") /*11*/, TermList.NIL))));
		s.add(new Predicate(1, 0, new TermList(owner.getTermConstant("truck2-1") /*24*/, new TermList(owner.getTermConstant("LOC2-1") /*23*/, TermList.NIL))));
		s.add(new Predicate(1, 0, new TermList(owner.getTermConstant("truck3-1") /*29*/, new TermList(owner.getTermConstant("LOC3-1") /*28*/, TermList.NIL))));
		s.add(new Predicate(1, 0, new TermList(owner.getTermConstant("truck4-1") /*33*/, new TermList(owner.getTermConstant("LOC4-1") /*13*/, TermList.NIL))));
		s.add(new Predicate(1, 0, new TermList(owner.getTermConstant("truck5-1") /*37*/, new TermList(owner.getTermConstant("LOC5-1") /*15*/, TermList.NIL))));
		s.add(new Predicate(1, 0, new TermList(owner.getTermConstant("truck6-1") /*42*/, new TermList(owner.getTermConstant("LOC6-1") /*41*/, TermList.NIL))));
		s.add(new Predicate(1, 0, new TermList(owner.getTermConstant("truck7-1") /*47*/, new TermList(owner.getTermConstant("LOC7-1") /*46*/, TermList.NIL))));
		s.add(new Predicate(1, 0, new TermList(owner.getTermConstant("truck8-1") /*52*/, new TermList(owner.getTermConstant("LOC8-1") /*51*/, TermList.NIL))));
		s.add(new Predicate(0, 0, new TermList(owner.getTermConstant("package1") /*56*/, new TermList(owner.getTermConstant("loc8-3") /*57*/, TermList.NIL))));
		s.add(new Predicate(0, 0, new TermList(owner.getTermConstant("package2") /*58*/, new TermList(owner.getTermConstant("loc1-1") /*59*/, TermList.NIL))));
		s.add(new Predicate(0, 0, new TermList(owner.getTermConstant("package3") /*60*/, new TermList(owner.getTermConstant("loc2-2") /*61*/, TermList.NIL))));
		s.add(new Predicate(0, 0, new TermList(owner.getTermConstant("package4") /*62*/, new TermList(owner.getTermConstant("loc6-3") /*63*/, TermList.NIL))));
		s.add(new Predicate(0, 0, new TermList(owner.getTermConstant("package5") /*64*/, new TermList(owner.getTermConstant("loc5-1") /*65*/, TermList.NIL))));
		s.add(new Predicate(0, 0, new TermList(owner.getTermConstant("package6") /*66*/, new TermList(owner.getTermConstant("loc2-3") /*67*/, TermList.NIL))));
		s.add(new Predicate(0, 0, new TermList(owner.getTermConstant("package7") /*68*/, new TermList(owner.getTermConstant("loc1-2") /*69*/, TermList.NIL))));
		s.add(new Predicate(0, 0, new TermList(owner.getTermConstant("package8") /*70*/, new TermList(owner.getTermConstant("loc6-3") /*63*/, TermList.NIL))));
		s.add(new Predicate(0, 0, new TermList(owner.getTermConstant("package9") /*71*/, new TermList(owner.getTermConstant("loc5-2") /*72*/, TermList.NIL))));
		s.add(new Predicate(0, 0, new TermList(owner.getTermConstant("package10") /*73*/, new TermList(owner.getTermConstant("loc7-1") /*74*/, TermList.NIL))));
		s.add(new Predicate(0, 0, new TermList(owner.getTermConstant("package11") /*75*/, new TermList(owner.getTermConstant("loc2-3") /*67*/, TermList.NIL))));
		s.add(new Predicate(0, 0, new TermList(owner.getTermConstant("package12") /*76*/, new TermList(owner.getTermConstant("loc2-2") /*61*/, TermList.NIL))));
		s.add(new Predicate(0, 0, new TermList(owner.getTermConstant("package13") /*77*/, new TermList(owner.getTermConstant("loc7-2") /*78*/, TermList.NIL))));
		s.add(new Predicate(0, 0, new TermList(owner.getTermConstant("package14") /*79*/, new TermList(owner.getTermConstant("loc7-1") /*74*/, TermList.NIL))));
		s.add(new Predicate(0, 0, new TermList(owner.getTermConstant("package15") /*80*/, new TermList(owner.getTermConstant("loc8-2") /*81*/, TermList.NIL))));
	}

	public static LinkedList<Plan> getPlans()
	{
		LinkedList<Plan> returnedPlans = new LinkedList<Plan>();
		defineConstants(owner);

		State s0 = new State(owner.getAxioms());

		createState0(owner, s0); // jShop2Planner.initialize(d, s0);

		TaskList tl;
			tl = new TaskList(15, false);
			tl.subtasks[0] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("package1") /*56*/, new TermList(owner.getTermConstant("loc8-1") /*82*/, TermList.NIL))), false, false));
			tl.subtasks[1] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("package2") /*58*/, new TermList(owner.getTermConstant("loc2-1") /*83*/, TermList.NIL))), false, false));
			tl.subtasks[2] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("package3") /*60*/, new TermList(owner.getTermConstant("loc2-3") /*67*/, TermList.NIL))), false, false));
			tl.subtasks[3] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("package4") /*62*/, new TermList(owner.getTermConstant("loc6-2") /*84*/, TermList.NIL))), false, false));
			tl.subtasks[4] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("package5") /*64*/, new TermList(owner.getTermConstant("loc1-1") /*59*/, TermList.NIL))), false, false));
			tl.subtasks[5] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("package6") /*66*/, new TermList(owner.getTermConstant("loc6-2") /*84*/, TermList.NIL))), false, false));
			tl.subtasks[6] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("package7") /*68*/, new TermList(owner.getTermConstant("loc6-3") /*63*/, TermList.NIL))), false, false));
			tl.subtasks[7] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("package8") /*70*/, new TermList(owner.getTermConstant("loc1-1") /*59*/, TermList.NIL))), false, false));
			tl.subtasks[8] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("package9") /*71*/, new TermList(owner.getTermConstant("loc4-2") /*85*/, TermList.NIL))), false, false));
			tl.subtasks[9] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("package10") /*73*/, new TermList(owner.getTermConstant("loc8-3") /*57*/, TermList.NIL))), false, false));
			tl.subtasks[10] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("package11") /*75*/, new TermList(owner.getTermConstant("loc3-2") /*86*/, TermList.NIL))), false, false));
			tl.subtasks[11] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("package12") /*76*/, new TermList(owner.getTermConstant("loc3-3") /*87*/, TermList.NIL))), false, false));
			tl.subtasks[12] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("package13") /*77*/, new TermList(owner.getTermConstant("loc3-2") /*86*/, TermList.NIL))), false, false));
			tl.subtasks[13] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("package14") /*79*/, new TermList(owner.getTermConstant("loc6-3") /*63*/, TermList.NIL))), false, false));
			tl.subtasks[14] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("package15") /*80*/, new TermList(owner.getTermConstant("loc5-1") /*65*/, TermList.NIL))), false, false));

		JSHOP2 jShop2Planner = new JSHOP2(tl, 100000, new DoubleCost(0.0), owner, s0);
		JSHOP2Provider.setJSHOP2(jShop2Planner);
		while (jShop2Planner.run()) { 
		}
		returnedPlans.addAll(jShop2Planner.getPlans());
		return returnedPlans;
	}

	public static LinkedList<Predicate> getFirstPlanOps() {
		return getPlans().getFirst().getOps();
	}
}