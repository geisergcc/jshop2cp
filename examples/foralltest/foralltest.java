package foralltest;
import com.gamalocus.jshop2rt.*;

public class foralltest extends Domain
{
	private static final long serialVersionUID = -8763035872919593852L;


	/**
	 * Operator #-1 for primitive task !do-something
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator2 extends Operator
{
	/**
	 * Operator #-1 for primitive task !do-something
	 */
		public Operator2(Domain owner)
		{
			super(owner, new Predicate(0, 0, TermList.NIL), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[0];

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[0];

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !do-something [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(0)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * consequence of Precondition #0 of Method -1 for compound task succeed
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition5 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition5(Domain owner, Term[] unifier)
		{
			p = new Precondition[4];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(1, 1, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(1, 1, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[3] = new PreconditionNegation(new PreconditionAtomic(new Predicate(2, 1, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier), 1);
			b = new Term[4][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[3] == null)
			{
				boolean b2changed = false;
				while (b[2] == null)
				{
					boolean b1changed = false;
					while (b[1] == null)
					{
						b[1] = p[1].nextBinding(state);
						if (b[1] == null)
							return null;
						else
							bestMatch = Math.max(bestMatch, 1);
						b1changed = true;
					}
					if ( b1changed ) {
						p[2].reset(state);
						p[2].bind(Term.merge(b, 2));
					}
					b[2] = p[2].nextBinding(state);
					if (b[2] == null)
						b[1] = null;
					else
						bestMatch = Math.max(bestMatch, 2);
					b2changed = true;
				}
				if ( b2changed ) {
					p[3].reset(state);
					p[3].bind(Term.merge(b, 3));
				}
				b[3] = p[3].nextBinding(state);
				if (b[3] == null)
					b[2] = null;
				else
					bestMatch = Math.max(bestMatch, 3);
			}

			Term[] retVal = Term.merge(b, 4);
			b[3] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}
		@Override
		public String toString()
		{
			return "consequence of Precondition #0 of Method -1 for compound task succeed [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task succeed
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method2 extends Method
	{
	/**
	 * Method -1 for compound task succeed
	 */
		public Method2(Domain owner)
		{
			super(owner, new Predicate(0, 1, TermList.NIL));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 1, TermList.NIL), false, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task succeed [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionForAll(new PreconditionAtomic(new Predicate(0, 1, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier), new Precondition5(owner, unifier), 1)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method0Branch0";
				default: return null;
			}
		}
	}

	public static final String sourcePath = "/Users/gcc/workspace/jshop2cp/examples/foralltest/foralltest";
	public static final long sourceLastModified = 1328619440000L;

	public foralltest()
	{
		constants = new String[3];
		constants[0] = "p";
		constants[1] = "q";
		constants[2] = "w";

		compoundTasks = new String[1];
		compoundTasks[0] = "succeed";

		primitiveTasks = new String[1];
		primitiveTasks[0] = "!do-something";

		initializeTermVariables(1);

		initializeTermConstants();

		methods = new Method[1][];

		methods[0] = new Method[1];
		methods[0][0] = new Method2(this);


		ops = new Operator[1][];

		ops[0] = new Operator[1];
		ops[0][0] = new Operator2(this);

		axioms = new Axiom[3][];

		axioms[0] = new Axiom[0];

		axioms[1] = new Axiom[0];

		axioms[2] = new Axiom[0];

	}
}