package freecell;
import java.util.LinkedList;
import com.gamalocus.jshop2rt.*;

public class problem
{
	public static final String sourcePath = "/Users/gcc/workspace/jshop2cp/examples/freecell/problem";
	public static final long sourceLastModified = 1328619440000L;

	public static final Domain owner = new freecell();

	private static String[] defineConstants(Domain owner)
	{
		String[] problemConstants = new String[0];


		return problemConstants;
	}

	private static void createState0(Domain owner, State s)	{
		s.add(new Predicate(6, 0, new TermList(new TermNumber(0.0), TermList.NIL)));
		s.add(new Predicate(8, 0, new TermList(new TermNumber(5.0), TermList.NIL)));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("s") /*0*/, new TermList(new TermNumber(1.0), new TermList(TermList.NIL, new TermList(TermList.NIL, TermList.NIL))))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("h") /*2*/, new TermList(new TermNumber(1.0), new TermList(TermList.NIL, new TermList(TermList.NIL, TermList.NIL))))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("d") /*3*/, new TermList(new TermNumber(1.0), new TermList(TermList.NIL, new TermList(TermList.NIL, TermList.NIL))))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("c") /*4*/, new TermList(new TermNumber(1.0), new TermList(owner.getTermConstant("s") /*0*/, new TermList(new TermNumber(1.0), TermList.NIL))))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("s") /*0*/, new TermList(new TermNumber(2.0), new TermList(owner.getTermConstant("h") /*2*/, new TermList(new TermNumber(1.0), TermList.NIL))))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("h") /*2*/, new TermList(new TermNumber(2.0), new TermList(owner.getTermConstant("d") /*3*/, new TermList(new TermNumber(1.0), TermList.NIL))))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("d") /*3*/, new TermList(new TermNumber(2.0), new TermList(owner.getTermConstant("c") /*4*/, new TermList(new TermNumber(1.0), TermList.NIL))))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("c") /*4*/, new TermList(new TermNumber(2.0), new TermList(owner.getTermConstant("s") /*0*/, new TermList(new TermNumber(2.0), TermList.NIL))))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("s") /*0*/, new TermList(new TermNumber(3.0), new TermList(owner.getTermConstant("h") /*2*/, new TermList(new TermNumber(2.0), TermList.NIL))))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("h") /*2*/, new TermList(new TermNumber(3.0), new TermList(owner.getTermConstant("d") /*3*/, new TermList(new TermNumber(2.0), TermList.NIL))))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("d") /*3*/, new TermList(new TermNumber(3.0), new TermList(owner.getTermConstant("c") /*4*/, new TermList(new TermNumber(2.0), TermList.NIL))))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("c") /*4*/, new TermList(new TermNumber(3.0), new TermList(owner.getTermConstant("s") /*0*/, new TermList(new TermNumber(3.0), TermList.NIL))))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("s") /*0*/, new TermList(new TermNumber(4.0), new TermList(owner.getTermConstant("h") /*2*/, new TermList(new TermNumber(3.0), TermList.NIL))))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("h") /*2*/, new TermList(new TermNumber(4.0), new TermList(owner.getTermConstant("d") /*3*/, new TermList(new TermNumber(3.0), TermList.NIL))))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("d") /*3*/, new TermList(new TermNumber(4.0), new TermList(owner.getTermConstant("c") /*4*/, new TermList(new TermNumber(3.0), TermList.NIL))))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("c") /*4*/, new TermList(new TermNumber(4.0), new TermList(owner.getTermConstant("s") /*0*/, new TermList(new TermNumber(4.0), TermList.NIL))))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("s") /*0*/, new TermList(new TermNumber(5.0), new TermList(owner.getTermConstant("h") /*2*/, new TermList(new TermNumber(4.0), TermList.NIL))))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("h") /*2*/, new TermList(new TermNumber(5.0), new TermList(owner.getTermConstant("d") /*3*/, new TermList(new TermNumber(4.0), TermList.NIL))))));
		s.add(new Predicate(5, 0, new TermList(owner.getTermConstant("d") /*3*/, new TermList(new TermNumber(5.0), new TermList(owner.getTermConstant("c") /*4*/, new TermList(new TermNumber(4.0), TermList.NIL))))));
	}

	public static LinkedList<Plan> getPlans()
	{
		LinkedList<Plan> returnedPlans = new LinkedList<Plan>();
		defineConstants(owner);

		State s0 = new State(owner.getAxioms());

		createState0(owner, s0); // jShop2Planner.initialize(d, s0);

		TaskList tl;
			tl = new TaskList(2, true);
			tl.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 0, TermList.NIL), false, true));
			tl.subtasks[1] = new TaskList(4, false);
			tl.subtasks[1].subtasks[0] = new TaskList(5, true);
			tl.subtasks[1].subtasks[0].subtasks[0] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("s") /*0*/, new TermList(new TermNumber(1.0), TermList.NIL))), false, false));
			tl.subtasks[1].subtasks[0].subtasks[1] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("s") /*0*/, new TermList(new TermNumber(2.0), TermList.NIL))), false, false));
			tl.subtasks[1].subtasks[0].subtasks[2] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("s") /*0*/, new TermList(new TermNumber(3.0), TermList.NIL))), false, false));
			tl.subtasks[1].subtasks[0].subtasks[3] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("s") /*0*/, new TermList(new TermNumber(4.0), TermList.NIL))), false, false));
			tl.subtasks[1].subtasks[0].subtasks[4] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("s") /*0*/, new TermList(new TermNumber(5.0), TermList.NIL))), false, false));
			tl.subtasks[1].subtasks[1] = new TaskList(5, true);
			tl.subtasks[1].subtasks[1].subtasks[0] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("h") /*2*/, new TermList(new TermNumber(1.0), TermList.NIL))), false, false));
			tl.subtasks[1].subtasks[1].subtasks[1] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("h") /*2*/, new TermList(new TermNumber(2.0), TermList.NIL))), false, false));
			tl.subtasks[1].subtasks[1].subtasks[2] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("h") /*2*/, new TermList(new TermNumber(3.0), TermList.NIL))), false, false));
			tl.subtasks[1].subtasks[1].subtasks[3] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("h") /*2*/, new TermList(new TermNumber(4.0), TermList.NIL))), false, false));
			tl.subtasks[1].subtasks[1].subtasks[4] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("h") /*2*/, new TermList(new TermNumber(5.0), TermList.NIL))), false, false));
			tl.subtasks[1].subtasks[2] = new TaskList(5, true);
			tl.subtasks[1].subtasks[2].subtasks[0] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("d") /*3*/, new TermList(new TermNumber(1.0), TermList.NIL))), false, false));
			tl.subtasks[1].subtasks[2].subtasks[1] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("d") /*3*/, new TermList(new TermNumber(2.0), TermList.NIL))), false, false));
			tl.subtasks[1].subtasks[2].subtasks[2] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("d") /*3*/, new TermList(new TermNumber(3.0), TermList.NIL))), false, false));
			tl.subtasks[1].subtasks[2].subtasks[3] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("d") /*3*/, new TermList(new TermNumber(4.0), TermList.NIL))), false, false));
			tl.subtasks[1].subtasks[2].subtasks[4] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("d") /*3*/, new TermList(new TermNumber(5.0), TermList.NIL))), false, false));
			tl.subtasks[1].subtasks[3] = new TaskList(4, true);
			tl.subtasks[1].subtasks[3].subtasks[0] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("c") /*4*/, new TermList(new TermNumber(1.0), TermList.NIL))), false, false));
			tl.subtasks[1].subtasks[3].subtasks[1] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("c") /*4*/, new TermList(new TermNumber(2.0), TermList.NIL))), false, false));
			tl.subtasks[1].subtasks[3].subtasks[2] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("c") /*4*/, new TermList(new TermNumber(3.0), TermList.NIL))), false, false));
			tl.subtasks[1].subtasks[3].subtasks[3] = new TaskList(new TaskAtom(new Predicate(2, 0, new TermList(owner.getTermConstant("c") /*4*/, new TermList(new TermNumber(4.0), TermList.NIL))), false, false));

		JSHOP2 jShop2Planner = new JSHOP2(tl, 100000, new DoubleCost(0.0), owner, s0);
		JSHOP2Provider.setJSHOP2(jShop2Planner);
		while (jShop2Planner.run()) { 
		}
		returnedPlans.addAll(jShop2Planner.getPlans());
		return returnedPlans;
	}

	public static LinkedList<Predicate> getFirstPlanOps() {
		return getPlans().getFirst().getOps();
	}
}