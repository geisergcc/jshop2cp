package freecell;
import com.gamalocus.jshop2rt.*;

public class freecell extends Domain
{
	private static final long serialVersionUID = 729393998719099196L;


	/**
	 * Operator #-1 for primitive task !!done
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator0 extends Operator
{
	/**
	 * Operator #-1 for primitive task !!done
	 */
		public Operator0(Domain owner)
		{
			super(owner, new Predicate(0, 0, TermList.NIL), -1, -1, new TermNumber(0.0));


			DelAddElement[] delIn = new DelAddElement[0];

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[4];
			addIn[0] = new DelAddAtomic(new Predicate(1, 0, new TermList(owner.getTermConstant(0) /*s*/, new TermList(new TermNumber(0.0), TermList.NIL))));
			addIn[1] = new DelAddAtomic(new Predicate(1, 0, new TermList(owner.getTermConstant(2) /*h*/, new TermList(new TermNumber(0.0), TermList.NIL))));
			addIn[2] = new DelAddAtomic(new Predicate(1, 0, new TermList(owner.getTermConstant(3) /*d*/, new TermList(new TermNumber(0.0), TermList.NIL))));
			addIn[3] = new DelAddAtomic(new Predicate(1, 0, new TermList(owner.getTermConstant(4) /*c*/, new TermList(new TermNumber(0.0), TermList.NIL))));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !!done [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(0)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Precondition of Operator #-1 for primitive task !move
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition0 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition0(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(5, 9, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(4), new TermList(owner.getTermVariable(5), TermList.NIL))))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(6, 9, new TermList(owner.getTermVariable(6), TermList.NIL)), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of Operator #-1 for primitive task !move [unknown source pos]";
		}
	}

	/**
	 * Operator #-1 for primitive task !move
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator1 extends Operator
{
	/**
	 * Operator #-1 for primitive task !move
	 */
		public Operator1(Domain owner)
		{
			super(owner, new Predicate(1, 9, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))))), -1, -1, new TermNumber(1.0));

			Term[] unifier;


			DelAddElement[] delIn = new DelAddElement[3];
			delIn[0] = new DelAddAtomic(new Predicate(5, 9, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(4), new TermList(owner.getTermVariable(5), TermList.NIL))))));
			delIn[1] = new DelAddAtomic(new Predicate(6, 9, new TermList(owner.getTermVariable(6), TermList.NIL)));
		// Delete list of DelAddElement #3 of Operator #-1 for primitive task !move
		unifier = new Term[9];

		unifier[0] = null;
		unifier[1] = null;
		unifier[2] = null;
		unifier[3] = null;
		unifier[4] = null;
		unifier[5] = null;
		unifier[6] = null;
		unifier[7] = null;
		unifier[8] = null;

		Predicate[] atoms0 = {
			new Predicate(7, 9, new TermList(owner.getTermVariable(7), new TermList(owner.getTermVariable(8), new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))))) };
			delIn[2] = new DelAddForAll(new PreconditionAtomic(new Predicate(7, 9, new TermList(owner.getTermVariable(7), new TermList(owner.getTermVariable(8), new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))))), unifier), atoms0);

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[3];
			addIn[0] = new DelAddAtomic(new Predicate(5, 9, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))))));
			addIn[1] = new DelAddAtomic(new Predicate(7, 9, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(4), new TermList(owner.getTermVariable(5), TermList.NIL))))));
			addIn[2] = new DelAddAtomic(new Predicate(6, 9, new TermList(new TermCall(new List(owner.getTermVariable(6), new TermList(new TermCall(new List(owner.getTermVariable(4), new TermList(new TermNumber(1.0), TermList.NIL)), ((freecell)owner).calculateCheckNull, "((freecell)owner).calculateCheckNull"), new TermList(new TermCall(new List(owner.getTermVariable(2), new TermList(new TermNumber(-1.0), TermList.NIL)), ((freecell)owner).calculateCheckNull, "((freecell)owner).calculateCheckNull"), TermList.NIL))), StdLib.plus, "StdLib.plus"), TermList.NIL)));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !move [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new Precondition0(owner, unifier)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Precondition of Operator #-1 for primitive task !free
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition1 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition1(Domain owner, Term[] unifier)
		{
			p = new Precondition[4];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(8, 8, new TermList(owner.getTermVariable(2), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(5, 8, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(4), TermList.NIL))))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(6, 8, new TermList(owner.getTermVariable(5), TermList.NIL)), unifier);
			b = new Term[4][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[3] == null)
			{
				boolean b2changed = false;
				while (b[2] == null)
				{
					boolean b1changed = false;
					while (b[1] == null)
					{
						b[1] = p[1].nextBinding(state);
						if (b[1] == null)
							return null;
						else
							bestMatch = Math.max(bestMatch, 1);
						b1changed = true;
					}
					if ( b1changed ) {
						p[2].reset(state);
						p[2].bind(Term.merge(b, 2));
					}
					b[2] = p[2].nextBinding(state);
					if (b[2] == null)
						b[1] = null;
					else
						bestMatch = Math.max(bestMatch, 2);
					b2changed = true;
				}
				if ( b2changed ) {
					p[3].reset(state);
					p[3].bind(Term.merge(b, 3));
				}
				b[3] = p[3].nextBinding(state);
				if (b[3] == null)
					b[2] = null;
				else
					bestMatch = Math.max(bestMatch, 3);
			}

			Term[] retVal = Term.merge(b, 4);
			b[3] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of Operator #-1 for primitive task !free [unknown source pos]";
		}
	}

	/**
	 * Operator #-1 for primitive task !free
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator2 extends Operator
{
	/**
	 * Operator #-1 for primitive task !free
	 */
		public Operator2(Domain owner)
		{
			super(owner, new Predicate(2, 8, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), -1, -1, new TermNumber(1.0));

			Term[] unifier;


			DelAddElement[] delIn = new DelAddElement[4];
			delIn[0] = new DelAddAtomic(new Predicate(5, 8, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(4), TermList.NIL))))));
			delIn[1] = new DelAddAtomic(new Predicate(8, 8, new TermList(owner.getTermVariable(2), TermList.NIL)));
			delIn[2] = new DelAddAtomic(new Predicate(6, 8, new TermList(owner.getTermVariable(5), TermList.NIL)));
		// Delete list of DelAddElement #4 of Operator #-1 for primitive task !free
		unifier = new Term[8];

		unifier[0] = null;
		unifier[1] = null;
		unifier[2] = null;
		unifier[3] = null;
		unifier[4] = null;
		unifier[5] = null;
		unifier[6] = null;
		unifier[7] = null;

		Predicate[] atoms1 = {
			new Predicate(7, 8, new TermList(owner.getTermVariable(6), new TermList(owner.getTermVariable(7), new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))))) };
			delIn[3] = new DelAddForAll(new PreconditionAtomic(new Predicate(7, 8, new TermList(owner.getTermVariable(6), new TermList(owner.getTermVariable(7), new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))))), unifier), atoms1);

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[4];
			addIn[0] = new DelAddAtomic(new Predicate(9, 8, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			addIn[1] = new DelAddAtomic(new Predicate(7, 8, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(4), TermList.NIL))))));
			addIn[2] = new DelAddAtomic(new Predicate(8, 8, new TermList(new TermCall(new List(owner.getTermVariable(2), new TermList(new TermNumber(1.0), TermList.NIL)), StdLib.minus, "StdLib.minus"), TermList.NIL)));
			addIn[3] = new DelAddAtomic(new Predicate(6, 8, new TermList(new TermCall(new List(owner.getTermVariable(5), new TermList(new TermCall(new List(owner.getTermVariable(3), new TermList(new TermNumber(1.0), TermList.NIL)), ((freecell)owner).calculateCheckNull, "((freecell)owner).calculateCheckNull"), TermList.NIL)), StdLib.plus, "StdLib.plus"), TermList.NIL)));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !free [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new Precondition1(owner, unifier)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Precondition of Operator #-1 for primitive task !unfree
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition2 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition2(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(8, 6, new TermList(owner.getTermVariable(4), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(6, 6, new TermList(owner.getTermVariable(5), TermList.NIL)), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of Operator #-1 for primitive task !unfree [unknown source pos]";
		}
	}

	/**
	 * Operator #-1 for primitive task !unfree
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator3 extends Operator
{
	/**
	 * Operator #-1 for primitive task !unfree
	 */
		public Operator3(Domain owner)
		{
			super(owner, new Predicate(3, 6, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))))), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[3];
			delIn[0] = new DelAddAtomic(new Predicate(9, 6, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			delIn[1] = new DelAddAtomic(new Predicate(8, 6, new TermList(owner.getTermVariable(4), TermList.NIL)));
			delIn[2] = new DelAddAtomic(new Predicate(6, 6, new TermList(owner.getTermVariable(5), TermList.NIL)));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[4];
			addIn[0] = new DelAddAtomic(new Predicate(5, 6, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))))));
			addIn[1] = new DelAddAtomic(new Predicate(7, 6, new TermList(owner.getTermConstant(10) /*dummy*/, new TermList(owner.getTermConstant(10) /*dummy*/, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))))));
			addIn[2] = new DelAddAtomic(new Predicate(8, 6, new TermList(new TermCall(new List(owner.getTermVariable(4), new TermList(new TermNumber(1.0), TermList.NIL)), StdLib.plus, "StdLib.plus"), TermList.NIL)));
			addIn[3] = new DelAddAtomic(new Predicate(6, 6, new TermList(new TermCall(new List(owner.getTermVariable(5), new TermList(new TermCall(new List(owner.getTermVariable(2), new TermList(new TermNumber(-1.0), TermList.NIL)), ((freecell)owner).calculateCheckNull, "((freecell)owner).calculateCheckNull"), TermList.NIL)), StdLib.plus, "StdLib.plus"), TermList.NIL)));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !unfree [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new Precondition2(owner, unifier)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Precondition of Operator #-1 for primitive task !finish
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition3 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition3(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(5, 9, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(6, 9, new TermList(owner.getTermVariable(4), TermList.NIL)), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of Operator #-1 for primitive task !finish [unknown source pos]";
		}
	}

	/**
	 * Operator #-1 for primitive task !finish
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator4 extends Operator
{
	/**
	 * Operator #-1 for primitive task !finish
	 */
		public Operator4(Domain owner)
		{
			super(owner, new Predicate(4, 9, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), -1, -1, new TermNumber(1.0));

			Term[] unifier;


			DelAddElement[] delIn = new DelAddElement[4];
			delIn[0] = new DelAddAtomic(new Predicate(5, 9, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))))));
			delIn[1] = new DelAddAtomic(new Predicate(1, 9, new TermList(owner.getTermVariable(0), new TermList(new TermCall(new List(owner.getTermVariable(1), new TermList(new TermNumber(1.0), TermList.NIL)), StdLib.minus, "StdLib.minus"), TermList.NIL))));
			delIn[2] = new DelAddAtomic(new Predicate(6, 9, new TermList(owner.getTermVariable(4), TermList.NIL)));
		// Delete list of DelAddElement #4 of Operator #-1 for primitive task !finish
		unifier = new Term[9];

		unifier[0] = null;
		unifier[1] = null;
		unifier[2] = null;
		unifier[3] = null;
		unifier[4] = null;
		unifier[5] = null;
		unifier[6] = null;
		unifier[7] = null;
		unifier[8] = null;

		Predicate[] atoms2 = {
			new Predicate(7, 9, new TermList(owner.getTermVariable(5), new TermList(owner.getTermVariable(6), new TermList(owner.getTermVariable(7), new TermList(owner.getTermVariable(8), TermList.NIL))))) };
			delIn[3] = new DelAddForAll(new PreconditionAtomic(new Predicate(7, 9, new TermList(owner.getTermVariable(5), new TermList(owner.getTermVariable(6), new TermList(owner.getTermVariable(7), new TermList(owner.getTermVariable(8), TermList.NIL))))), unifier), atoms2);

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[2];
			addIn[0] = new DelAddAtomic(new Predicate(1, 9, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			addIn[1] = new DelAddAtomic(new Predicate(6, 9, new TermList(new TermCall(new List(owner.getTermVariable(4), new TermList(new TermCall(new List(owner.getTermVariable(2), new TermList(new TermNumber(1.0), TermList.NIL)), ((freecell)owner).calculateCheckNull, "((freecell)owner).calculateCheckNull"), TermList.NIL)), StdLib.plus, "StdLib.plus"), TermList.NIL)));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !finish [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new Precondition3(owner, unifier)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Operator #-1 for primitive task !finish-from-cell
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator5 extends Operator
{
	/**
	 * Operator #-1 for primitive task !finish-from-cell
	 */
		public Operator5(Domain owner)
		{
			super(owner, new Predicate(5, 7, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), -1, -1, new TermNumber(1.0));

			Term[] unifier;


			DelAddElement[] delIn = new DelAddElement[4];
			delIn[0] = new DelAddAtomic(new Predicate(9, 7, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			delIn[1] = new DelAddAtomic(new Predicate(1, 7, new TermList(owner.getTermVariable(0), new TermList(new TermCall(new List(owner.getTermVariable(1), new TermList(new TermNumber(1.0), TermList.NIL)), StdLib.minus, "StdLib.minus"), TermList.NIL))));
			delIn[2] = new DelAddAtomic(new Predicate(8, 7, new TermList(owner.getTermVariable(2), TermList.NIL)));
		// Delete list of DelAddElement #4 of Operator #-1 for primitive task !finish-from-cell
		unifier = new Term[7];

		unifier[0] = null;
		unifier[1] = null;
		unifier[2] = null;
		unifier[3] = null;
		unifier[4] = null;
		unifier[5] = null;
		unifier[6] = null;

		Predicate[] atoms3 = {
			new Predicate(7, 7, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(4), new TermList(owner.getTermVariable(5), new TermList(owner.getTermVariable(6), TermList.NIL))))) };
			delIn[3] = new DelAddForAll(new PreconditionAtomic(new Predicate(7, 7, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(4), new TermList(owner.getTermVariable(5), new TermList(owner.getTermVariable(6), TermList.NIL))))), unifier), atoms3);

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[2];
			addIn[0] = new DelAddAtomic(new Predicate(1, 7, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			addIn[1] = new DelAddAtomic(new Predicate(8, 7, new TermList(new TermCall(new List(owner.getTermVariable(2), new TermList(new TermNumber(1.0), TermList.NIL)), StdLib.plus, "StdLib.plus"), TermList.NIL)));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !finish-from-cell [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionAtomic(new Predicate(8, 7, new TermList(owner.getTermVariable(2), TermList.NIL)), unifier)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task bottom-out-of-the-way
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition4 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition4(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(1, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionCall)
			p[2] = new PreconditionCall(new TermCall(new List(new TermCall(new List(owner.getTermVariable(2), new TermList(new TermNumber(1.0), TermList.NIL)), StdLib.plus, "StdLib.plus"), new TermList(owner.getTermVariable(1), TermList.NIL)), StdLib.equal, "StdLib.equal"), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task bottom-out-of-the-way [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task bottom-out-of-the-way
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method0 extends Method
	{
	/**
	 * Method -1 for compound task bottom-out-of-the-way
	 */
		public Method0(Domain owner)
		{
			super(owner, new Predicate(0, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(4, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), false, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task bottom-out-of-the-way [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition4(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "bottom-out-of-the-way-finish";
				default: return null;
			}
		}
	}

	/**
	 * Method -1 for compound task bottom-out-of-the-way
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method1 extends Method
	{
	/**
	 * Method -1 for compound task bottom-out-of-the-way
	 */
		public Method1(Domain owner)
		{
			super(owner, new Predicate(0, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(1, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))))), false, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task bottom-out-of-the-way [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionAtomic(new Predicate(11, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))))), unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "bottom-out-of-the-way-move";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task bottom-out-of-the-way
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition5 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition5(Domain owner, Term[] unifier)
		{
			p = new Precondition[4];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[1] = new PreconditionNegation(new PreconditionAtomic(new Predicate(7, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(TermList.NIL, new TermList(TermList.NIL, TermList.NIL))))), unifier), 3);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(6, 3, new TermList(owner.getTermVariable(2), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionCall)
			p[3] = new PreconditionCall(new TermCall(new List(owner.getTermVariable(2), new TermList(new TermNumber(0.0), TermList.NIL)), StdLib.more, "StdLib.more"), unifier);
			b = new Term[4][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[3] == null)
			{
				boolean b2changed = false;
				while (b[2] == null)
				{
					boolean b1changed = false;
					while (b[1] == null)
					{
						b[1] = p[1].nextBinding(state);
						if (b[1] == null)
							return null;
						else
							bestMatch = Math.max(bestMatch, 1);
						b1changed = true;
					}
					if ( b1changed ) {
						p[2].reset(state);
						p[2].bind(Term.merge(b, 2));
					}
					b[2] = p[2].nextBinding(state);
					if (b[2] == null)
						b[1] = null;
					else
						bestMatch = Math.max(bestMatch, 2);
					b2changed = true;
				}
				if ( b2changed ) {
					p[3].reset(state);
					p[3].bind(Term.merge(b, 3));
				}
				b[3] = p[3].nextBinding(state);
				if (b[3] == null)
					b[2] = null;
				else
					bestMatch = Math.max(bestMatch, 3);
			}

			Term[] retVal = Term.merge(b, 4);
			b[3] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task bottom-out-of-the-way [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task bottom-out-of-the-way
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method2 extends Method
	{
	/**
	 * Method -1 for compound task bottom-out-of-the-way
	 */
		public Method2(Domain owner)
		{
			super(owner, new Predicate(0, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(1, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(TermList.NIL, new TermList(TermList.NIL, TermList.NIL))))), false, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task bottom-out-of-the-way [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition5(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "bottom-out-of-the-way-col";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task bottom-out-of-the-way
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition6 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition6(Domain owner, Term[] unifier)
		{
			p = new Precondition[4];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[1] = new PreconditionNegation(new PreconditionAtomic(new Predicate(7, 3, new TermList(owner.getTermConstant(10) /*dummy*/, new TermList(owner.getTermConstant(10) /*dummy*/, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))))), unifier), 3);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(8, 3, new TermList(owner.getTermVariable(2), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionCall)
			p[3] = new PreconditionCall(new TermCall(new List(owner.getTermVariable(2), new TermList(new TermNumber(0.0), TermList.NIL)), StdLib.more, "StdLib.more"), unifier);
			b = new Term[4][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[3] == null)
			{
				boolean b2changed = false;
				while (b[2] == null)
				{
					boolean b1changed = false;
					while (b[1] == null)
					{
						b[1] = p[1].nextBinding(state);
						if (b[1] == null)
							return null;
						else
							bestMatch = Math.max(bestMatch, 1);
						b1changed = true;
					}
					if ( b1changed ) {
						p[2].reset(state);
						p[2].bind(Term.merge(b, 2));
					}
					b[2] = p[2].nextBinding(state);
					if (b[2] == null)
						b[1] = null;
					else
						bestMatch = Math.max(bestMatch, 2);
					b2changed = true;
				}
				if ( b2changed ) {
					p[3].reset(state);
					p[3].bind(Term.merge(b, 3));
				}
				b[3] = p[3].nextBinding(state);
				if (b[3] == null)
					b[2] = null;
				else
					bestMatch = Math.max(bestMatch, 3);
			}

			Term[] retVal = Term.merge(b, 4);
			b[3] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task bottom-out-of-the-way [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task bottom-out-of-the-way
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method3 extends Method
	{
	/**
	 * Method -1 for compound task bottom-out-of-the-way
	 */
		public Method3(Domain owner)
		{
			super(owner, new Predicate(0, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(2, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), false, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task bottom-out-of-the-way [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition6(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "bottom-out-of-the-way-cell";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task bottom-out-of-the-way
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition7 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition7(Domain owner, Term[] unifier)
		{
			p = new Precondition[4];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(1, 7, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionCall)
			p[2] = new PreconditionCall(new TermCall(new List(new TermCall(new List(owner.getTermVariable(3), new TermList(new TermNumber(1.0), TermList.NIL)), StdLib.plus, "StdLib.plus"), new TermList(owner.getTermVariable(4), TermList.NIL)), StdLib.equal, "StdLib.equal"), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[3] = new PreconditionNegation(new PreconditionAtomic(new Predicate(5, 7, new TermList(owner.getTermVariable(5), new TermList(owner.getTermVariable(6), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(4), TermList.NIL))))), unifier), 7);
			b = new Term[4][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[3] == null)
			{
				boolean b2changed = false;
				while (b[2] == null)
				{
					boolean b1changed = false;
					while (b[1] == null)
					{
						b[1] = p[1].nextBinding(state);
						if (b[1] == null)
							return null;
						else
							bestMatch = Math.max(bestMatch, 1);
						b1changed = true;
					}
					if ( b1changed ) {
						p[2].reset(state);
						p[2].bind(Term.merge(b, 2));
					}
					b[2] = p[2].nextBinding(state);
					if (b[2] == null)
						b[1] = null;
					else
						bestMatch = Math.max(bestMatch, 2);
					b2changed = true;
				}
				if ( b2changed ) {
					p[3].reset(state);
					p[3].bind(Term.merge(b, 3));
				}
				b[3] = p[3].nextBinding(state);
				if (b[3] == null)
					b[2] = null;
				else
					bestMatch = Math.max(bestMatch, 3);
			}

			Term[] retVal = Term.merge(b, 4);
			b[3] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task bottom-out-of-the-way [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task bottom-out-of-the-way
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method4 extends Method
	{
	/**
	 * Method -1 for compound task bottom-out-of-the-way
	 */
		public Method4(Domain owner)
		{
			super(owner, new Predicate(0, 7, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(2, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(4, 7, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(4), TermList.NIL))), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(0, 7, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), true, false));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task bottom-out-of-the-way [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition7(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "bottom-out-of-the-way-other-finish";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task bottom-out-of-the-way
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition8 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition8(Domain owner, Term[] unifier)
		{
			p = new Precondition[4];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(9, 5, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(1, 5, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(4), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionCall)
			p[3] = new PreconditionCall(new TermCall(new List(new TermCall(new List(owner.getTermVariable(4), new TermList(new TermNumber(1.0), TermList.NIL)), StdLib.plus, "StdLib.plus"), new TermList(owner.getTermVariable(3), TermList.NIL)), StdLib.equal, "StdLib.equal"), unifier);
			b = new Term[4][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[3] == null)
			{
				boolean b2changed = false;
				while (b[2] == null)
				{
					boolean b1changed = false;
					while (b[1] == null)
					{
						b[1] = p[1].nextBinding(state);
						if (b[1] == null)
							return null;
						else
							bestMatch = Math.max(bestMatch, 1);
						b1changed = true;
					}
					if ( b1changed ) {
						p[2].reset(state);
						p[2].bind(Term.merge(b, 2));
					}
					b[2] = p[2].nextBinding(state);
					if (b[2] == null)
						b[1] = null;
					else
						bestMatch = Math.max(bestMatch, 2);
					b2changed = true;
				}
				if ( b2changed ) {
					p[3].reset(state);
					p[3].bind(Term.merge(b, 3));
				}
				b[3] = p[3].nextBinding(state);
				if (b[3] == null)
					b[2] = null;
				else
					bestMatch = Math.max(bestMatch, 3);
			}

			Term[] retVal = Term.merge(b, 4);
			b[3] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task bottom-out-of-the-way [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task bottom-out-of-the-way
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method5 extends Method
	{
	/**
	 * Method -1 for compound task bottom-out-of-the-way
	 */
		public Method5(Domain owner)
		{
			super(owner, new Predicate(0, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(2, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(5, 5, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(0, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), true, false));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task bottom-out-of-the-way [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition8(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "bottom-out-of-the-way-other-finish-cell";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task bottom-out-of-the-way
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition9 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition9(Domain owner, Term[] unifier)
		{
			p = new Precondition[4];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(9, 6, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(11, 6, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(4), new TermList(owner.getTermVariable(5), TermList.NIL))))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionCall)
			p[3] = new PreconditionCall(new TermCall(new List(new TermList(owner.getTermVariable(4), new TermList(owner.getTermVariable(5), TermList.NIL)), new TermList(new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL)), TermList.NIL)), StdLib.notEq, "StdLib.notEq"), unifier);
			b = new Term[4][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[3] == null)
			{
				boolean b2changed = false;
				while (b[2] == null)
				{
					boolean b1changed = false;
					while (b[1] == null)
					{
						b[1] = p[1].nextBinding(state);
						if (b[1] == null)
							return null;
						else
							bestMatch = Math.max(bestMatch, 1);
						b1changed = true;
					}
					if ( b1changed ) {
						p[2].reset(state);
						p[2].bind(Term.merge(b, 2));
					}
					b[2] = p[2].nextBinding(state);
					if (b[2] == null)
						b[1] = null;
					else
						bestMatch = Math.max(bestMatch, 2);
					b2changed = true;
				}
				if ( b2changed ) {
					p[3].reset(state);
					p[3].bind(Term.merge(b, 3));
				}
				b[3] = p[3].nextBinding(state);
				if (b[3] == null)
					b[2] = null;
				else
					bestMatch = Math.max(bestMatch, 3);
			}

			Term[] retVal = Term.merge(b, 4);
			b[3] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task bottom-out-of-the-way [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task bottom-out-of-the-way
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method6 extends Method
	{
	/**
	 * Method -1 for compound task bottom-out-of-the-way
	 */
		public Method6(Domain owner)
		{
			super(owner, new Predicate(0, 6, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(2, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(3, 6, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(4), new TermList(owner.getTermVariable(5), TermList.NIL))))), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(0, 6, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), true, false));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task bottom-out-of-the-way [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition9(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "bottom-out-of-the-way-other-unfree";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task bottom-out-of-the-way
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition10 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition10(Domain owner, Term[] unifier)
		{
			p = new Precondition[5];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(9, 5, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[2] = new PreconditionNegation(new PreconditionAtomic(new Predicate(7, 5, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), new TermList(TermList.NIL, new TermList(TermList.NIL, TermList.NIL))))), unifier), 5);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(6, 5, new TermList(owner.getTermVariable(4), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionCall)
			p[4] = new PreconditionCall(new TermCall(new List(owner.getTermVariable(4), new TermList(new TermNumber(0.0), TermList.NIL)), StdLib.more, "StdLib.more"), unifier);
			b = new Term[5][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[4] == null)
			{
				boolean b3changed = false;
				while (b[3] == null)
				{
					boolean b2changed = false;
					while (b[2] == null)
					{
						boolean b1changed = false;
						while (b[1] == null)
						{
							b[1] = p[1].nextBinding(state);
							if (b[1] == null)
								return null;
							else
								bestMatch = Math.max(bestMatch, 1);
							b1changed = true;
						}
						if ( b1changed ) {
							p[2].reset(state);
							p[2].bind(Term.merge(b, 2));
						}
						b[2] = p[2].nextBinding(state);
						if (b[2] == null)
							b[1] = null;
						else
							bestMatch = Math.max(bestMatch, 2);
						b2changed = true;
					}
					if ( b2changed ) {
						p[3].reset(state);
						p[3].bind(Term.merge(b, 3));
					}
					b[3] = p[3].nextBinding(state);
					if (b[3] == null)
						b[2] = null;
					else
						bestMatch = Math.max(bestMatch, 3);
					b3changed = true;
				}
				if ( b3changed ) {
					p[4].reset(state);
					p[4].bind(Term.merge(b, 4));
				}
				b[4] = p[4].nextBinding(state);
				if (b[4] == null)
					b[3] = null;
				else
					bestMatch = Math.max(bestMatch, 4);
			}

			Term[] retVal = Term.merge(b, 5);
			b[4] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			p[4].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task bottom-out-of-the-way [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task bottom-out-of-the-way
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method7 extends Method
	{
	/**
	 * Method -1 for compound task bottom-out-of-the-way
	 */
		public Method7(Domain owner)
		{
			super(owner, new Predicate(0, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(2, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(3, 5, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), new TermList(TermList.NIL, new TermList(TermList.NIL, TermList.NIL))))), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(0, 5, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), true, false));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task bottom-out-of-the-way [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition10(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "bottom-out-of-the-way-other-unfree-col";
				default: return null;
			}
		}
	}

	/**
	 * Method -1 for compound task out-of-the-way
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method8 extends Method
	{
	/**
	 * Method -1 for compound task out-of-the-way
	 */
		public Method8(Domain owner)
		{
			super(owner, new Predicate(1, 6, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))))));
			TaskList[] subsIn = new TaskList[3];

			subsIn[0] = TaskList.empty;
			subsIn[1] = createTaskList1();
			subsIn[2] = createTaskList2();

			setSubs(subsIn);
		}

		TaskList createTaskList1()
		{
			TaskList retVal;

			retVal = new TaskList(2, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(1, 6, new TermList(owner.getTermVariable(4), new TermList(owner.getTermVariable(5), new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))))), false, false));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(1, 6, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))))), true, false));

			return retVal;
		}

		TaskList createTaskList2()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 6, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), false, false));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task out-of-the-way [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionNegation(new PreconditionAtomic(new Predicate(12, 6, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))))), unifier), 6)).setComparator(null);
				break;
				case 1:
					p = (new PreconditionAtomic(new Predicate(5, 6, new TermList(owner.getTermVariable(4), new TermList(owner.getTermVariable(5), new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))))), unifier)).setComparator(null);
				break;
				case 2:
					p = (new PreconditionNil(6)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
				case 1:
				break;
				case 2:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method8Branch0";
				case 1: return "Method8Branch1";
				case 2: return "Method8Branch2";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task do-card
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition11 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition11(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(1, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionCall)
			p[2] = new PreconditionCall(new TermCall(new List(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL)), StdLib.lessEq, "StdLib.lessEq"), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task do-card [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task do-card
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method9 extends Method
	{
	/**
	 * Method -1 for compound task do-card
	 */
		public Method9(Domain owner)
		{
			super(owner, new Predicate(2, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			TaskList[] subsIn = new TaskList[4];

			subsIn[0] = TaskList.empty;
			subsIn[1] = createTaskList1();
			subsIn[2] = createTaskList2();
			subsIn[3] = createTaskList3();

			setSubs(subsIn);
		}

		TaskList createTaskList1()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(5, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), false, true));

			return retVal;
		}

		TaskList createTaskList2()
		{
			TaskList retVal;

			retVal = new TaskList(2, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(1, 4, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))))), false, false));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(4, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), true, true));

			return retVal;
		}

		TaskList createTaskList3()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(4, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), false, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task do-card [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition11(owner, unifier)).setComparator(null);
				break;
				case 1:
					p = (new PreconditionAtomic(new Predicate(9, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier)).setComparator(null);
				break;
				case 2:
					p = (new PreconditionAtomic(new Predicate(5, 4, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))))), unifier)).setComparator(null);
				break;
				case 3:
					p = (new PreconditionNil(4)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
				case 1:
				break;
				case 2:
				break;
				case 3:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method9Branch0";
				case 1: return "Method9Branch1";
				case 2: return "Method9Branch2";
				case 3: return "Method9Branch3";
				default: return null;
			}
		}
	}

	/**
	 * Branch -1 for axiom diff-clr
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalAxiom)
	 */
	public static class Axiom0 extends Axiom
{
	/**
	 * Branch -1 for axiom diff-clr
	 */
		public Axiom0(Domain owner)
		{
			super(owner, new Predicate(13, 0, new TermList(owner.getTermConstant(0) /*s*/, new TermList(owner.getTermConstant(2) /*h*/, TermList.NIL))), 1);
		}

		@Override
		public String toString()
		{
			return "Branch -1 for axiom diff-clr [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionNil(0)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Axiom0Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Branch -1 for axiom diff-clr
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalAxiom)
	 */
	public static class Axiom1 extends Axiom
{
	/**
	 * Branch -1 for axiom diff-clr
	 */
		public Axiom1(Domain owner)
		{
			super(owner, new Predicate(13, 0, new TermList(owner.getTermConstant(0) /*s*/, new TermList(owner.getTermConstant(3) /*d*/, TermList.NIL))), 1);
		}

		@Override
		public String toString()
		{
			return "Branch -1 for axiom diff-clr [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionNil(0)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Axiom1Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Branch -1 for axiom diff-clr
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalAxiom)
	 */
	public static class Axiom2 extends Axiom
{
	/**
	 * Branch -1 for axiom diff-clr
	 */
		public Axiom2(Domain owner)
		{
			super(owner, new Predicate(13, 0, new TermList(owner.getTermConstant(2) /*h*/, new TermList(owner.getTermConstant(0) /*s*/, TermList.NIL))), 1);
		}

		@Override
		public String toString()
		{
			return "Branch -1 for axiom diff-clr [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionNil(0)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Axiom2Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Branch -1 for axiom diff-clr
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalAxiom)
	 */
	public static class Axiom3 extends Axiom
{
	/**
	 * Branch -1 for axiom diff-clr
	 */
		public Axiom3(Domain owner)
		{
			super(owner, new Predicate(13, 0, new TermList(owner.getTermConstant(2) /*h*/, new TermList(owner.getTermConstant(4) /*c*/, TermList.NIL))), 1);
		}

		@Override
		public String toString()
		{
			return "Branch -1 for axiom diff-clr [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionNil(0)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Axiom3Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Branch -1 for axiom diff-clr
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalAxiom)
	 */
	public static class Axiom4 extends Axiom
{
	/**
	 * Branch -1 for axiom diff-clr
	 */
		public Axiom4(Domain owner)
		{
			super(owner, new Predicate(13, 0, new TermList(owner.getTermConstant(3) /*d*/, new TermList(owner.getTermConstant(0) /*s*/, TermList.NIL))), 1);
		}

		@Override
		public String toString()
		{
			return "Branch -1 for axiom diff-clr [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionNil(0)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Axiom4Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Branch -1 for axiom diff-clr
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalAxiom)
	 */
	public static class Axiom5 extends Axiom
{
	/**
	 * Branch -1 for axiom diff-clr
	 */
		public Axiom5(Domain owner)
		{
			super(owner, new Predicate(13, 0, new TermList(owner.getTermConstant(3) /*d*/, new TermList(owner.getTermConstant(4) /*c*/, TermList.NIL))), 1);
		}

		@Override
		public String toString()
		{
			return "Branch -1 for axiom diff-clr [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionNil(0)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Axiom5Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Branch -1 for axiom diff-clr
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalAxiom)
	 */
	public static class Axiom6 extends Axiom
{
	/**
	 * Branch -1 for axiom diff-clr
	 */
		public Axiom6(Domain owner)
		{
			super(owner, new Predicate(13, 0, new TermList(owner.getTermConstant(4) /*c*/, new TermList(owner.getTermConstant(2) /*h*/, TermList.NIL))), 1);
		}

		@Override
		public String toString()
		{
			return "Branch -1 for axiom diff-clr [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionNil(0)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Axiom6Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Branch -1 for axiom diff-clr
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalAxiom)
	 */
	public static class Axiom7 extends Axiom
{
	/**
	 * Branch -1 for axiom diff-clr
	 */
		public Axiom7(Domain owner)
		{
			super(owner, new Predicate(13, 0, new TermList(owner.getTermConstant(4) /*c*/, new TermList(owner.getTermConstant(3) /*d*/, TermList.NIL))), 1);
		}

		@Override
		public String toString()
		{
			return "Branch -1 for axiom diff-clr [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionNil(0)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Axiom7Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Precondition of branch #0 of Branch -1 for axiom movable
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition12 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition12(Domain owner, Term[] unifier)
		{
			p = new Precondition[6];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAssignment)
			p[1] = new PreconditionAssign(new TermCall(new List(owner.getTermVariable(1), new TermList(new TermNumber(1.0), TermList.NIL)), StdLib.plus, "StdLib.plus"), unifier, 3);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(13, 8, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(5, 8, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(4), new TermList(owner.getTermVariable(5), TermList.NIL))))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[4] = new PreconditionNegation(new PreconditionAtomic(new Predicate(5, 8, new TermList(owner.getTermVariable(6), new TermList(owner.getTermVariable(7), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))))), unifier), 8);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[5] = new PreconditionNegation(new PreconditionAtomic(new Predicate(7, 8, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))))), unifier), 8);
			b = new Term[6][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[5] == null)
			{
				boolean b4changed = false;
				while (b[4] == null)
				{
					boolean b3changed = false;
					while (b[3] == null)
					{
						boolean b2changed = false;
						while (b[2] == null)
						{
							boolean b1changed = false;
							while (b[1] == null)
							{
								b[1] = p[1].nextBinding(state);
								if (b[1] == null)
									return null;
								else
									bestMatch = Math.max(bestMatch, 1);
								b1changed = true;
							}
							if ( b1changed ) {
								p[2].reset(state);
								p[2].bind(Term.merge(b, 2));
							}
							b[2] = p[2].nextBinding(state);
							if (b[2] == null)
								b[1] = null;
							else
								bestMatch = Math.max(bestMatch, 2);
							b2changed = true;
						}
						if ( b2changed ) {
							p[3].reset(state);
							p[3].bind(Term.merge(b, 3));
						}
						b[3] = p[3].nextBinding(state);
						if (b[3] == null)
							b[2] = null;
						else
							bestMatch = Math.max(bestMatch, 3);
						b3changed = true;
					}
					if ( b3changed ) {
						p[4].reset(state);
						p[4].bind(Term.merge(b, 4));
					}
					b[4] = p[4].nextBinding(state);
					if (b[4] == null)
						b[3] = null;
					else
						bestMatch = Math.max(bestMatch, 4);
					b4changed = true;
				}
				if ( b4changed ) {
					p[5].reset(state);
					p[5].bind(Term.merge(b, 5));
				}
				b[5] = p[5].nextBinding(state);
				if (b[5] == null)
					b[4] = null;
				else
					bestMatch = Math.max(bestMatch, 5);
			}

			Term[] retVal = Term.merge(b, 6);
			b[5] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			p[4].reset(state);
			p[5].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of branch #0 of Branch -1 for axiom movable [unknown source pos]";
		}
	}

	/**
	 * Branch -1 for axiom movable
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalAxiom)
	 */
	public static class Axiom8 extends Axiom
{
	/**
	 * Branch -1 for axiom movable
	 */
		public Axiom8(Domain owner)
		{
			super(owner, new Predicate(11, 8, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))))), 1);
		}

		@Override
		public String toString()
		{
			return "Branch -1 for axiom movable [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition12(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Axiom8Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Precondition of branch #1 of Branch -1 for axiom on-top
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition13 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition13(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(5, 6, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(4), new TermList(owner.getTermVariable(5), TermList.NIL))))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(12, 6, new TermList(owner.getTermVariable(4), new TermList(owner.getTermVariable(5), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))))), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of branch #1 of Branch -1 for axiom on-top [unknown source pos]";
		}
	}

	/**
	 * Branch -1 for axiom on-top
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalAxiom)
	 */
	public static class Axiom9 extends Axiom
{
	/**
	 * Branch -1 for axiom on-top
	 */
		public Axiom9(Domain owner)
		{
			super(owner, new Predicate(12, 6, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))))), 2);
		}

		@Override
		public String toString()
		{
			return "Branch -1 for axiom on-top [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionAtomic(new Predicate(5, 6, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(3), TermList.NIL))))), unifier)).setComparator(null);
				break;
				case 1:
					p = (new Precondition13(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Axiom9Branch0";
				case 1: return "Axiom9Branch1";
				default: return null;
			}
		}
	}

	public static final String sourcePath = "/Users/gcc/workspace/jshop2cp/examples/freecell/freecell";
	public static final long sourceLastModified = 1328619440000L;

	public CheckNull calculateCheckNull = new CheckNull();

	public freecell()
	{
		constants = new String[14];
		constants[0] = "s";
		constants[1] = "done";
		constants[2] = "h";
		constants[3] = "d";
		constants[4] = "c";
		constants[5] = "on";
		constants[6] = "empty-cols";
		constants[7] = "forbidden";
		constants[8] = "empty-cells";
		constants[9] = "in-cell";
		constants[10] = "dummy";
		constants[11] = "movable";
		constants[12] = "on-top";
		constants[13] = "diff-clr";

		compoundTasks = new String[3];
		compoundTasks[0] = "bottom-out-of-the-way";
		compoundTasks[1] = "out-of-the-way";
		compoundTasks[2] = "do-card";

		primitiveTasks = new String[6];
		primitiveTasks[0] = "!!done";
		primitiveTasks[1] = "!move";
		primitiveTasks[2] = "!free";
		primitiveTasks[3] = "!unfree";
		primitiveTasks[4] = "!finish";
		primitiveTasks[5] = "!finish-from-cell";

		initializeTermVariables(9);

		initializeTermConstants();

		methods = new Method[3][];

		methods[0] = new Method[8];
		methods[0][0] = new Method0(this);
		methods[0][1] = new Method1(this);
		methods[0][2] = new Method2(this);
		methods[0][3] = new Method3(this);
		methods[0][4] = new Method4(this);
		methods[0][5] = new Method5(this);
		methods[0][6] = new Method6(this);
		methods[0][7] = new Method7(this);

		methods[1] = new Method[1];
		methods[1][0] = new Method8(this);

		methods[2] = new Method[1];
		methods[2][0] = new Method9(this);


		ops = new Operator[6][];

		ops[0] = new Operator[1];
		ops[0][0] = new Operator0(this);

		ops[1] = new Operator[1];
		ops[1][0] = new Operator1(this);

		ops[2] = new Operator[1];
		ops[2][0] = new Operator2(this);

		ops[3] = new Operator[1];
		ops[3][0] = new Operator3(this);

		ops[4] = new Operator[1];
		ops[4][0] = new Operator4(this);

		ops[5] = new Operator[1];
		ops[5][0] = new Operator5(this);

		axioms = new Axiom[14][];

		axioms[0] = new Axiom[0];

		axioms[1] = new Axiom[0];

		axioms[2] = new Axiom[0];

		axioms[3] = new Axiom[0];

		axioms[4] = new Axiom[0];

		axioms[5] = new Axiom[0];

		axioms[6] = new Axiom[0];

		axioms[7] = new Axiom[0];

		axioms[8] = new Axiom[0];

		axioms[9] = new Axiom[0];

		axioms[10] = new Axiom[0];

		axioms[11] = new Axiom[1];
		axioms[11][0] = new Axiom8(this);

		axioms[12] = new Axiom[1];
		axioms[12][0] = new Axiom9(this);

		axioms[13] = new Axiom[8];
		axioms[13][0] = new Axiom0(this);
		axioms[13][1] = new Axiom1(this);
		axioms[13][2] = new Axiom2(this);
		axioms[13][3] = new Axiom3(this);
		axioms[13][4] = new Axiom4(this);
		axioms[13][5] = new Axiom5(this);
		axioms[13][6] = new Axiom6(this);
		axioms[13][7] = new Axiom7(this);

	}
}