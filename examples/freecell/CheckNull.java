package freecell;

import com.gamalocus.jshop2rt.Calculate;
import com.gamalocus.jshop2rt.List;
import com.gamalocus.jshop2rt.Term;
import com.gamalocus.jshop2rt.TermNumber;



public class CheckNull implements Calculate {
  public Term call(List l) {
    if (l.getHead().isNil())
      return l.getRest().getHead();

    return new TermNumber(0);
  }
}
