package forall;
import com.gamalocus.jshop2rt.*;

public class forallexample extends Domain
{
	private static final long serialVersionUID = -4052720519876063045L;


	/**
	 * Operator #-1 for primitive task !load
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator0 extends Operator
{
	/**
	 * Operator #-1 for primitive task !load
	 */
		public Operator0(Domain owner)
		{
			super(owner, new Predicate(0, 2, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[0];

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[1];
			addIn[0] = new DelAddAtomic(new Predicate(0, 2, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !load [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(2)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Bindings for preconditions of delete part of DelAddElement #2 of Operator #-1 for primitive task !drive
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition0 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition0(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(2, 4, new TermList(owner.getTermVariable(3), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(0, 4, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(0), TermList.NIL))), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Bindings for preconditions of delete part of DelAddElement #2 of Operator #-1 for primitive task !drive [unknown source pos]";
		}
	}

	/**
	 * Bindings for precondition of add part of DelAddElement #2 of Operator #-1 for primitive task !drive
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition1 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition1(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(2, 4, new TermList(owner.getTermVariable(3), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(0, 4, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(0), TermList.NIL))), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Bindings for precondition of add part of DelAddElement #2 of Operator #-1 for primitive task !drive [unknown source pos]";
		}
	}

	/**
	 * Operator #-1 for primitive task !drive
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator1 extends Operator
{
	/**
	 * Operator #-1 for primitive task !drive
	 */
		public Operator1(Domain owner)
		{
			super(owner, new Predicate(1, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL)))), -1, -1, new TermNumber(1.0));

			Term[] unifier;


			DelAddElement[] delIn = new DelAddElement[2];
			delIn[0] = new DelAddAtomic(new Predicate(1, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
		// Delete list of DelAddElement #2 of Operator #-1 for primitive task !drive
		unifier = new Term[4];

		unifier[0] = null;
		unifier[1] = null;
		unifier[2] = null;
		unifier[3] = null;

		Predicate[] atoms0 = {
			new Predicate(3, 4, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(1), TermList.NIL))) };
			delIn[1] = new DelAddForAll(new Precondition0(owner, unifier), atoms0);

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[2];
			addIn[0] = new DelAddAtomic(new Predicate(1, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))));
		// Add list of DelAddElement #2 of Operator #-1 for primitive task !drive
		unifier = new Term[4];

		unifier[0] = null;
		unifier[1] = null;
		unifier[2] = null;
		unifier[3] = null;

		Predicate[] atoms1 = {
			new Predicate(3, 4, new TermList(owner.getTermVariable(3), new TermList(owner.getTermVariable(2), TermList.NIL))) };
			addIn[1] = new DelAddForAll(new Precondition1(owner, unifier), atoms1);

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !drive [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(4)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task move-packages
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition2 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition2(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(4, 3, new TermList(owner.getTermVariable(2), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(1, 3, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(0), TermList.NIL))), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task move-packages [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task move-packages
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method0 extends Method
	{
	/**
	 * Method -1 for compound task move-packages
	 */
		public Method0(Domain owner)
		{
			super(owner, new Predicate(1, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(2, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), false, false));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(1, 3, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL)))), false, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task move-packages [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition2(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method0Branch0";
				default: return null;
			}
		}
	}

	/**
	 * premise of Precondition #0 of Method -1 for compound task load-packages
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition3 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition3(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(2, 3, new TermList(owner.getTermVariable(2), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(3, 3, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(0), TermList.NIL))), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "premise of Precondition #0 of Method -1 for compound task load-packages [unknown source pos]";
		}
	}

	/**
	 * Precondition #1 of Method -1 for compound task load-packages
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition4 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition4(Domain owner, Term[] unifier)
		{
			p = new Precondition[4];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(2, 3, new TermList(owner.getTermVariable(2), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(3, 3, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(0), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[3] = new PreconditionNegation(new PreconditionAtomic(new Predicate(0, 3, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier), 3);
			b = new Term[4][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[3] == null)
			{
				boolean b2changed = false;
				while (b[2] == null)
				{
					boolean b1changed = false;
					while (b[1] == null)
					{
						b[1] = p[1].nextBinding(state);
						if (b[1] == null)
							return null;
						else
							bestMatch = Math.max(bestMatch, 1);
						b1changed = true;
					}
					if ( b1changed ) {
						p[2].reset(state);
						p[2].bind(Term.merge(b, 2));
					}
					b[2] = p[2].nextBinding(state);
					if (b[2] == null)
						b[1] = null;
					else
						bestMatch = Math.max(bestMatch, 2);
					b2changed = true;
				}
				if ( b2changed ) {
					p[3].reset(state);
					p[3].bind(Term.merge(b, 3));
				}
				b[3] = p[3].nextBinding(state);
				if (b[3] == null)
					b[2] = null;
				else
					bestMatch = Math.max(bestMatch, 3);
			}

			Term[] retVal = Term.merge(b, 4);
			b[3] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #1 of Method -1 for compound task load-packages [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task load-packages
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method1 extends Method
	{
	/**
	 * Method -1 for compound task load-packages
	 */
		public Method1(Domain owner)
		{
			super(owner, new Predicate(0, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			TaskList[] subsIn = new TaskList[2];

			subsIn[0] = TaskList.empty;
			subsIn[1] = createTaskList1();

			setSubs(subsIn);
		}

		TaskList createTaskList1()
		{
			TaskList retVal;

			retVal = new TaskList(2, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 3, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(1), TermList.NIL))), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(0, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), false, false));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task load-packages [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionForAll(new Precondition3(owner, unifier), new PreconditionAtomic(new Predicate(0, 3, new TermList(owner.getTermVariable(2), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier), 3)).setComparator(null);
				break;
				case 1:
					p = (new Precondition4(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
				case 1:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method1Branch0";
				case 1: return "Method1Branch1";
				default: return null;
			}
		}
	}

	public static final String sourcePath = "/Users/gcc/workspace/jshop2cp/examples/forall/forallexample";
	public static final long sourceLastModified = 1328664701000L;

	public forallexample()
	{
		constants = new String[5];
		constants[0] = "in";
		constants[1] = "truck-at";
		constants[2] = "package";
		constants[3] = "at";
		constants[4] = "truck";

		compoundTasks = new String[2];
		compoundTasks[0] = "load-packages";
		compoundTasks[1] = "move-packages";

		primitiveTasks = new String[2];
		primitiveTasks[0] = "!load";
		primitiveTasks[1] = "!drive";

		initializeTermVariables(4);

		initializeTermConstants();

		methods = new Method[2][];

		methods[0] = new Method[1];
		methods[0][0] = new Method1(this);

		methods[1] = new Method[1];
		methods[1][0] = new Method0(this);


		ops = new Operator[2][];

		ops[0] = new Operator[1];
		ops[0][0] = new Operator0(this);

		ops[1] = new Operator[1];
		ops[1][0] = new Operator1(this);

		axioms = new Axiom[5][];

		axioms[0] = new Axiom[0];

		axioms[1] = new Axiom[0];

		axioms[2] = new Axiom[0];

		axioms[3] = new Axiom[0];

		axioms[4] = new Axiom[0];

	}
}