package forall;
import java.util.LinkedList;
import com.gamalocus.jshop2rt.*;

public class problem
{
	public static final String sourcePath = "/Users/gcc/workspace/jshop2cp/examples/forall/problem";
	public static final long sourceLastModified = 1328664718000L;

	public static final Domain owner = new forallexample();

	private static String[] defineConstants(Domain owner)
	{
		String[] problemConstants = new String[8];

		owner.addConstant("t1");
		owner.addConstant("t2");
		owner.addConstant("p1");
		owner.addConstant("p2");
		owner.addConstant("p3");
		owner.addConstant("p4");
		owner.addConstant("city1");
		owner.addConstant("city2");

		return problemConstants;
	}

	private static void createState0(Domain owner, State s)	{
		s.add(new Predicate(4, 0, new TermList(owner.getTermConstant("t1") /*5*/, TermList.NIL)));
		s.add(new Predicate(4, 0, new TermList(owner.getTermConstant("t2") /*6*/, TermList.NIL)));
		s.add(new Predicate(2, 0, new TermList(owner.getTermConstant("p1") /*7*/, TermList.NIL)));
		s.add(new Predicate(2, 0, new TermList(owner.getTermConstant("p2") /*8*/, TermList.NIL)));
		s.add(new Predicate(2, 0, new TermList(owner.getTermConstant("p3") /*9*/, TermList.NIL)));
		s.add(new Predicate(2, 0, new TermList(owner.getTermConstant("p4") /*10*/, TermList.NIL)));
		s.add(new Predicate(1, 0, new TermList(owner.getTermConstant("t1") /*5*/, new TermList(owner.getTermConstant("city1") /*11*/, TermList.NIL))));
		s.add(new Predicate(1, 0, new TermList(owner.getTermConstant("t2") /*6*/, new TermList(owner.getTermConstant("city2") /*12*/, TermList.NIL))));
		s.add(new Predicate(3, 0, new TermList(owner.getTermConstant("p1") /*7*/, new TermList(owner.getTermConstant("city2") /*12*/, TermList.NIL))));
		s.add(new Predicate(3, 0, new TermList(owner.getTermConstant("p2") /*8*/, new TermList(owner.getTermConstant("city1") /*11*/, TermList.NIL))));
		s.add(new Predicate(3, 0, new TermList(owner.getTermConstant("p3") /*9*/, new TermList(owner.getTermConstant("city2") /*12*/, TermList.NIL))));
		s.add(new Predicate(3, 0, new TermList(owner.getTermConstant("p4") /*10*/, new TermList(owner.getTermConstant("city2") /*12*/, TermList.NIL))));
		s.add(new Predicate(0, 0, new TermList(owner.getTermConstant("p3") /*9*/, new TermList(owner.getTermConstant("t2") /*6*/, TermList.NIL))));
	}

	public static LinkedList<Plan> getPlans()
	{
		LinkedList<Plan> returnedPlans = new LinkedList<Plan>();
		defineConstants(owner);

		State s0 = new State(owner.getAxioms());

		createState0(owner, s0); // jShop2Planner.initialize(d, s0);

		TaskList tl;
			tl = new TaskList(1, true);
			tl.subtasks[0] = new TaskList(new TaskAtom(new Predicate(1, 0, new TermList(owner.getTermConstant("city2") /*12*/, new TermList(owner.getTermConstant("city1") /*11*/, TermList.NIL))), false, false));

		JSHOP2 jShop2Planner = new JSHOP2(tl, 100000, new DoubleCost(0.0), owner, s0);
		JSHOP2Provider.setJSHOP2(jShop2Planner);
		while (jShop2Planner.run()) { 
		}
		returnedPlans.addAll(jShop2Planner.getPlans());
		return returnedPlans;
	}

	public static LinkedList<Predicate> getFirstPlanOps() {
		return getPlans().getFirst().getOps();
	}
}