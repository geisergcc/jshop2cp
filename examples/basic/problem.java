package basic;
import java.util.LinkedList;
import com.gamalocus.jshop2rt.*;

public class problem
{
	public static final String sourcePath = "/Users/gcc/workspace/jshop2cp/examples/basic/problem";
	public static final long sourceLastModified = 1328619440000L;

	public static final Domain owner = new basic();

	private static String[] defineConstants(Domain owner)
	{
		String[] problemConstants = new String[2];

		owner.addConstant("kiwi");
		owner.addConstant("banjo");

		return problemConstants;
	}

	private static void createState0(Domain owner, State s)	{
		s.add(new Predicate(0, 0, new TermList(owner.getTermConstant("kiwi") /*1*/, TermList.NIL)));
	}

	public static LinkedList<Plan> getPlans()
	{
		LinkedList<Plan> returnedPlans = new LinkedList<Plan>();
		defineConstants(owner);

		State s0 = new State(owner.getAxioms());

		createState0(owner, s0); // jShop2Planner.initialize(d, s0);

		TaskList tl;
			tl = new TaskList(1, true);
			tl.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 0, new TermList(owner.getTermConstant("banjo") /*2*/, new TermList(owner.getTermConstant("kiwi") /*1*/, TermList.NIL))), false, false));

		JSHOP2 jShop2Planner = new JSHOP2(tl, 100000, new DoubleCost(0.0), owner, s0);
		JSHOP2Provider.setJSHOP2(jShop2Planner);
		while (jShop2Planner.run()) { 
		}
		returnedPlans.addAll(jShop2Planner.getPlans());
		return returnedPlans;
	}

	public static LinkedList<Predicate> getFirstPlanOps() {
		return getPlans().getFirst().getOps();
	}
}