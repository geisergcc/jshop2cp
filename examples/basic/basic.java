package basic;
import com.gamalocus.jshop2rt.*;

public class basic extends Domain
{
	private static final long serialVersionUID = 7382497424239996054L;


	/**
	 * Operator #-1 for primitive task !pickup
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator0 extends Operator
{
	/**
	 * Operator #-1 for primitive task !pickup
	 */
		public Operator0(Domain owner)
		{
			super(owner, new Predicate(0, 1, new TermList(owner.getTermVariable(0), TermList.NIL)), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[0];

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[1];
			addIn[0] = new DelAddAtomic(new Predicate(0, 1, new TermList(owner.getTermVariable(0), TermList.NIL)));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !pickup [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(1)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Operator #-1 for primitive task !drop
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator1 extends Operator
{
	/**
	 * Operator #-1 for primitive task !drop
	 */
		public Operator1(Domain owner)
		{
			super(owner, new Predicate(1, 1, new TermList(owner.getTermVariable(0), TermList.NIL)), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[1];
			delIn[0] = new DelAddAtomic(new Predicate(0, 1, new TermList(owner.getTermVariable(0), TermList.NIL)));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[0];

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !drop [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionAtomic(new Predicate(0, 1, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task swap
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition0 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition0(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(0, 2, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[2] = new PreconditionNegation(new PreconditionAtomic(new Predicate(0, 2, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier), 2);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task swap [unknown source pos]";
		}
	}

	/**
	 * Precondition #1 of Method -1 for compound task swap
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition1 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition1(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(0, 2, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[2] = new PreconditionNegation(new PreconditionAtomic(new Predicate(0, 2, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier), 2);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #1 of Method -1 for compound task swap [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task swap
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method0 extends Method
	{
	/**
	 * Method -1 for compound task swap
	 */
		public Method0(Domain owner)
		{
			super(owner, new Predicate(0, 2, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			TaskList[] subsIn = new TaskList[2];

			subsIn[0] = createTaskList0();
			subsIn[1] = createTaskList1();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(2, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(1, 2, new TermList(owner.getTermVariable(0), TermList.NIL)), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(0, 2, new TermList(owner.getTermVariable(1), TermList.NIL)), false, true));

			return retVal;
		}

		TaskList createTaskList1()
		{
			TaskList retVal;

			retVal = new TaskList(2, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(1, 2, new TermList(owner.getTermVariable(1), TermList.NIL)), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(0, 2, new TermList(owner.getTermVariable(0), TermList.NIL)), false, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task swap [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition0(owner, unifier)).setComparator(null);
				break;
				case 1:
					p = (new Precondition1(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
				case 1:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method0Branch0";
				case 1: return "Method0Branch1";
				default: return null;
			}
		}
	}

	public static final String sourcePath = "/Users/gcc/workspace/jshop2cp/examples/basic/basic";
	public static final long sourceLastModified = 1328619440000L;

	public basic()
	{
		constants = new String[1];
		constants[0] = "have";

		compoundTasks = new String[1];
		compoundTasks[0] = "swap";

		primitiveTasks = new String[2];
		primitiveTasks[0] = "!pickup";
		primitiveTasks[1] = "!drop";

		initializeTermVariables(2);

		initializeTermConstants();

		methods = new Method[1][];

		methods[0] = new Method[1];
		methods[0][0] = new Method0(this);


		ops = new Operator[2][];

		ops[0] = new Operator[1];
		ops[0][0] = new Operator0(this);

		ops[1] = new Operator[1];
		ops[1][0] = new Operator1(this);

		axioms = new Axiom[1][];

		axioms[0] = new Axiom[0];

	}
}