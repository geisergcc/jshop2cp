package blocks;
import com.gamalocus.jshop2rt.*;

public class blocks extends Domain
{
	private static final long serialVersionUID = 4451009465854307251L;


	/**
	 * Operator #-1 for primitive task !pickup
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator0 extends Operator
{
	/**
	 * Operator #-1 for primitive task !pickup
	 */
		public Operator0(Domain owner)
		{
			super(owner, new Predicate(0, 1, new TermList(owner.getTermVariable(0), TermList.NIL)), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[2];
			delIn[0] = new DelAddAtomic(new Predicate(0, 1, new TermList(owner.getTermVariable(0), TermList.NIL)));
			delIn[1] = new DelAddAtomic(new Predicate(1, 1, new TermList(owner.getTermVariable(0), TermList.NIL)));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[1];
			addIn[0] = new DelAddAtomic(new Predicate(2, 1, new TermList(owner.getTermVariable(0), TermList.NIL)));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !pickup [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(1)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Operator #-1 for primitive task !putdown
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator1 extends Operator
{
	/**
	 * Operator #-1 for primitive task !putdown
	 */
		public Operator1(Domain owner)
		{
			super(owner, new Predicate(1, 1, new TermList(owner.getTermVariable(0), TermList.NIL)), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[1];
			delIn[0] = new DelAddAtomic(new Predicate(2, 1, new TermList(owner.getTermVariable(0), TermList.NIL)));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[2];
			addIn[0] = new DelAddAtomic(new Predicate(1, 1, new TermList(owner.getTermVariable(0), TermList.NIL)));
			addIn[1] = new DelAddAtomic(new Predicate(0, 1, new TermList(owner.getTermVariable(0), TermList.NIL)));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !putdown [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(1)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Operator #-1 for primitive task !stack
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator2 extends Operator
{
	/**
	 * Operator #-1 for primitive task !stack
	 */
		public Operator2(Domain owner)
		{
			super(owner, new Predicate(2, 2, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[2];
			delIn[0] = new DelAddAtomic(new Predicate(2, 2, new TermList(owner.getTermVariable(0), TermList.NIL)));
			delIn[1] = new DelAddAtomic(new Predicate(0, 2, new TermList(owner.getTermVariable(1), TermList.NIL)));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[2];
			addIn[0] = new DelAddAtomic(new Predicate(3, 2, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			addIn[1] = new DelAddAtomic(new Predicate(0, 2, new TermList(owner.getTermVariable(0), TermList.NIL)));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !stack [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(2)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Operator #-1 for primitive task !unstack
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator3 extends Operator
{
	/**
	 * Operator #-1 for primitive task !unstack
	 */
		public Operator3(Domain owner)
		{
			super(owner, new Predicate(3, 2, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), -1, -1, new TermNumber(1.0));


			DelAddElement[] delIn = new DelAddElement[2];
			delIn[0] = new DelAddAtomic(new Predicate(0, 2, new TermList(owner.getTermVariable(0), TermList.NIL)));
			delIn[1] = new DelAddAtomic(new Predicate(3, 2, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[2];
			addIn[0] = new DelAddAtomic(new Predicate(2, 2, new TermList(owner.getTermVariable(0), TermList.NIL)));
			addIn[1] = new DelAddAtomic(new Predicate(0, 2, new TermList(owner.getTermVariable(1), TermList.NIL)));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !unstack [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(2)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Operator #-1 for primitive task !!assert
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator4 extends Operator
{
	/**
	 * Operator #-1 for primitive task !!assert
	 */
		public Operator4(Domain owner)
		{
			super(owner, new Predicate(4, 1, new TermList(owner.getTermVariable(0), TermList.NIL)), -1, -1, new TermNumber(0.0));


			DelAddElement[] delIn = new DelAddElement[0];

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[1];
			addIn[0] = new DelAddAtomic(new Predicate(0, 1));

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !!assert [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(1)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Operator #-1 for primitive task !!remove
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalOperator)
	 */
	public static class Operator5 extends Operator
{
	/**
	 * Operator #-1 for primitive task !!remove
	 */
		public Operator5(Domain owner)
		{
			super(owner, new Predicate(5, 1, new TermList(owner.getTermVariable(0), TermList.NIL)), -1, -1, new TermNumber(0.0));


			DelAddElement[] delIn = new DelAddElement[1];
			delIn[0] = new DelAddAtomic(new Predicate(0, 1));

			setDel(delIn);

			DelAddElement[] addIn = new DelAddElement[0];

			setAdd(addIn);
		}

		@Override
		public String toString()
		{
			return "Operator #-1 for primitive task !!remove [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			p = (new PreconditionNil(1)).setComparator(null);
			p.reset(state);

			return p;
		}
	}

	/**
	 * Method -1 for compound task achieve-goals
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method0 extends Method
	{
	/**
	 * Method -1 for compound task achieve-goals
	 */
		public Method0(Domain owner)
		{
			super(owner, new Predicate(5, 1, new TermList(owner.getTermVariable(0), TermList.NIL)));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(5, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 1, new TermList(owner.getTermVariable(0), TermList.NIL)), false, false));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(1, 1, TermList.NIL), false, false));
			retVal.subtasks[2] = new TaskList(new TaskAtom(new Predicate(2, 1, TermList.NIL), false, false));
			retVal.subtasks[3] = new TaskList(new TaskAtom(new Predicate(3, 1, TermList.NIL), false, false));
			retVal.subtasks[4] = new TaskList(new TaskAtom(new Predicate(4, 1, TermList.NIL), false, false));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task achieve-goals [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionNil(1)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method0Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Method -1 for compound task assert-goals
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method1 extends Method
	{
	/**
	 * Method -1 for compound task assert-goals
	 */
		public Method1(Domain owner)
		{
			super(owner, new Predicate(0, 2, new TermList(new TermList(owner.getTermVariable(0), owner.getTermVariable(1)), TermList.NIL)));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = createTaskList0();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(2, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(4, 2, new TermList(new TermList(owner.getTermConstant(4) /*goal*/, new TermList(owner.getTermVariable(0), TermList.NIL)), TermList.NIL)), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(0, 2, new TermList(owner.getTermVariable(1), TermList.NIL)), false, false));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task assert-goals [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionNil(2)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method1Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Method -1 for compound task assert-goals
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method2 extends Method
	{
	/**
	 * Method -1 for compound task assert-goals
	 */
		public Method2(Domain owner)
		{
			super(owner, new Predicate(0, 0, new TermList(TermList.NIL, TermList.NIL)));
			TaskList[] subsIn = new TaskList[1];

			subsIn[0] = TaskList.empty;

			setSubs(subsIn);
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task assert-goals [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionNil(0)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method2Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task find-nomove
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition0 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition0(Domain owner, Term[] unifier)
		{
			p = new Precondition[4];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(5, 1, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[2] = new PreconditionNegation(new PreconditionAtomic(new Predicate(6, 1, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier), 1);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[3] = new PreconditionNegation(new PreconditionAtomic(new Predicate(7, 1, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier), 1);
			b = new Term[4][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[3] == null)
			{
				boolean b2changed = false;
				while (b[2] == null)
				{
					boolean b1changed = false;
					while (b[1] == null)
					{
						b[1] = p[1].nextBinding(state);
						if (b[1] == null)
							return null;
						else
							bestMatch = Math.max(bestMatch, 1);
						b1changed = true;
					}
					if ( b1changed ) {
						p[2].reset(state);
						p[2].bind(Term.merge(b, 2));
					}
					b[2] = p[2].nextBinding(state);
					if (b[2] == null)
						b[1] = null;
					else
						bestMatch = Math.max(bestMatch, 2);
					b2changed = true;
				}
				if ( b2changed ) {
					p[3].reset(state);
					p[3].bind(Term.merge(b, 3));
				}
				b[3] = p[3].nextBinding(state);
				if (b[3] == null)
					b[2] = null;
				else
					bestMatch = Math.max(bestMatch, 3);
			}

			Term[] retVal = Term.merge(b, 4);
			b[3] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task find-nomove [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task find-nomove
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method3 extends Method
	{
	/**
	 * Method -1 for compound task find-nomove
	 */
		public Method3(Domain owner)
		{
			super(owner, new Predicate(1, 1, TermList.NIL));
			TaskList[] subsIn = new TaskList[2];

			subsIn[0] = createTaskList0();
			subsIn[1] = TaskList.empty;

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(2, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(4, 1, new TermList(new TermList(owner.getTermConstant(6) /*dont-move*/, new TermList(owner.getTermVariable(0), TermList.NIL)), TermList.NIL)), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(1, 1, TermList.NIL), false, false));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task find-nomove [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition0(owner, unifier)).setComparator(null);
				break;
				case 1:
					p = (new PreconditionNil(1)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
				case 1:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method3Branch0";
				case 1: return "Method3Branch1";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task add-new-goals
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition1 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition1(Domain owner, Term[] unifier)
		{
			p = new Precondition[5];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(5, 2, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[2] = new PreconditionNegation(new PreconditionAtomic(new Predicate(6, 2, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier), 2);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[3] = new PreconditionNegation(new PreconditionAtomic(new Predicate(4, 2, new TermList(new TermList(owner.getTermConstant(1) /*on-table*/, new TermList(owner.getTermVariable(0), TermList.NIL)), TermList.NIL)), unifier), 2);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[4] = new PreconditionNegation(new PreconditionAtomic(new Predicate(4, 2, new TermList(new TermList(owner.getTermConstant(3) /*on*/, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), TermList.NIL)), unifier), 2);
			b = new Term[5][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[4] == null)
			{
				boolean b3changed = false;
				while (b[3] == null)
				{
					boolean b2changed = false;
					while (b[2] == null)
					{
						boolean b1changed = false;
						while (b[1] == null)
						{
							b[1] = p[1].nextBinding(state);
							if (b[1] == null)
								return null;
							else
								bestMatch = Math.max(bestMatch, 1);
							b1changed = true;
						}
						if ( b1changed ) {
							p[2].reset(state);
							p[2].bind(Term.merge(b, 2));
						}
						b[2] = p[2].nextBinding(state);
						if (b[2] == null)
							b[1] = null;
						else
							bestMatch = Math.max(bestMatch, 2);
						b2changed = true;
					}
					if ( b2changed ) {
						p[3].reset(state);
						p[3].bind(Term.merge(b, 3));
					}
					b[3] = p[3].nextBinding(state);
					if (b[3] == null)
						b[2] = null;
					else
						bestMatch = Math.max(bestMatch, 3);
					b3changed = true;
				}
				if ( b3changed ) {
					p[4].reset(state);
					p[4].bind(Term.merge(b, 4));
				}
				b[4] = p[4].nextBinding(state);
				if (b[4] == null)
					b[3] = null;
				else
					bestMatch = Math.max(bestMatch, 4);
			}

			Term[] retVal = Term.merge(b, 5);
			b[4] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			p[4].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task add-new-goals [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task add-new-goals
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method4 extends Method
	{
	/**
	 * Method -1 for compound task add-new-goals
	 */
		public Method4(Domain owner)
		{
			super(owner, new Predicate(2, 2, TermList.NIL));
			TaskList[] subsIn = new TaskList[2];

			subsIn[0] = createTaskList0();
			subsIn[1] = TaskList.empty;

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(2, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(4, 2, new TermList(new TermList(owner.getTermConstant(4) /*goal*/, new TermList(new TermList(owner.getTermConstant(1) /*on-table*/, new TermList(owner.getTermVariable(0), TermList.NIL)), TermList.NIL)), TermList.NIL)), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(2, 2, TermList.NIL), false, false));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task add-new-goals [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition1(owner, unifier)).setComparator(null);
				break;
				case 1:
					p = (new PreconditionNil(2)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
				case 1:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method4Branch0";
				case 1: return "Method4Branch1";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task find-movable
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition2 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition2(Domain owner, Term[] unifier)
		{
			p = new Precondition[5];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(0, 2, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[2] = new PreconditionNegation(new PreconditionAtomic(new Predicate(6, 2, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier), 2);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(4, 2, new TermList(new TermList(owner.getTermConstant(1) /*on-table*/, new TermList(owner.getTermVariable(0), TermList.NIL)), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[4] = new PreconditionNegation(new PreconditionAtomic(new Predicate(8, 2, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier), 2);
			b = new Term[5][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[4] == null)
			{
				boolean b3changed = false;
				while (b[3] == null)
				{
					boolean b2changed = false;
					while (b[2] == null)
					{
						boolean b1changed = false;
						while (b[1] == null)
						{
							b[1] = p[1].nextBinding(state);
							if (b[1] == null)
								return null;
							else
								bestMatch = Math.max(bestMatch, 1);
							b1changed = true;
						}
						if ( b1changed ) {
							p[2].reset(state);
							p[2].bind(Term.merge(b, 2));
						}
						b[2] = p[2].nextBinding(state);
						if (b[2] == null)
							b[1] = null;
						else
							bestMatch = Math.max(bestMatch, 2);
						b2changed = true;
					}
					if ( b2changed ) {
						p[3].reset(state);
						p[3].bind(Term.merge(b, 3));
					}
					b[3] = p[3].nextBinding(state);
					if (b[3] == null)
						b[2] = null;
					else
						bestMatch = Math.max(bestMatch, 3);
					b3changed = true;
				}
				if ( b3changed ) {
					p[4].reset(state);
					p[4].bind(Term.merge(b, 4));
				}
				b[4] = p[4].nextBinding(state);
				if (b[4] == null)
					b[3] = null;
				else
					bestMatch = Math.max(bestMatch, 4);
			}

			Term[] retVal = Term.merge(b, 5);
			b[4] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			p[4].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task find-movable [unknown source pos]";
		}
	}

	/**
	 * Precondition #1 of Method -1 for compound task find-movable
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition3 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition3(Domain owner, Term[] unifier)
		{
			p = new Precondition[7];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(0, 2, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[2] = new PreconditionNegation(new PreconditionAtomic(new Predicate(6, 2, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier), 2);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(4, 2, new TermList(new TermList(owner.getTermConstant(3) /*on*/, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[4] = new PreconditionNegation(new PreconditionAtomic(new Predicate(9, 2, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier), 2);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[5] = new PreconditionAtomic(new Predicate(6, 2, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[6] = new PreconditionAtomic(new Predicate(0, 2, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			b = new Term[7][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
			b[6] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[6] == null)
			{
				boolean b5changed = false;
				while (b[5] == null)
				{
					boolean b4changed = false;
					while (b[4] == null)
					{
						boolean b3changed = false;
						while (b[3] == null)
						{
							boolean b2changed = false;
							while (b[2] == null)
							{
								boolean b1changed = false;
								while (b[1] == null)
								{
									b[1] = p[1].nextBinding(state);
									if (b[1] == null)
										return null;
									else
										bestMatch = Math.max(bestMatch, 1);
									b1changed = true;
								}
								if ( b1changed ) {
									p[2].reset(state);
									p[2].bind(Term.merge(b, 2));
								}
								b[2] = p[2].nextBinding(state);
								if (b[2] == null)
									b[1] = null;
								else
									bestMatch = Math.max(bestMatch, 2);
								b2changed = true;
							}
							if ( b2changed ) {
								p[3].reset(state);
								p[3].bind(Term.merge(b, 3));
							}
							b[3] = p[3].nextBinding(state);
							if (b[3] == null)
								b[2] = null;
							else
								bestMatch = Math.max(bestMatch, 3);
							b3changed = true;
						}
						if ( b3changed ) {
							p[4].reset(state);
							p[4].bind(Term.merge(b, 4));
						}
						b[4] = p[4].nextBinding(state);
						if (b[4] == null)
							b[3] = null;
						else
							bestMatch = Math.max(bestMatch, 4);
						b4changed = true;
					}
					if ( b4changed ) {
						p[5].reset(state);
						p[5].bind(Term.merge(b, 5));
					}
					b[5] = p[5].nextBinding(state);
					if (b[5] == null)
						b[4] = null;
					else
						bestMatch = Math.max(bestMatch, 5);
					b5changed = true;
				}
				if ( b5changed ) {
					p[6].reset(state);
					p[6].bind(Term.merge(b, 6));
				}
				b[6] = p[6].nextBinding(state);
				if (b[6] == null)
					b[5] = null;
				else
					bestMatch = Math.max(bestMatch, 6);
			}

			Term[] retVal = Term.merge(b, 7);
			b[6] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			p[4].reset(state);
			p[5].reset(state);
			p[6].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
			b[4] = null;
			b[5] = null;
			b[6] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #1 of Method -1 for compound task find-movable [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task find-movable
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method5 extends Method
	{
	/**
	 * Method -1 for compound task find-movable
	 */
		public Method5(Domain owner)
		{
			super(owner, new Predicate(3, 2, TermList.NIL));
			TaskList[] subsIn = new TaskList[3];

			subsIn[0] = createTaskList0();
			subsIn[1] = createTaskList1();
			subsIn[2] = TaskList.empty;

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(2, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(4, 2, new TermList(new TermList(owner.getTermConstant(8) /*put-on-table*/, new TermList(owner.getTermVariable(0), TermList.NIL)), TermList.NIL)), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(3, 2, TermList.NIL), false, false));

			return retVal;
		}

		TaskList createTaskList1()
		{
			TaskList retVal;

			retVal = new TaskList(2, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(4, 2, new TermList(new TermList(owner.getTermConstant(9) /*stack-on-block*/, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), TermList.NIL)), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(3, 2, TermList.NIL), false, false));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task find-movable [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition2(owner, unifier)).setComparator(null);
				break;
				case 1:
					p = (new Precondition3(owner, unifier)).setComparator(null);
				break;
				case 2:
					p = (new PreconditionNil(2)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
				case 1:
				break;
				case 2:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method5Branch0";
				case 1: return "Method5Branch1";
				case 2: return "Method5Branch2";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task check
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition4 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition4(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(4, 2, new TermList(new TermList(owner.getTermConstant(3) /*on*/, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(0), TermList.NIL))), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(0, 2, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task check [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task check
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method6 extends Method
	{
	/**
	 * Method -1 for compound task check
	 */
		public Method6(Domain owner)
		{
			super(owner, new Predicate(6, 2, new TermList(owner.getTermVariable(0), TermList.NIL)));
			TaskList[] subsIn = new TaskList[2];

			subsIn[0] = createTaskList0();
			subsIn[1] = TaskList.empty;

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(4, 2, new TermList(new TermList(owner.getTermConstant(9) /*stack-on-block*/, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(0), TermList.NIL))), TermList.NIL)), false, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task check [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition4(owner, unifier)).setComparator(null);
				break;
				case 1:
					p = (new PreconditionNil(2)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
				case 1:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method6Branch0";
				case 1: return "Method6Branch1";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #0 of Method -1 for compound task check2
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition5 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition5(Domain owner, Term[] unifier)
		{
			p = new Precondition[4];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(6, 2, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(4, 2, new TermList(new TermList(owner.getTermConstant(3) /*on*/, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(0), TermList.NIL))), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(0, 2, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			b = new Term[4][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[3] == null)
			{
				boolean b2changed = false;
				while (b[2] == null)
				{
					boolean b1changed = false;
					while (b[1] == null)
					{
						b[1] = p[1].nextBinding(state);
						if (b[1] == null)
							return null;
						else
							bestMatch = Math.max(bestMatch, 1);
						b1changed = true;
					}
					if ( b1changed ) {
						p[2].reset(state);
						p[2].bind(Term.merge(b, 2));
					}
					b[2] = p[2].nextBinding(state);
					if (b[2] == null)
						b[1] = null;
					else
						bestMatch = Math.max(bestMatch, 2);
					b2changed = true;
				}
				if ( b2changed ) {
					p[3].reset(state);
					p[3].bind(Term.merge(b, 3));
				}
				b[3] = p[3].nextBinding(state);
				if (b[3] == null)
					b[2] = null;
				else
					bestMatch = Math.max(bestMatch, 3);
			}

			Term[] retVal = Term.merge(b, 4);
			b[3] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #0 of Method -1 for compound task check2 [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task check2
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method7 extends Method
	{
	/**
	 * Method -1 for compound task check2
	 */
		public Method7(Domain owner)
		{
			super(owner, new Predicate(7, 2, new TermList(owner.getTermVariable(0), TermList.NIL)));
			TaskList[] subsIn = new TaskList[2];

			subsIn[0] = createTaskList0();
			subsIn[1] = TaskList.empty;

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(4, 2, new TermList(new TermList(owner.getTermConstant(9) /*stack-on-block*/, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(0), TermList.NIL))), TermList.NIL)), false, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task check2 [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition5(owner, unifier)).setComparator(null);
				break;
				case 1:
					p = (new PreconditionNil(2)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
				case 1:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method7Branch0";
				case 1: return "Method7Branch1";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #1 of Method -1 for compound task check3
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition6 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition6(Domain owner, Term[] unifier)
		{
			p = new Precondition[4];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(4, 2, new TermList(new TermList(owner.getTermConstant(3) /*on*/, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(0, 2, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(6, 2, new TermList(owner.getTermVariable(1), TermList.NIL)), unifier);
			b = new Term[4][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[3] == null)
			{
				boolean b2changed = false;
				while (b[2] == null)
				{
					boolean b1changed = false;
					while (b[1] == null)
					{
						b[1] = p[1].nextBinding(state);
						if (b[1] == null)
							return null;
						else
							bestMatch = Math.max(bestMatch, 1);
						b1changed = true;
					}
					if ( b1changed ) {
						p[2].reset(state);
						p[2].bind(Term.merge(b, 2));
					}
					b[2] = p[2].nextBinding(state);
					if (b[2] == null)
						b[1] = null;
					else
						bestMatch = Math.max(bestMatch, 2);
					b2changed = true;
				}
				if ( b2changed ) {
					p[3].reset(state);
					p[3].bind(Term.merge(b, 3));
				}
				b[3] = p[3].nextBinding(state);
				if (b[3] == null)
					b[2] = null;
				else
					bestMatch = Math.max(bestMatch, 3);
			}

			Term[] retVal = Term.merge(b, 4);
			b[3] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #1 of Method -1 for compound task check3 [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task check3
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method8 extends Method
	{
	/**
	 * Method -1 for compound task check3
	 */
		public Method8(Domain owner)
		{
			super(owner, new Predicate(8, 2, new TermList(owner.getTermVariable(0), TermList.NIL)));
			TaskList[] subsIn = new TaskList[4];

			subsIn[0] = TaskList.empty;
			subsIn[1] = createTaskList1();
			subsIn[2] = createTaskList2();
			subsIn[3] = TaskList.empty;

			setSubs(subsIn);
		}

		TaskList createTaskList1()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(4, 2, new TermList(new TermList(owner.getTermConstant(9) /*stack-on-block*/, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), TermList.NIL)), false, true));

			return retVal;
		}

		TaskList createTaskList2()
		{
			TaskList retVal;

			retVal = new TaskList(1, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(4, 2, new TermList(new TermList(owner.getTermConstant(8) /*put-on-table*/, new TermList(owner.getTermVariable(0), TermList.NIL)), TermList.NIL)), false, true));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task check3 [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionAtomic(new Predicate(6, 2, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier)).setComparator(null);
				break;
				case 1:
					p = (new Precondition6(owner, unifier)).setComparator(null);
				break;
				case 2:
					p = (new PreconditionAtomic(new Predicate(4, 2, new TermList(new TermList(owner.getTermConstant(1) /*on-table*/, new TermList(owner.getTermVariable(0), TermList.NIL)), TermList.NIL)), unifier)).setComparator(null);
				break;
				case 3:
					p = (new PreconditionNil(2)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
				case 1:
				break;
				case 2:
				break;
				case 3:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method8Branch0";
				case 1: return "Method8Branch1";
				case 2: return "Method8Branch2";
				case 3: return "Method8Branch3";
				default: return null;
			}
		}
	}

	/**
	 * Method -1 for compound task move-block1
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method9 extends Method
	{
	/**
	 * Method -1 for compound task move-block1
	 */
		public Method9(Domain owner)
		{
			super(owner, new Predicate(9, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))));
			TaskList[] subsIn = new TaskList[2];

			subsIn[0] = createTaskList0();
			subsIn[1] = createTaskList1();

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(7, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(3, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(2, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), false, true));
			retVal.subtasks[2] = new TaskList(new TaskAtom(new Predicate(4, 3, new TermList(new TermList(owner.getTermConstant(6) /*dont-move*/, new TermList(owner.getTermVariable(0), TermList.NIL)), TermList.NIL)), false, true));
			retVal.subtasks[3] = new TaskList(new TaskAtom(new Predicate(5, 3, new TermList(new TermList(owner.getTermConstant(9) /*stack-on-block*/, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), TermList.NIL)), false, true));
			retVal.subtasks[4] = new TaskList(new TaskAtom(new Predicate(6, 3, new TermList(owner.getTermVariable(0), TermList.NIL)), false, false));
			retVal.subtasks[5] = new TaskList(new TaskAtom(new Predicate(7, 3, new TermList(owner.getTermVariable(2), TermList.NIL)), false, false));
			retVal.subtasks[6] = new TaskList(new TaskAtom(new Predicate(8, 3, new TermList(owner.getTermVariable(2), TermList.NIL)), false, false));

			return retVal;
		}

		TaskList createTaskList1()
		{
			TaskList retVal;

			retVal = new TaskList(5, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(0, 3, new TermList(owner.getTermVariable(0), TermList.NIL)), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(2, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), false, true));
			retVal.subtasks[2] = new TaskList(new TaskAtom(new Predicate(4, 3, new TermList(new TermList(owner.getTermConstant(6) /*dont-move*/, new TermList(owner.getTermVariable(0), TermList.NIL)), TermList.NIL)), false, true));
			retVal.subtasks[3] = new TaskList(new TaskAtom(new Predicate(5, 3, new TermList(new TermList(owner.getTermConstant(9) /*stack-on-block*/, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), TermList.NIL)), false, true));
			retVal.subtasks[4] = new TaskList(new TaskAtom(new Predicate(6, 3, new TermList(owner.getTermVariable(0), TermList.NIL)), false, false));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task move-block1 [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionAtomic(new Predicate(3, 3, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier)).setComparator(null);
				break;
				case 1:
					p = (new PreconditionNil(3)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
				case 1:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "method-for-moving-x-from-y-to-z";
				case 1: return "method-for-moving-x-from-table-to-z";
				default: return null;
			}
		}
	}

	/**
	 * Precondition #1 of Method -1 for compound task move-block
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition7 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition7(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(8, 2, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(3, 2, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #1 of Method -1 for compound task move-block [unknown source pos]";
		}
	}

	/**
	 * Precondition #2 of Method -1 for compound task move-block
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition8 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition8(Domain owner, Term[] unifier)
		{
			p = new Precondition[4];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(0, 2, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[2] = new PreconditionNegation(new PreconditionAtomic(new Predicate(6, 2, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier), 2);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[3] = new PreconditionAtomic(new Predicate(3, 2, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier);
			b = new Term[4][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[3] == null)
			{
				boolean b2changed = false;
				while (b[2] == null)
				{
					boolean b1changed = false;
					while (b[1] == null)
					{
						b[1] = p[1].nextBinding(state);
						if (b[1] == null)
							return null;
						else
							bestMatch = Math.max(bestMatch, 1);
						b1changed = true;
					}
					if ( b1changed ) {
						p[2].reset(state);
						p[2].bind(Term.merge(b, 2));
					}
					b[2] = p[2].nextBinding(state);
					if (b[2] == null)
						b[1] = null;
					else
						bestMatch = Math.max(bestMatch, 2);
					b2changed = true;
				}
				if ( b2changed ) {
					p[3].reset(state);
					p[3].bind(Term.merge(b, 3));
				}
				b[3] = p[3].nextBinding(state);
				if (b[3] == null)
					b[2] = null;
				else
					bestMatch = Math.max(bestMatch, 3);
			}

			Term[] retVal = Term.merge(b, 4);
			b[3] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition #2 of Method -1 for compound task move-block [unknown source pos]";
		}
	}

	/**
	 * Method -1 for compound task move-block
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalMethod)
	 */
	public static class Method10 extends Method
	{
	/**
	 * Method -1 for compound task move-block
	 */
		public Method10(Domain owner)
		{
			super(owner, new Predicate(4, 2, TermList.NIL));
			TaskList[] subsIn = new TaskList[4];

			subsIn[0] = createTaskList0();
			subsIn[1] = createTaskList1();
			subsIn[2] = createTaskList2();
			subsIn[3] = TaskList.empty;

			setSubs(subsIn);
		}

		TaskList createTaskList0()
		{
			TaskList retVal;

			retVal = new TaskList(2, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(9, 2, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), false, false));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(4, 2, TermList.NIL), false, false));

			return retVal;
		}

		TaskList createTaskList1()
		{
			TaskList retVal;

			retVal = new TaskList(8, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(3, 2, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(1, 2, new TermList(owner.getTermVariable(0), TermList.NIL)), false, true));
			retVal.subtasks[2] = new TaskList(new TaskAtom(new Predicate(4, 2, new TermList(new TermList(owner.getTermConstant(6) /*dont-move*/, new TermList(owner.getTermVariable(0), TermList.NIL)), TermList.NIL)), false, true));
			retVal.subtasks[3] = new TaskList(new TaskAtom(new Predicate(5, 2, new TermList(new TermList(owner.getTermConstant(8) /*put-on-table*/, new TermList(owner.getTermVariable(0), TermList.NIL)), TermList.NIL)), false, true));
			retVal.subtasks[4] = new TaskList(new TaskAtom(new Predicate(6, 2, new TermList(owner.getTermVariable(0), TermList.NIL)), false, false));
			retVal.subtasks[5] = new TaskList(new TaskAtom(new Predicate(7, 2, new TermList(owner.getTermVariable(1), TermList.NIL)), false, false));
			retVal.subtasks[6] = new TaskList(new TaskAtom(new Predicate(8, 2, new TermList(owner.getTermVariable(1), TermList.NIL)), false, false));
			retVal.subtasks[7] = new TaskList(new TaskAtom(new Predicate(4, 2, TermList.NIL), false, false));

			return retVal;
		}

		TaskList createTaskList2()
		{
			TaskList retVal;

			retVal = new TaskList(5, true);
			retVal.subtasks[0] = new TaskList(new TaskAtom(new Predicate(3, 2, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), false, true));
			retVal.subtasks[1] = new TaskList(new TaskAtom(new Predicate(1, 2, new TermList(owner.getTermVariable(0), TermList.NIL)), false, true));
			retVal.subtasks[2] = new TaskList(new TaskAtom(new Predicate(7, 2, new TermList(owner.getTermVariable(1), TermList.NIL)), false, false));
			retVal.subtasks[3] = new TaskList(new TaskAtom(new Predicate(8, 2, new TermList(owner.getTermVariable(1), TermList.NIL)), false, false));
			retVal.subtasks[4] = new TaskList(new TaskAtom(new Predicate(4, 2, TermList.NIL), false, false));

			return retVal;
		}

		@Override
		public String toString()
		{
			return "Method -1 for compound task move-block [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionAtomic(new Predicate(9, 2, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier)).setComparator(null);
				break;
				case 1:
					p = (new Precondition7(owner, unifier)).setComparator(null);
				break;
				case 2:
					p = (new Precondition8(owner, unifier)).setComparator(null);
				break;
				case 3:
					p = (new PreconditionNil(2)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public int getApplicability(State state, Term[] unifier, int which) {
			int toReturn = 0;
			Precondition p;
			switch(which) {
				case 0:
				break;
				case 1:
				break;
				case 2:
				break;
				case 3:
				break;
			}
			return toReturn;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Method10Branch0";
				case 1: return "method-for-moving-x-from-y-to-table";
				case 2: return "method-for-moving-x-out-of-the-way";
				case 3: return "termination-method-branch";
				default: return null;
			}
		}
	}

	/**
	 * Branch -1 for axiom same
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalAxiom)
	 */
	public static class Axiom0 extends Axiom
{
	/**
	 * Branch -1 for axiom same
	 */
		public Axiom0(Domain owner)
		{
			super(owner, new Predicate(10, 1, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(0), TermList.NIL))), 1);
		}

		@Override
		public String toString()
		{
			return "Branch -1 for axiom same [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new PreconditionNil(1)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Axiom0Branch0";
				default: return null;
			}
		}
	}

	/**
	 * Precondition of branch #0 of Branch -1 for axiom need-to-move
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition9 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition9(Domain owner, Term[] unifier)
		{
			p = new Precondition[4];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(3, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(4, 4, new TermList(new TermList(owner.getTermConstant(3) /*on*/, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[3] = new PreconditionNegation(new PreconditionAtomic(new Predicate(10, 4, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier), 4);
			b = new Term[4][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[3] == null)
			{
				boolean b2changed = false;
				while (b[2] == null)
				{
					boolean b1changed = false;
					while (b[1] == null)
					{
						b[1] = p[1].nextBinding(state);
						if (b[1] == null)
							return null;
						else
							bestMatch = Math.max(bestMatch, 1);
						b1changed = true;
					}
					if ( b1changed ) {
						p[2].reset(state);
						p[2].bind(Term.merge(b, 2));
					}
					b[2] = p[2].nextBinding(state);
					if (b[2] == null)
						b[1] = null;
					else
						bestMatch = Math.max(bestMatch, 2);
					b2changed = true;
				}
				if ( b2changed ) {
					p[3].reset(state);
					p[3].bind(Term.merge(b, 3));
				}
				b[3] = p[3].nextBinding(state);
				if (b[3] == null)
					b[2] = null;
				else
					bestMatch = Math.max(bestMatch, 3);
			}

			Term[] retVal = Term.merge(b, 4);
			b[3] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of branch #0 of Branch -1 for axiom need-to-move [unknown source pos]";
		}
	}

	/**
	 * Precondition of branch #1 of Branch -1 for axiom need-to-move
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition10 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition10(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(1, 4, new TermList(owner.getTermVariable(0), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(4, 4, new TermList(new TermList(owner.getTermConstant(3) /*on*/, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), TermList.NIL)), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of branch #1 of Branch -1 for axiom need-to-move [unknown source pos]";
		}
	}

	/**
	 * Precondition of branch #2 of Branch -1 for axiom need-to-move
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition11 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition11(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(3, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(4, 4, new TermList(new TermList(owner.getTermConstant(1) /*on-table*/, new TermList(owner.getTermVariable(0), TermList.NIL)), TermList.NIL)), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of branch #2 of Branch -1 for axiom need-to-move [unknown source pos]";
		}
	}

	/**
	 * Precondition of branch #3 of Branch -1 for axiom need-to-move
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition12 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition12(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(3, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(4, 4, new TermList(new TermList(owner.getTermConstant(0) /*clear*/, new TermList(owner.getTermVariable(1), TermList.NIL)), TermList.NIL)), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of branch #3 of Branch -1 for axiom need-to-move [unknown source pos]";
		}
	}

	/**
	 * Precondition of branch #4 of Branch -1 for axiom need-to-move
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition13 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition13(Domain owner, Term[] unifier)
		{
			p = new Precondition[4];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(3, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(2), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(4, 4, new TermList(new TermList(owner.getTermConstant(3) /*on*/, new TermList(owner.getTermVariable(1), new TermList(owner.getTermVariable(2), TermList.NIL))), TermList.NIL)), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionNegation)
			p[3] = new PreconditionNegation(new PreconditionAtomic(new Predicate(10, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(1), TermList.NIL))), unifier), 4);
			b = new Term[4][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[3] == null)
			{
				boolean b2changed = false;
				while (b[2] == null)
				{
					boolean b1changed = false;
					while (b[1] == null)
					{
						b[1] = p[1].nextBinding(state);
						if (b[1] == null)
							return null;
						else
							bestMatch = Math.max(bestMatch, 1);
						b1changed = true;
					}
					if ( b1changed ) {
						p[2].reset(state);
						p[2].bind(Term.merge(b, 2));
					}
					b[2] = p[2].nextBinding(state);
					if (b[2] == null)
						b[1] = null;
					else
						bestMatch = Math.max(bestMatch, 2);
					b2changed = true;
				}
				if ( b2changed ) {
					p[3].reset(state);
					p[3].bind(Term.merge(b, 3));
				}
				b[3] = p[3].nextBinding(state);
				if (b[3] == null)
					b[2] = null;
				else
					bestMatch = Math.max(bestMatch, 3);
			}

			Term[] retVal = Term.merge(b, 4);
			b[3] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			p[3].reset(state);
			b[1] = null;
			b[2] = null;
			b[3] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of branch #4 of Branch -1 for axiom need-to-move [unknown source pos]";
		}
	}

	/**
	 * Precondition of branch #5 of Branch -1 for axiom need-to-move
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionConjunction)
	 */
	public static class Precondition14 extends Precondition
	{
		Precondition[] p;
		Term[][] b;

		public Precondition14(Domain owner, Term[] unifier)
		{
			p = new Precondition[3];
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[1] = new PreconditionAtomic(new Predicate(3, 4, new TermList(owner.getTermVariable(0), new TermList(owner.getTermVariable(3), TermList.NIL))), unifier);
			// The source position was not specified. (class: com.gamalocus.jshop2rt.LogicalExpressionAtomic)
			p[2] = new PreconditionAtomic(new Predicate(7, 4, new TermList(owner.getTermVariable(3), TermList.NIL)), unifier);
			b = new Term[3][];
			b[0] = unifier;
			b[0] = Term.merge( b, 1 );

			setFirst(false);
		}

		public void bind(Term[] binding)
		{
			b[0] = binding;
			b[0] = Term.merge( b, 1 );
			p[1].bind(binding);
			b[1] = null;
			b[2] = null;
		}

		protected Term[] nextBindingHelper(State state)
		{
			bestMatch = 0;
			while (b[2] == null)
			{
				boolean b1changed = false;
				while (b[1] == null)
				{
					b[1] = p[1].nextBinding(state);
					if (b[1] == null)
						return null;
					else
						bestMatch = Math.max(bestMatch, 1);
					b1changed = true;
				}
				if ( b1changed ) {
					p[2].reset(state);
					p[2].bind(Term.merge(b, 2));
				}
				b[2] = p[2].nextBinding(state);
				if (b[2] == null)
					b[1] = null;
				else
					bestMatch = Math.max(bestMatch, 2);
			}

			Term[] retVal = Term.merge(b, 3);
			b[2] = null;
			return retVal;
		}

		protected void resetHelper(State state)
		{
			p[1].reset(state);
			p[2].reset(state);
			b[1] = null;
			b[2] = null;
		}
		@Override
		public String toString()
		{
			return "Precondition of branch #5 of Branch -1 for axiom need-to-move [unknown source pos]";
		}
	}

	/**
	 * Branch -1 for axiom need-to-move
	 * The source position was not specified. (class: com.gamalocus.jshop2rt.InternalAxiom)
	 */
	public static class Axiom1 extends Axiom
{
	/**
	 * Branch -1 for axiom need-to-move
	 */
		public Axiom1(Domain owner)
		{
			super(owner, new Predicate(7, 4, new TermList(owner.getTermVariable(0), TermList.NIL)), 6);
		}

		@Override
		public String toString()
		{
			return "Branch -1 for axiom need-to-move [unknown source pos]";
		}
		public Precondition getIterator(State state, Term[] unifier, int which)
		{
			Precondition p;

			switch (which)
			{
				case 0:
					p = (new Precondition9(owner, unifier)).setComparator(null);
				break;
				case 1:
					p = (new Precondition10(owner, unifier)).setComparator(null);
				break;
				case 2:
					p = (new Precondition11(owner, unifier)).setComparator(null);
				break;
				case 3:
					p = (new Precondition12(owner, unifier)).setComparator(null);
				break;
				case 4:
					p = (new Precondition13(owner, unifier)).setComparator(null);
				break;
				case 5:
					p = (new Precondition14(owner, unifier)).setComparator(null);
				break;
				default:
					return null;
			}

			p.reset(state);

			return p;
		}

		public String getLabel(int which)
		{
			switch (which)
			{
				case 0: return "Axiom1Branch0";
				case 1: return "Axiom1Branch1";
				case 2: return "Axiom1Branch2";
				case 3: return "Axiom1Branch3";
				case 4: return "Axiom1Branch4";
				case 5: return "Axiom1Branch5";
				default: return null;
			}
		}
	}

	public static final String sourcePath = "/Users/gcc/workspace/jshop2cp/examples/blocks/blocks";
	public static final long sourceLastModified = 1328619440000L;

	public blocks()
	{
		constants = new String[11];
		constants[0] = "clear";
		constants[1] = "on-table";
		constants[2] = "holding";
		constants[3] = "on";
		constants[4] = "goal";
		constants[5] = "block";
		constants[6] = "dont-move";
		constants[7] = "need-to-move";
		constants[8] = "put-on-table";
		constants[9] = "stack-on-block";
		constants[10] = "same";

		compoundTasks = new String[10];
		compoundTasks[0] = "assert-goals";
		compoundTasks[1] = "find-nomove";
		compoundTasks[2] = "add-new-goals";
		compoundTasks[3] = "find-movable";
		compoundTasks[4] = "move-block";
		compoundTasks[5] = "achieve-goals";
		compoundTasks[6] = "check";
		compoundTasks[7] = "check2";
		compoundTasks[8] = "check3";
		compoundTasks[9] = "move-block1";

		primitiveTasks = new String[6];
		primitiveTasks[0] = "!pickup";
		primitiveTasks[1] = "!putdown";
		primitiveTasks[2] = "!stack";
		primitiveTasks[3] = "!unstack";
		primitiveTasks[4] = "!!assert";
		primitiveTasks[5] = "!!remove";

		initializeTermVariables(4);

		initializeTermConstants();

		methods = new Method[10][];

		methods[0] = new Method[2];
		methods[0][0] = new Method1(this);
		methods[0][1] = new Method2(this);

		methods[1] = new Method[1];
		methods[1][0] = new Method3(this);

		methods[2] = new Method[1];
		methods[2][0] = new Method4(this);

		methods[3] = new Method[1];
		methods[3][0] = new Method5(this);

		methods[4] = new Method[1];
		methods[4][0] = new Method10(this);

		methods[5] = new Method[1];
		methods[5][0] = new Method0(this);

		methods[6] = new Method[1];
		methods[6][0] = new Method6(this);

		methods[7] = new Method[1];
		methods[7][0] = new Method7(this);

		methods[8] = new Method[1];
		methods[8][0] = new Method8(this);

		methods[9] = new Method[1];
		methods[9][0] = new Method9(this);


		ops = new Operator[6][];

		ops[0] = new Operator[1];
		ops[0][0] = new Operator0(this);

		ops[1] = new Operator[1];
		ops[1][0] = new Operator1(this);

		ops[2] = new Operator[1];
		ops[2][0] = new Operator2(this);

		ops[3] = new Operator[1];
		ops[3][0] = new Operator3(this);

		ops[4] = new Operator[1];
		ops[4][0] = new Operator4(this);

		ops[5] = new Operator[1];
		ops[5][0] = new Operator5(this);

		axioms = new Axiom[11][];

		axioms[0] = new Axiom[0];

		axioms[1] = new Axiom[0];

		axioms[2] = new Axiom[0];

		axioms[3] = new Axiom[0];

		axioms[4] = new Axiom[0];

		axioms[5] = new Axiom[0];

		axioms[6] = new Axiom[0];

		axioms[7] = new Axiom[1];
		axioms[7][0] = new Axiom1(this);

		axioms[8] = new Axiom[0];

		axioms[9] = new Axiom[0];

		axioms[10] = new Axiom[1];
		axioms[10][0] = new Axiom0(this);

	}
}